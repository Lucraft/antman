package lucraft.mods.heroes.antman.util;

import net.minecraft.item.ItemStack;

public interface IPymParticleContainer {

	public int getPymParticles(PymParticleVersion version, ItemStack stack);
	
	public void setPymParticles(PymParticleVersion version, ItemStack stack, int i);
	
	public int addPymParticles(PymParticleVersion version, ItemStack stack, int i, boolean simulate);
	
	public int removePymParticles(PymParticleVersion version, ItemStack stack, int i, boolean simulate);
	
	public boolean hasEnoughPymParticles(PymParticleVersion version, ItemStack stack, int i);
	
	public boolean canAddPymParticles(PymParticleVersion version, ItemStack stack, int i);
	
	public boolean canRemovePymParticles(PymParticleVersion version, ItemStack stack, int i);
	
	public int getMaxPymParticles(PymParticleVersion version, ItemStack stack);
	
	public String getNBTTagString(PymParticleVersion version, ItemStack stack);
	
	public enum PymParticleVersion {
		
		SHRINK,
		GROW;
		
	}
	
}
