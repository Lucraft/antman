package lucraft.mods.heroes.antman.util;

import lucraft.mods.heroes.antman.AntMan;

public class AMSounds {

	public static final String shrink = AntMan.ASSETDIR + "shrink";
	public static final String grow = AntMan.ASSETDIR + "grow";
	public static final String laser1 = AntMan.ASSETDIR + "laser1";
	public static final String laser2 = AntMan.ASSETDIR + "laser2";
	public static final String jetpack = AntMan.ASSETDIR + "jetpack";
	public static final String wings = AntMan.ASSETDIR + "wings";
	
}
