package lucraft.mods.heroes.antman.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lucraft.mods.heroes.antman.items.AMItems;
import lucraft.mods.lucraftcore.items.LCItems;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;

public enum ShrinkerTypes {
	
	MCU_ANTMAN("MCU Version", AMItems.antMan, "random.anvil_land", "plateIron", Arrays.asList(ShrinkerArmorAbilities.shrink, ShrinkerArmorAbilities.antControlling)),
	MCU_ANTMAN_2("MCU Civil War Version", AMItems.antManCW, "random.anvil_land", "plateIntertium", Arrays.asList(ShrinkerArmorAbilities.shrink, ShrinkerArmorAbilities.grow, ShrinkerArmorAbilities.sizeRegulator, ShrinkerArmorAbilities.antControlling)),
	MCU_YELLOWJACKET("MCU Version", AMItems.yellowjacket, "random.anvil_land", "plateTitanium", Arrays.asList(ShrinkerArmorAbilities.shrink, ShrinkerArmorAbilities.laser, ShrinkerArmorAbilities.flight)),
	MCU_WASP("MCU Hope Van Dyne Version", AMItems.antMan, "random.anvil_land", "plateIron", Arrays.asList(ShrinkerArmorAbilities.shrink, ShrinkerArmorAbilities.flight, ShrinkerArmorAbilities.antControlling)),
	COMIC_ANTMAN("Comic Version", ArmorMaterial.CHAIN, "random.anvil_land", LCItems.getColoredTriPolymer(EnumDyeColor.RED, 1), Arrays.asList(ShrinkerArmorAbilities.shrink, ShrinkerArmorAbilities.antControlling)),
	COMIC_WASP("Comic Version", ArmorMaterial.CHAIN, "", LCItems.getColoredTriPolymer(EnumDyeColor.YELLOW, 1), Arrays.asList(ShrinkerArmorAbilities.shrink, ShrinkerArmorAbilities.flight, ShrinkerArmorAbilities.antControlling));
	
	private String desc;
	private ArmorMaterial material;
	private String helmetSound;
	private Object repair;
	private List<ShrinkerArmorAbilities> abilities;
	
	public ItemStack helmet;
	public ItemStack chestplate;
	public ItemStack legs;
	public ItemStack boots;
	
	private ShrinkerTypes(String desc, ArmorMaterial material, String helmetSound, Object stack) {
		this(desc, material, helmetSound, stack, new ArrayList<ShrinkerArmorAbilities>());
	}
	
	private ShrinkerTypes(String desc, ArmorMaterial material, String helmetSound, Object stack, List<ShrinkerArmorAbilities> abilities) {
		this.desc = desc;
		this.material = material;
		this.helmetSound = helmetSound;
		this.repair = stack;
		this.abilities = abilities;
	}
	
	public String getDisplayName() {
		return StatCollector.translateToLocal("antman.shrinker." + this.toString().toLowerCase() + ".name");
	}
	
	public boolean canFly() {
		return getAbilites().contains(ShrinkerArmorAbilities.flight);
	}
	
	public ArmorMaterial getArmorMaterial() {
		return material;
	}
	
	public String getDescription() {
		return desc;
	}
	
	public String getHelmetSound() {
		return helmetSound;
	}
	
	public Object getThingToRepair() {
		return repair;
	}
	
	public List<ShrinkerArmorAbilities> getAbilites() {
		return abilities;
	}
	
	public ItemStack getHelmet() {
		return helmet;
	}
	
	public ItemStack getChestplate() {
		return chestplate;
	}
	
	public ItemStack getLegs() {
		return legs;
	}
	
	public ItemStack getBoots() {
		return boots;
	}
	
}
