package lucraft.mods.heroes.antman.util;

import com.mojang.realmsclient.gui.ChatFormatting;

import lucraft.mods.heroes.antman.entity.AntManEntityData;
import lucraft.mods.heroes.antman.entity.EntityLaser;
import lucraft.mods.heroes.antman.items.AMItems;
import lucraft.mods.heroes.antman.items.InventoryRegulator;
import lucraft.mods.heroes.antman.items.ItemAntManArmor;
import lucraft.mods.heroes.antman.items.ItemAntManChestplate;
import lucraft.mods.heroes.antman.items.ItemShrinkDisc;
import lucraft.mods.heroes.antman.util.IPymParticleContainer.PymParticleVersion;
import lucraft.mods.lucraftcore.access.LucraftForgeLoading;
import lucraft.mods.lucraftcore.modhelpers.antman.INotSizeChangeableEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.MathHelper;
import net.minecraft.util.StatCollector;
import net.minecraft.util.Vec3;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

public class AntManHelper {

	public static void sendInfoMessage(EntityPlayer player, String msg) {
		sendInfoMessage(player, msg, true);
	}

	public static void sendInfoMessage(EntityPlayer player, String msg, boolean translate) {
		String message = translate ? StatCollector.translateToLocal(msg) : msg;
		if (message == null)
			message = "null";
		player.addChatMessage(new ChatComponentText(ChatFormatting.DARK_GRAY + "[" + ChatFormatting.RED + "AntMan" + ChatFormatting.DARK_GRAY + "] " + ChatFormatting.GRAY + message));
	}

	public static void shootYellowjacketLaser(EntityPlayer player, boolean left) {
		Vec3 user = player.getPositionVector().addVector(0, player.eyeHeight, 0);

		if (left) {
			Vec3 vLeft = new Vec3(player.width * 2.5F, 0, player.width / 1.8F);
			vLeft = vLeft.rotateYaw(-player.renderYawOffset * (float) Math.PI / 180F);
			vLeft = vLeft.addVector(-player.motionX * 0.2D, -player.motionY * 0.2D, -player.motionZ * 0.2D);

			Vec3 v = user;
			v = v.add(vLeft);
			player.worldObj.spawnEntityInWorld(new EntityLaser(player.worldObj, player, v.xCoord, v.yCoord, v.zCoord));
			player.worldObj.playSoundAtEntity(player, AMSounds.laser1, 1F, 1F);
		}

		else {
			Vec3 vRight = new Vec3(-player.width * 2.5F, 0, player.width / 1.8F);
			vRight = vRight.rotateYaw(-player.renderYawOffset * (float) Math.PI / 180F);
			vRight = vRight.addVector(-player.motionX * 0.2D, -player.motionY * 0.2D, -player.motionZ * 0.2D);

			Vec3 v2 = user;
			v2 = v2.add(vRight);
			player.worldObj.spawnEntityInWorld(new EntityLaser(player.worldObj, player, v2.xCoord, v2.yCoord, v2.zCoord));
			player.worldObj.playSoundAtEntity(player, AMSounds.laser2, 1F, 1F);
		}

	}

	public static boolean isSizeChangeable(Entity entity) {
		return !(entity instanceof INotSizeChangeableEntity) && entity instanceof EntityLivingBase;
	}

	public static boolean isItemAcceptableForRegulator(ItemStack stack, Item regulator) {
		Item item = stack.getItem();
		return item instanceof ItemShrinkDisc ? true : item == regulator;
	}

	public static Item getRegulatorFromArmor(ItemStack armor) {
		ShrinkerTypes type = getShrinkerType(armor);

		switch (type) {
		case MCU_YELLOWJACKET:
			return AMItems.yjRegulator;
		default:
			return AMItems.regulator;
		}
	}

	public static ItemStack getItemInAntManArmorChestplate(ItemStack chestplate) {
		return (new InventoryRegulator(chestplate)).getStackInSlot(0);
	}

	public static final String[] explosionRadiusNames = new String[] { "explosionRadius", "field_82226_g" };

	public static void setSize(EntityLivingBase en, float standardHeight, float standardWidth, float scale) {

		if (en != null) {
			float height = standardHeight * scale;
			float width = standardWidth * scale;
			float currentWidth = en.width;

			en.entityCollisionReduction = 1 - scale;
			en.width = width;
			en.height = height;
			en.renderDistanceWeight = 10D;

			if (en instanceof EntityPlayer) {
				EntityPlayer player = (EntityPlayer) en;
				player.eyeHeight = player.getDefaultEyeHeight() * scale;
				
//				if(player instanceof EntityPlayerMP) {
//					EntityPlayerMP playerMP = (EntityPlayerMP) player;
//					playerMP.theItemInWorldManager.setBlockReachDistance(5D * scale);
//				}
			}

			if (en instanceof EntityCreeper) {
				int ex = (int) (3 * scale);
				ReflectionHelper.setPrivateValue(EntityCreeper.class, (EntityCreeper) en, ex, LucraftForgeLoading.runtimeObfuscationEnabled ? explosionRadiusNames[0] : explosionRadiusNames[1]);
			}

			en.setEntityBoundingBox(new AxisAlignedBB(en.getEntityBoundingBox().minX, en.getEntityBoundingBox().minY, en.getEntityBoundingBox().minZ, en.getEntityBoundingBox().minX + width, en.getEntityBoundingBox().minY + height, en.getEntityBoundingBox().minZ + width));

			if (width != currentWidth)
				en.setPosition(en.posX, en.posY, en.posZ);

//			if (en.isEntityInsideOpaqueBlock()) {
//				removeBlockInEntity(en);
//			}

		}
	}

	public static void removeBlockInEntity(EntityLivingBase en) {
		if (en.noClip) {
			return;
		} else {
			BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE);

			for (int i = 0; i < 8; ++i) {
				int j = MathHelper.floor_double(en.posY + (double) (((float) ((i >> 0) % 2) - 0.5F) * 0.1F) + (double) en.getEyeHeight());
				int k = MathHelper.floor_double(en.posX + (double) (((float) ((i >> 1) % 2) - 0.5F) * en.width * 0.8F));
				int l = MathHelper.floor_double(en.posZ + (double) (((float) ((i >> 2) % 2) - 0.5F) * en.width * 0.8F));

				if (blockpos$mutableblockpos.getX() != k || blockpos$mutableblockpos.getY() != j || blockpos$mutableblockpos.getZ() != l) {
					blockpos$mutableblockpos.set(k, j, l);

					if (en.worldObj.getBlockState(blockpos$mutableblockpos).getBlock().isVisuallyOpaque()) {
						en.worldObj.setBlockToAir(blockpos$mutableblockpos);
						return;
					}
				}
			}
		}
	}

	public static boolean isNormalSize(EntityLivingBase en) {
		return AntManEntityData.get(en).size == 1F;
	}

	public static boolean isEntityMoving(Entity entity) {
		return entity.prevPosX != entity.posX || entity.prevPosY != entity.posY || entity.prevPosZ != entity.posZ;
	}

	public static boolean hasArmorOn(EntityPlayer player) {
		if (player.getCurrentArmor(3) != null && player.getCurrentArmor(2) != null && player.getCurrentArmor(1) != null && player.getCurrentArmor(0) != null && player.getCurrentArmor(3).getItem() instanceof ItemAntManArmor && player.getCurrentArmor(2).getItem() instanceof ItemAntManArmor && player.getCurrentArmor(1).getItem() instanceof ItemAntManArmor && player.getCurrentArmor(0).getItem() instanceof ItemAntManArmor
				&& ((ItemAntManArmor) player.getCurrentArmor(0).getItem()).getShrinkerType() == ((ItemAntManArmor) player.getCurrentArmor(1).getItem()).getShrinkerType() && ((ItemAntManArmor) player.getCurrentArmor(1).getItem()).getShrinkerType() == ((ItemAntManArmor) player.getCurrentArmor(2).getItem()).getShrinkerType() && ((ItemAntManArmor) player.getCurrentArmor(2).getItem()).getShrinkerType() == ((ItemAntManArmor) player.getCurrentArmor(3).getItem()).getShrinkerType()) {
			return true;
		}

		return false;
	}

	public static boolean hasArmorOn(EntityPlayer player, ShrinkerTypes type) {
		return hasArmorOn(player) && ((ItemAntManArmor) player.getCurrentArmor(2).getItem()).getShrinkerType() == type;
	}

	public static boolean hasHelmetOn(EntityPlayer player) {
		return player.getCurrentArmor(3) != null && player.getCurrentArmor(3).getItem() instanceof ItemAntManArmor;
	}

	public static boolean hasChestplateOn(EntityPlayer player) {
		return player.getCurrentArmor(2) != null && player.getCurrentArmor(2).getItem() instanceof ItemAntManChestplate;
	}

	public static ItemStack getAntManChestplate(EntityPlayer player) {
		return hasChestplateOn(player) ? player.getCurrentArmor(2) : null;
	}

	public static float getSavedScaleFromChestplate(EntityPlayer player) {
		return getSavedScaleFromChestplate(player.getCurrentArmor(2));
	}

	public static float getSavedScaleFromChestplate(ItemStack stack) {
		if (stack.getItem() instanceof ItemAntManChestplate && ((ItemAntManChestplate) stack.getItem()).hasAbility(ShrinkerArmorAbilities.sizeRegulator)) {
			return (float) ((ItemAntManChestplate) stack.getItem()).getEstimatedSizeToChange(stack);
		}

		return 0F;
	}

	public static void setSavedScaleInChestplate(EntityPlayer player, double size) {
		setSavedScaleInChestplate(player.getCurrentArmor(2), size);
	}

	public static void setSavedScaleInChestplate(ItemStack stack, double size) {
		if (stack.getItem() instanceof ItemAntManChestplate && ((ItemAntManChestplate) stack.getItem()).hasAbility(ShrinkerArmorAbilities.sizeRegulator)) {
			NBTTagCompound compound = stack.getTagCompound();
			size = MathHelper.clamp_double(size, 0.11D, 10D);
			compound.setDouble(nbtSizeChestValue, size);
		}
	}

	public static ShrinkerTypes getShrinkerType(ItemStack stack) {
		return getShrinkerType(stack.getItem());
	}

	public static ShrinkerTypes getShrinkerType(Item item) {
		return item instanceof ItemAntManArmor ? ((ItemAntManArmor) item).getShrinkerType() : null;
	}

	public static ShrinkerTypes getShrinkerTypeFromHelmet(EntityPlayer player) {
		ItemStack helmet = player.getCurrentArmor(3);

		if (helmet != null && helmet.getItem() instanceof ItemAntManArmor) {
			return ((ItemAntManArmor) helmet.getItem()).getShrinkerType();
		}

		return null;
	}

	public static double getExtraDmgFromSize(float size) {
		float f = 1F - size;
		return size == 1F ? 1F : size > 1F ? size : (f * f * 4);
	}

	public static String nbtShrinkParticles = "shrinkPymParticles";
	public static String nbtGrowParticles = "growPymParticles";
	public static String nbtSizeChestValue = "sizeValue";

	public static ItemStack setDefaultPymTankNBTTags(ItemStack stack) {
		if (!stack.hasTagCompound()) {
			NBTTagCompound compound = new NBTTagCompound();
			compound.setInteger(nbtShrinkParticles, 0);
			compound.setInteger(nbtGrowParticles, 0);
			stack.setTagCompound(compound);
		}

		return stack;
	}

	public static String getPymTankDesc(PymParticleVersion v) {
		return v == PymParticleVersion.SHRINK ? ChatFormatting.RED + StatCollector.translateToLocal("antman.info.shrinkparticledesc") + ChatFormatting.DARK_RED + ":" : ChatFormatting.BLUE + StatCollector.translateToLocal("antman.info.growparticledesc") + ChatFormatting.DARK_BLUE + ":";
	}

	public static ItemStack setDefaultPymTankNBTTags(ItemStack stack, int particleAmount) {
		setDefaultPymTankNBTTags(stack, PymParticleVersion.SHRINK, particleAmount);
		setDefaultPymTankNBTTags(stack, PymParticleVersion.SHRINK, particleAmount);
		return stack;
	}

	public static ItemStack setDefaultPymTankNBTTags(ItemStack stack, PymParticleVersion version, int particleAmount) {
		if (!stack.hasTagCompound()) {
			setDefaultPymTankNBTTags(stack);
			NBTTagCompound compound = stack.getTagCompound();

			if (version == PymParticleVersion.SHRINK) {
				compound.setInteger(nbtShrinkParticles, particleAmount);
			} else if (version == PymParticleVersion.GROW) {
				compound.setInteger(nbtGrowParticles, particleAmount);
			}

			if (version != null)
				compound.setString("Particles", version.toString().toLowerCase());
			else
				compound.setString("Particles", "null");

			if (particleAmount == 0)
				compound.setString("Particles", "null");

			stack.setTagCompound(compound);
		}

		return stack;
	}

}