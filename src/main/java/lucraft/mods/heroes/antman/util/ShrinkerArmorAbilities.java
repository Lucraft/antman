package lucraft.mods.heroes.antman.util;

import net.minecraft.util.StatCollector;

public class ShrinkerArmorAbilities {
	
	public static ShrinkerArmorAbilities shrink = new ShrinkerArmorAbilities("shrink");
	public static ShrinkerArmorAbilities grow = new ShrinkerArmorAbilities("grow");
	public static ShrinkerArmorAbilities flight = new ShrinkerArmorAbilities("flight");
	public static ShrinkerArmorAbilities laser = new ShrinkerArmorAbilities("laser");
	public static ShrinkerArmorAbilities sizeRegulator = new ShrinkerArmorAbilities("sizeRegulator");
	public static ShrinkerArmorAbilities antControlling = new ShrinkerArmorAbilities("antControlling");
	
	//------------------------------------------------------
	
	private String name;
	
	public ShrinkerArmorAbilities(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDisplayName() {
		return StatCollector.translateToLocal("ability." + name + ".name");
	}
	
}
