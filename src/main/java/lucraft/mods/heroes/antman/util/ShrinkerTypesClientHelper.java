package lucraft.mods.heroes.antman.util;

import lucraft.mods.heroes.antman.AntMan;
import lucraft.mods.heroes.antman.client.models.ModelAdvancedArmor;
import lucraft.mods.heroes.antman.client.models.ModelAntManHelmet;
import lucraft.mods.heroes.antman.client.models.ModelWaspWings;
import lucraft.mods.heroes.antman.client.models.ModelYellowjacketChestplate;
import lucraft.mods.heroes.antman.client.models.ModelYellowjacketHelmet;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;

public class ShrinkerTypesClientHelper {

	public static ModelAdvancedArmor getChestplateModel(ShrinkerTypes type, String lightTextLoc) {
		switch (type) {
		case MCU_YELLOWJACKET:
			return new ModelYellowjacketChestplate(0.5F, 128, 64, new ResourceLocation(lightTextLoc));
		case MCU_WASP:
		case COMIC_WASP:
			return new ModelWaspWings(0.5F, 128, 64, new ResourceLocation(lightTextLoc));
		default:
			return new ModelAdvancedArmor(0.5F, 64, 64, new ResourceLocation(lightTextLoc));
		}
	}
	
	public static ModelAdvancedArmor getHelmetModel(ShrinkerTypes type, String lightTextLoc) {
		switch (type) {
		case MCU_ANTMAN:
		case MCU_ANTMAN_2:
		case COMIC_ANTMAN:
			return new ModelAntManHelmet(0.5F, new ResourceLocation(lightTextLoc));
		case MCU_YELLOWJACKET:
			return new ModelYellowjacketHelmet(0.5F, new ResourceLocation(lightTextLoc));
		default:
			return new ModelAdvancedArmor(0.5F, 64, 32, new ResourceLocation(lightTextLoc));
		}
	}
	
	public static ResourceLocation normal = new ResourceLocation(AntMan.ASSETDIR + "textures/gui/hud.png");
	public static ResourceLocation yellow = new ResourceLocation(AntMan.ASSETDIR + "textures/gui/hud_yellow.png");
	
	public static ResourceLocation getHUDResourceLocation(ShrinkerTypes type) {
		switch (type) {
		case MCU_YELLOWJACKET:
			return yellow;
		default:
			return normal;
		}
	}

	public static EnumChatFormatting getShrinkPymParticlesChatColor(ShrinkerTypes type, boolean dark) {
		switch (type) {
		case MCU_YELLOWJACKET:
			return dark ? EnumChatFormatting.GOLD : EnumChatFormatting.YELLOW;
		default:
			return dark ? EnumChatFormatting.DARK_RED : EnumChatFormatting.RED;
		}
	}
	
}
