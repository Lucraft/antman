package lucraft.mods.heroes.antman.util;

public class AMSizes {

	public static float small = 0.11F;
	public static float normal = 1F;
	public static float big = 3F;
	
	public enum AntManSizes {

		SMALL(0, 0.11F, 3F),
		NORMAL(1, 1F, 0F),
		BIG(2, 3F, 3F);
		
		private int id;
		private float scale;
		private float extraDmg;
		
		private AntManSizes(int id, float scale, float extraDmg) {
			this.id = id;
			this.scale = scale;
			this.extraDmg = extraDmg;
		}
		
		public int getId() {
			return id;
		}
		
		public float getScale() {
			return scale;
		}
		
		public float getExtraDmg() {
			return extraDmg;
		}
		
		public static AntManSizes getBiggerSize(AntManSizes size) {
			if(size == AntManSizes.BIG)
				return size;
			else
				return getSizeFromId(size.getId() + 1);
		}
		
		public static AntManSizes getSmallerSize(AntManSizes size) {
			if(size == AntManSizes.SMALL)
				return size;
			else
				return getSizeFromId(size.getId() - 1);
		}
		
		public static AntManSizes getSizeFromId(int id) {
			for(AntManSizes size : AntManSizes.values()) {
				if(size.getId() == id) {
					return size;
				}
			}
			
			return null;
		}
		
		public static AntManSizes getSizeFromFloat(float f) {
			
			for(AntManSizes s : AntManSizes.values()) {
				if(s.scale == f) {
					return s;
				}
			}
			
			if(f > 1F)
				return AntManSizes.BIG;
			
			if(f < 1F)
				return SMALL; 
				
			return NORMAL;
		}
		
	}
	
}
