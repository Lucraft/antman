package lucraft.mods.heroes.antman.integration.jei;

import lucraft.mods.heroes.antman.client.gui.GuiPymWorkbench;
import lucraft.mods.heroes.antman.container.ContainerPymWorkbench;
import lucraft.mods.heroes.antman.integration.jei.pymWorkbench.PymWorkbenchRecipeCategory;
import lucraft.mods.heroes.antman.integration.jei.pymWorkbench.PymWorkbenchShapedOreRecipeHandler;
import lucraft.mods.heroes.antman.recipes.AntManCraftingManager;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.IItemRegistry;
import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.IJeiRuntime;
import mezz.jei.api.IModPlugin;
import mezz.jei.api.IModRegistry;
import mezz.jei.api.IRecipeRegistry;
import mezz.jei.api.JEIPlugin;
import mezz.jei.api.recipe.transfer.IRecipeTransferRegistry;

@JEIPlugin
public class AntManJEIPlugin implements IModPlugin {

	public static final String PYM_WORKBENCH = "antman.pym_workbench";
	
	public AntManJEIPlugin() {}
	
	@Override
	public void register(IModRegistry reg) {
		IJeiHelpers jeiHelpers = reg.getJeiHelpers();
		IGuiHelper guiHelper = jeiHelpers.getGuiHelper();
		
		reg.addRecipeCategories(new PymWorkbenchRecipeCategory(guiHelper));
		reg.addRecipeHandlers(new PymWorkbenchShapedOreRecipeHandler());
		
		reg.addRecipeClickArea(GuiPymWorkbench.class, 88, 32, 28, 23, PYM_WORKBENCH);
		
		IRecipeTransferRegistry recipeTransferRegistry = reg.getRecipeTransferRegistry();
		
		recipeTransferRegistry.addRecipeTransferHandler(ContainerPymWorkbench.class, PYM_WORKBENCH, 1, 9, 10, 36);
		reg.addRecipes(AntManCraftingManager.getInstance().getRecipeList());
	}
	
	@Override
	public void onItemRegistryAvailable(IItemRegistry arg0) {
	}

	@Override
	public void onJeiHelpersAvailable(IJeiHelpers arg0) {
	}

	@Override
	public void onRecipeRegistryAvailable(IRecipeRegistry arg0) {
	}

	@Override
	public void onRuntimeAvailable(IJeiRuntime arg0) {
	}

}
