package lucraft.mods.heroes.antman.integration.jei.pymWorkbench;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;

import lucraft.mods.heroes.antman.integration.jei.PymWorkbenchRecipeWrapper;
import lucraft.mods.heroes.antman.recipes.ShapedPymOreRecipe;
import mezz.jei.api.recipe.wrapper.IShapedCraftingRecipeWrapper;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

public class PymWorkbenchShapedOreRecipeWrapper extends PymWorkbenchRecipeWrapper implements IShapedCraftingRecipeWrapper {

	@Nonnull
	private final ShapedPymOreRecipe recipe;
	private final int width;
	private final int height;

	public PymWorkbenchShapedOreRecipeWrapper(@Nonnull ShapedPymOreRecipe recipe) {
		this.recipe = recipe;
		for (Object input : this.recipe.getInput()) {
			if (input instanceof ItemStack) {
				ItemStack itemStack = (ItemStack) input;
				if (itemStack.stackSize != 1) {
					itemStack.stackSize = 1;
				}
			}
		}
		this.width = ObfuscationReflectionHelper.getPrivateValue(ShapedPymOreRecipe.class, this.recipe, "width");
		this.height = ObfuscationReflectionHelper.getPrivateValue(ShapedPymOreRecipe.class, this.recipe, "height");
	}

	@Nonnull
	@Override
	public List getInputs() {
		return Arrays.asList(recipe.getInput());
	}

	@Nonnull
	@Override
	public List<ItemStack> getOutputs() {
		return Collections.singletonList(recipe.getRecipeOutput());
	}

	@Override
	public int getWidth() {
		return width;
	}

	@Override
	public int getHeight() {
		return height;
	}

}
