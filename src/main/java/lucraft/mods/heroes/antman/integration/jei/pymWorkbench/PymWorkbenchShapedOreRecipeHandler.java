package lucraft.mods.heroes.antman.integration.jei.pymWorkbench;

import java.util.List;

import javax.annotation.Nonnull;

import lucraft.mods.heroes.antman.integration.jei.AntManJEIPlugin;
import lucraft.mods.heroes.antman.recipes.ShapedPymOreRecipe;
import mezz.jei.api.recipe.IRecipeHandler;
import mezz.jei.api.recipe.IRecipeWrapper;

public class PymWorkbenchShapedOreRecipeHandler implements IRecipeHandler<ShapedPymOreRecipe> {

	@Override
	@Nonnull
	public Class<ShapedPymOreRecipe> getRecipeClass() {
		return ShapedPymOreRecipe.class;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid() {
		return AntManJEIPlugin.PYM_WORKBENCH;
	}

	@Override
	@Nonnull
	public IRecipeWrapper getRecipeWrapper(@Nonnull ShapedPymOreRecipe recipe) {
		return new PymWorkbenchShapedOreRecipeWrapper(recipe);
	}

	@Override
	public boolean isRecipeValid(@Nonnull ShapedPymOreRecipe recipe) {
		if (recipe.getRecipeOutput() == null) {
			return false;
		}
		int inputCount = 0;
		for (Object input : recipe.getInput()) {
			if (input instanceof List) {
				if (((List) input).size() == 0) {
					return false;
				}
			}
			if (input != null) {
				inputCount++;
			}
		}
		return inputCount > 0;
	}
}
