package lucraft.mods.heroes.antman.client.models;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityArmorStand;
import net.minecraft.util.ResourceLocation;

public class ModelAdvancedArmor extends ModelBiped {
	
	public ResourceLocation lightsTex;
	
	public ModelRenderer bipedBodyWear;

	public ModelRenderer bipedRightArmWear;

	public ModelRenderer bipedLeftArmWear;

	public ModelRenderer bipedRightLegWear;

	public ModelRenderer bipedLeftLegWear;

	public ModelAdvancedArmor(float f, int width, int height, ResourceLocation resource) {
		super(f, 0, width, height);

		this.textureWidth = width;
		this.textureHeight = height;
		this.lightsTex = resource;
		
		float scale = f + 0.5F;
		
		this.bipedBodyWear = new ModelRenderer(this, 16, 32);
		this.bipedBodyWear.addBox(-4.0F, 0.0F, -2.0F, 8, 12, 4, scale);
		this.bipedBodyWear.setRotationPoint(0.0F, 0.0F, 0.0F);

		this.bipedRightArmWear = new ModelRenderer(this, 40, 32);
		this.bipedRightArmWear.addBox(-3.0F, -2.0F, -2.0F, 4, 12, 4, scale);
		this.bipedRightArmWear.setRotationPoint(0.0F, 0.0F, 0.0F);

		this.bipedLeftArmWear = new ModelRenderer(this, 48, 48);
//		this.bipedLeftArmWear.mirror = true;
		this.bipedLeftArmWear.addBox(-1.0F, -2.0F, -2.0F, 4, 12, 4, scale);
		this.bipedLeftArmWear.setRotationPoint(0.0F, 0.0F, 0.0F);

		this.bipedRightLegWear = new ModelRenderer(this, 0, 32);
		this.bipedRightLegWear.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, scale);
		this.bipedRightLegWear.setRotationPoint(0.0F, 0.0F, 0.0F);

		this.bipedLeftLegWear = new ModelRenderer(this, 0, 48);
//		this.bipedLeftLegWear.mirror = true;
		this.bipedLeftLegWear.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, scale);
		this.bipedLeftLegWear.setRotationPoint(0.0F, 0.0F, 0.0F);
		
		this.bipedRightArm.setTextureOffset(40, 16);
		this.bipedLeftArm.setTextureOffset(32, 48);
		this.bipedRightLeg.setTextureOffset(0, 16);
		this.bipedLeftLeg.setTextureOffset(16, 48);
		
		this.bipedBody.addChild(bipedBodyWear);
		this.bipedRightArm.addChild(bipedRightArmWear);
		this.bipedLeftArm.addChild(bipedLeftArmWear);
		this.bipedRightLeg.addChild(bipedRightLegWear);
		this.bipedLeftLeg.addChild(bipedLeftLegWear);

	}

	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		GlStateManager.enableBlend();
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		super.render(entity, f, f1, f2, f3, f4, f5);
		super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		Minecraft.getMinecraft().renderEngine.bindTexture(lightsTex);
		GlStateManager.pushMatrix();
		GlStateManager.disableLighting();
		
		float lastBrightnessX = OpenGlHelper.lastBrightnessX;
		float lastBrightnessY = OpenGlHelper.lastBrightnessY;
		
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 240F, 240F);
		super.render(entity, f, f1, f2, f3, f4, f5);
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, lastBrightnessX, lastBrightnessY);
		GlStateManager.disableBlend();
		GlStateManager.enableLighting();
		GlStateManager.popMatrix();
	}
	
	@Override
	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entityIn) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, entityIn);

		if (entityIn instanceof EntityArmorStand) {
			EntityArmorStand entityarmorstand = (EntityArmorStand) entityIn;
			this.bipedHead.rotateAngleX = 0.017453292F * entityarmorstand.getHeadRotation().getX();
			this.bipedHead.rotateAngleY = 0.017453292F * entityarmorstand.getHeadRotation().getY();
			this.bipedHead.rotateAngleZ = 0.017453292F * entityarmorstand.getHeadRotation().getZ();
			this.bipedHead.setRotationPoint(0.0F, 1.0F, 0.0F);
			this.bipedBody.rotateAngleX = 0.017453292F * entityarmorstand.getBodyRotation().getX();
			this.bipedBody.rotateAngleY = 0.017453292F * entityarmorstand.getBodyRotation().getY();
			this.bipedBody.rotateAngleZ = 0.017453292F * entityarmorstand.getBodyRotation().getZ();
			this.bipedLeftArm.rotateAngleX = 0.017453292F * entityarmorstand.getLeftArmRotation().getX();
			this.bipedLeftArm.rotateAngleY = 0.017453292F * entityarmorstand.getLeftArmRotation().getY();
			this.bipedLeftArm.rotateAngleZ = 0.017453292F * entityarmorstand.getLeftArmRotation().getZ();
			this.bipedRightArm.rotateAngleX = 0.017453292F * entityarmorstand.getRightArmRotation().getX();
			this.bipedRightArm.rotateAngleY = 0.017453292F * entityarmorstand.getRightArmRotation().getY();
			this.bipedRightArm.rotateAngleZ = 0.017453292F * entityarmorstand.getRightArmRotation().getZ();
			this.bipedLeftLeg.rotateAngleX = 0.017453292F * entityarmorstand.getLeftLegRotation().getX();
			this.bipedLeftLeg.rotateAngleY = 0.017453292F * entityarmorstand.getLeftLegRotation().getY();
			this.bipedLeftLeg.rotateAngleZ = 0.017453292F * entityarmorstand.getLeftLegRotation().getZ();
			this.bipedLeftLeg.setRotationPoint(1.9F, 11.0F, 0.0F);
			this.bipedRightLeg.rotateAngleX = 0.017453292F * entityarmorstand.getRightLegRotation().getX();
			this.bipedRightLeg.rotateAngleY = 0.017453292F * entityarmorstand.getRightLegRotation().getY();
			this.bipedRightLeg.rotateAngleZ = 0.017453292F * entityarmorstand.getRightLegRotation().getZ();
			this.bipedRightLeg.setRotationPoint(-1.9F, 11.0F, 0.0F);
			copyModelAngles(this.bipedHead, this.bipedHeadwear);
		}
	}
}