package lucraft.mods.heroes.antman.client.book;

import lucraft.mods.heroes.antman.entity.EntityCarpenterAnt;
import lucraft.mods.lucraftcore.client.gui.book.BookChapter;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

public class BookPageCarpenterAnt extends BookPageAnt {

	public BookPageCarpenterAnt(BookChapter parent) {
		super(parent);
	}
	
	@Override
	public EntityLivingBase getAnt(World world) {
		return new EntityCarpenterAnt(world);
	}

	@Override
	public String getDescription() {
		return StatCollector.translateToLocal("book.ants.carpenterAnt.desc");
	}

}
