package lucraft.mods.heroes.antman.client.gui;

public class GuiIDs {
	
	public static final int pymWorkbenchGuiId = 0;
	public static final int pymParticleProducerGuiId = 1;
	public static final int sizeRegulatorGuiId = 2;
	public static final int quantumRealmChest = 3;
	
}
