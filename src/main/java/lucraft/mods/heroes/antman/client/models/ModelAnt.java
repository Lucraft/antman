package lucraft.mods.heroes.antman.client.models;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * ModelAnt by Sheriff
 */
@SideOnly(Side.CLIENT)
public class ModelAnt extends ModelBase {
	
    public ModelRenderer gaster;
    public ModelRenderer node;
    public ModelRenderer thorax;
    public ModelRenderer node2;
    public ModelRenderer head;
    public ModelRenderer shape21;
    public ModelRenderer shape22;
    public ModelRenderer shape23;
    public ModelRenderer shape24;
    public ModelRenderer right_front_top_leg;
    public ModelRenderer right_middle_top_leg;
    public ModelRenderer right_front_bottom_leg;
    public ModelRenderer right_middle_bottom_leg;
    public ModelRenderer right_back_top_leg;
    public ModelRenderer right_back_bottom_leg;
    public ModelRenderer left_front_top_leg;
    public ModelRenderer left_front_bottom_leg;
    public ModelRenderer left_middle_top_leg;
    public ModelRenderer left_middle_bottom_leg;
    public ModelRenderer left_back_top_leg;
    public ModelRenderer left_back_bottom_leg;

    public ModelAnt() {
        this.textureWidth = 64;
        this.textureHeight = 32;

        this.gaster = new ModelRenderer(this, 0, 10);
        this.gaster.setRotationPoint(0.0F, 21.0F, 3.0F);
        this.gaster.addBox(-2.0F, -1.0F, 0.0F, 4, 3, 5);
        this.node = new ModelRenderer(this, 0, 18);
        this.node.setRotationPoint(0.0F, 21.0F, 3.0F);
        this.node.addBox(-1.0F, 0.0F, -2.0F, 2, 1, 2);
        this.thorax = new ModelRenderer(this, 0, 21);
        this.thorax.setRotationPoint(0.0F, 21.5F, 1.0F);
        this.thorax.addBox(-2.0F, -1.0F, -3.0F, 4, 2, 3);
        this.node2 = new ModelRenderer(this, 9, 18);
        this.node2.setRotationPoint(0.0F, 22.0F, -2.0F);
        this.node2.addBox(-1.0F, -1.0F, -1.0F, 2, 1, 1);
        this.head = new ModelRenderer(this, 0, 26);
        this.head.setRotationPoint(0.0F, 22.0F, -2.0F);
        this.head.addBox(-2.0F, -2.5F, -4.0F, 4, 3, 3);
        this.shape21 = new ModelRenderer(this, 0, 0);
        this.shape21.setRotationPoint(0.0F, 22.0F, -2.0F);
        this.shape21.addBox(1.0F, -0.6F, -5.0F, 1, 1, 1);
        this.shape22 = new ModelRenderer(this, 0, 0);
        this.shape22.setRotationPoint(0.0F, 22.0F, -2.0F);
        this.shape22.addBox(-2.0F, -0.6F, -5.0F, 1, 1, 1);
        this.shape23 = new ModelRenderer(this, 5, 0);
        this.shape23.setRotationPoint(0.0F, 22.0F, -2.0F);
        this.shape23.addBox(1.0F, -5.2F, -3.0F, 1, 3, 1);
        this.setRotationAngles(this.shape23, 0.2275909337942703F, 0.0F, 0.0F);
        this.shape24 = new ModelRenderer(this, 5, 0);
        this.shape24.setRotationPoint(0.0F, 22.0F, -2.0F);
        this.shape24.addBox(-2.0F, -5.2F, -3.0F, 1, 3, 1);
        this.setRotationAngles(this.shape24, 0.2275909337942703F, 0.0F, 0.0F);
        this.right_front_top_leg = new ModelRenderer(this, 25, 0);
        this.right_front_top_leg.setRotationPoint(2.0F, 21.0F, -1.5F);
        this.right_front_top_leg.addBox(-0.5F, 0.0F, -0.5F, 3, 1, 1);
        this.setRotationAngles(this.right_front_top_leg, 0.0F, 0.2617993877991494F, -0.4363323129985824F);
        this.right_middle_top_leg = new ModelRenderer(this, 25, 0);
        this.right_middle_top_leg.setRotationPoint(2.0F, 21.0F, -0.5F);
        this.right_middle_top_leg.addBox(-0.5F, 0.0F, -0.5F, 3, 1, 1);
        this.setRotationAngles(this.right_middle_top_leg, 0.0F, 0.0F, -0.4363323129985824F);
        this.right_front_bottom_leg = new ModelRenderer(this, 34, 0);
        this.right_front_bottom_leg.setRotationPoint(2.0F, 21.0F, -1.5F);
        this.right_front_bottom_leg.addBox(2.5F, 0.0F, -0.5F, 1, 4, 1);
        this.setRotationAngles(this.right_front_bottom_leg, 0.0F, 0.2617993877991494F, -0.4363323129985824F);
        this.right_middle_bottom_leg = new ModelRenderer(this, 34, 0);
        this.right_middle_bottom_leg.setRotationPoint(2.0F, 21.0F, -0.5F);
        this.right_middle_bottom_leg.addBox(2.5F, 0.0F, -0.5F, 1, 4, 1);
        this.setRotationAngles(this.right_middle_bottom_leg, 0.0F, 0.0F, -0.4363323129985824F);
        this.right_back_top_leg = new ModelRenderer(this, 25, 0);
        this.right_back_top_leg.setRotationPoint(2.0F, 21.0F, 0.5F);
        this.right_back_top_leg.addBox(-0.5F, 0.0F, -0.5F, 3, 1, 1);
        this.setRotationAngles(this.right_back_top_leg, 0.0F, -0.2617993877991494F, -0.4363323129985824F);
        this.right_back_bottom_leg = new ModelRenderer(this, 34, 0);
        this.right_back_bottom_leg.setRotationPoint(2.0F, 21.0F, 0.5F);
        this.right_back_bottom_leg.addBox(2.5F, 0.0F, -0.5F, 1, 4, 1);
        this.setRotationAngles(this.right_back_bottom_leg, 0.0F, -0.2617993877991494F, -0.4363323129985824F);
        this.left_front_top_leg = new ModelRenderer(this, 15, 0);
        this.left_front_top_leg.setRotationPoint(-2.0F, 21.0F, -1.5F);
        this.left_front_top_leg.addBox(-2.5F, 0.0F, -0.5F, 3, 1, 1);
        this.setRotationAngles(this.left_front_top_leg, 0.0F, -0.2617993877991494F, 0.4363323129985824F);
        this.left_front_bottom_leg = new ModelRenderer(this, 10, 0);
        this.left_front_bottom_leg.setRotationPoint(-2.0F, 21.0F, -1.5F);
        this.left_front_bottom_leg.addBox(-3.5F, 0.0F, -0.5F, 1, 4, 1);
        this.setRotationAngles(this.left_front_bottom_leg, 0.0F, -0.2617993877991494F, 0.4363323129985824F);
        this.left_middle_top_leg = new ModelRenderer(this, 15, 0);
        this.left_middle_top_leg.setRotationPoint(-2.0F, 21.0F, -0.5F);
        this.left_middle_top_leg.addBox(-2.5F, 0.0F, -0.5F, 3, 1, 1);
        this.setRotationAngles(this.left_middle_top_leg, 0.0F, 0.0F, 0.4363323129985824F);
        this.left_middle_bottom_leg = new ModelRenderer(this, 10, 0);
        this.left_middle_bottom_leg.setRotationPoint(-2.0F, 21.0F, -0.5F);
        this.left_middle_bottom_leg.addBox(-3.5F, 0.0F, -0.5F, 1, 4, 1);
        this.setRotationAngles(this.left_middle_bottom_leg, 0.0F, 0.0F, 0.4363323129985824F);
        this.left_back_top_leg = new ModelRenderer(this, 15, 0);
        this.left_back_top_leg.setRotationPoint(-2.0F, 21.0F, 0.5F);
        this.left_back_top_leg.addBox(-2.5F, 0.0F, -0.5F, 3, 1, 1);
        this.setRotationAngles(this.left_back_top_leg, 0.0F, 0.2617993877991494F, 0.4363323129985824F);
        this.left_back_bottom_leg = new ModelRenderer(this, 10, 0);
        this.left_back_bottom_leg.setRotationPoint(-2.0F, 21.0F, 0.5F);
        this.left_back_bottom_leg.addBox(-3.5F, 0.0F, -0.5F, 1, 4, 1);
        this.setRotationAngles(this.left_back_bottom_leg, 0.0F, 0.2617993877991494F, 0.4363323129985824F);
    }

    @Override
    public void render(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float rotationYaw, float rotationPitch, float scale) {
        this.gaster.render(scale);
        this.node.render(scale);
        this.thorax.render(scale);
        this.node2.render(scale);
        this.head.render(scale);
        this.shape21.render(scale);
        this.shape22.render(scale);
        GlStateManager.pushMatrix();
        GlStateManager.translate(this.shape23.offsetX, this.shape23.offsetY, this.shape23.offsetZ);
        GlStateManager.translate(this.shape23.rotationPointX * scale, this.shape23.rotationPointY * scale, this.shape23.rotationPointZ * scale);
        GlStateManager.scale(1.0F, 1.0F, 1.0F);
        GlStateManager.translate(-this.shape23.offsetX, -this.shape23.offsetY, -this.shape23.offsetZ);
        GlStateManager.translate(-this.shape23.rotationPointX * scale, -this.shape23.rotationPointY * scale, -this.shape23.rotationPointZ * scale);
        this.shape23.render(scale);
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        GlStateManager.translate(this.shape24.offsetX, this.shape24.offsetY, this.shape24.offsetZ);
        GlStateManager.translate(this.shape24.rotationPointX * scale, this.shape24.rotationPointY * scale, this.shape24.rotationPointZ * scale);
        GlStateManager.scale(1.0F, 1.0F, 1.0F);
        GlStateManager.translate(-this.shape24.offsetX, -this.shape24.offsetY, -this.shape24.offsetZ);
        GlStateManager.translate(-this.shape24.rotationPointX * scale, -this.shape24.rotationPointY * scale, -this.shape24.rotationPointZ * scale);
        this.shape24.render(scale);
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        GlStateManager.translate(this.right_front_top_leg.offsetX, this.right_front_top_leg.offsetY, this.right_front_top_leg.offsetZ);
        GlStateManager.translate(this.right_front_top_leg.rotationPointX * scale, this.right_front_top_leg.rotationPointY * scale, this.right_front_top_leg.rotationPointZ * scale);
        GlStateManager.scale(1.0F, 1.0F, 1.0F);
        GlStateManager.translate(-this.right_front_top_leg.offsetX, -this.right_front_top_leg.offsetY, -this.right_front_top_leg.offsetZ);
        GlStateManager.translate(-this.right_front_top_leg.rotationPointX * scale, -this.right_front_top_leg.rotationPointY * scale, -this.right_front_top_leg.rotationPointZ * scale);
        this.right_front_top_leg.render(scale);
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        GlStateManager.translate(this.right_middle_top_leg.offsetX, this.right_middle_top_leg.offsetY, this.right_middle_top_leg.offsetZ);
        GlStateManager.translate(this.right_middle_top_leg.rotationPointX * scale, this.right_middle_top_leg.rotationPointY * scale, this.right_middle_top_leg.rotationPointZ * scale);
        GlStateManager.scale(1.0F, 1.0F, 1.0F);
        GlStateManager.translate(-this.right_middle_top_leg.offsetX, -this.right_middle_top_leg.offsetY, -this.right_middle_top_leg.offsetZ);
        GlStateManager.translate(-this.right_middle_top_leg.rotationPointX * scale, -this.right_middle_top_leg.rotationPointY * scale, -this.right_middle_top_leg.rotationPointZ * scale);
        this.right_middle_top_leg.render(scale);
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        GlStateManager.translate(this.right_front_bottom_leg.offsetX, this.right_front_bottom_leg.offsetY, this.right_front_bottom_leg.offsetZ);
        GlStateManager.translate(this.right_front_bottom_leg.rotationPointX * scale, this.right_front_bottom_leg.rotationPointY * scale, this.right_front_bottom_leg.rotationPointZ * scale);
        GlStateManager.scale(1.0F, 1.0F, 1.0F);
        GlStateManager.translate(-this.right_front_bottom_leg.offsetX, -this.right_front_bottom_leg.offsetY, -this.right_front_bottom_leg.offsetZ);
        GlStateManager.translate(-this.right_front_bottom_leg.rotationPointX * scale, -this.right_front_bottom_leg.rotationPointY * scale, -this.right_front_bottom_leg.rotationPointZ * scale);
        this.right_front_bottom_leg.render(scale);
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        GlStateManager.translate(this.right_middle_bottom_leg.offsetX, this.right_middle_bottom_leg.offsetY, this.right_middle_bottom_leg.offsetZ);
        GlStateManager.translate(this.right_middle_bottom_leg.rotationPointX * scale, this.right_middle_bottom_leg.rotationPointY * scale, this.right_middle_bottom_leg.rotationPointZ * scale);
        GlStateManager.scale(1.0F, 1.0F, 1.0F);
        GlStateManager.translate(-this.right_middle_bottom_leg.offsetX, -this.right_middle_bottom_leg.offsetY, -this.right_middle_bottom_leg.offsetZ);
        GlStateManager.translate(-this.right_middle_bottom_leg.rotationPointX * scale, -this.right_middle_bottom_leg.rotationPointY * scale, -this.right_middle_bottom_leg.rotationPointZ * scale);
        this.right_middle_bottom_leg.render(scale);
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        GlStateManager.translate(this.right_back_top_leg.offsetX, this.right_back_top_leg.offsetY, this.right_back_top_leg.offsetZ);
        GlStateManager.translate(this.right_back_top_leg.rotationPointX * scale, this.right_back_top_leg.rotationPointY * scale, this.right_back_top_leg.rotationPointZ * scale);
        GlStateManager.scale(1.0F, 1.0F, 1.0F);
        GlStateManager.translate(-this.right_back_top_leg.offsetX, -this.right_back_top_leg.offsetY, -this.right_back_top_leg.offsetZ);
        GlStateManager.translate(-this.right_back_top_leg.rotationPointX * scale, -this.right_back_top_leg.rotationPointY * scale, -this.right_back_top_leg.rotationPointZ * scale);
        this.right_back_top_leg.render(scale);
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        GlStateManager.translate(this.right_back_bottom_leg.offsetX, this.right_back_bottom_leg.offsetY, this.right_back_bottom_leg.offsetZ);
        GlStateManager.translate(this.right_back_bottom_leg.rotationPointX * scale, this.right_back_bottom_leg.rotationPointY * scale, this.right_back_bottom_leg.rotationPointZ * scale);
        GlStateManager.scale(1.0F, 1.0F, 1.0F);
        GlStateManager.translate(-this.right_back_bottom_leg.offsetX, -this.right_back_bottom_leg.offsetY, -this.right_back_bottom_leg.offsetZ);
        GlStateManager.translate(-this.right_back_bottom_leg.rotationPointX * scale, -this.right_back_bottom_leg.rotationPointY * scale, -this.right_back_bottom_leg.rotationPointZ * scale);
        this.right_back_bottom_leg.render(scale);
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        GlStateManager.translate(this.left_front_top_leg.offsetX, this.left_front_top_leg.offsetY, this.left_front_top_leg.offsetZ);
        GlStateManager.translate(this.left_front_top_leg.rotationPointX * scale, this.left_front_top_leg.rotationPointY * scale, this.left_front_top_leg.rotationPointZ * scale);
        GlStateManager.scale(1.0F, 1.0F, 1.0F);
        GlStateManager.translate(-this.left_front_top_leg.offsetX, -this.left_front_top_leg.offsetY, -this.left_front_top_leg.offsetZ);
        GlStateManager.translate(-this.left_front_top_leg.rotationPointX * scale, -this.left_front_top_leg.rotationPointY * scale, -this.left_front_top_leg.rotationPointZ * scale);
        this.left_front_top_leg.render(scale);
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        GlStateManager.translate(this.left_front_bottom_leg.offsetX, this.left_front_bottom_leg.offsetY, this.left_front_bottom_leg.offsetZ);
        GlStateManager.translate(this.left_front_bottom_leg.rotationPointX * scale, this.left_front_bottom_leg.rotationPointY * scale, this.left_front_bottom_leg.rotationPointZ * scale);
        GlStateManager.scale(1.0F, 1.0F, 1.0F);
        GlStateManager.translate(-this.left_front_bottom_leg.offsetX, -this.left_front_bottom_leg.offsetY, -this.left_front_bottom_leg.offsetZ);
        GlStateManager.translate(-this.left_front_bottom_leg.rotationPointX * scale, -this.left_front_bottom_leg.rotationPointY * scale, -this.left_front_bottom_leg.rotationPointZ * scale);
        this.left_front_bottom_leg.render(scale);
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        GlStateManager.translate(this.left_middle_top_leg.offsetX, this.left_middle_top_leg.offsetY, this.left_middle_top_leg.offsetZ);
        GlStateManager.translate(this.left_middle_top_leg.rotationPointX * scale, this.left_middle_top_leg.rotationPointY * scale, this.left_middle_top_leg.rotationPointZ * scale);
        GlStateManager.scale(1.0F, 1.0F, 1.0F);
        GlStateManager.translate(-this.left_middle_top_leg.offsetX, -this.left_middle_top_leg.offsetY, -this.left_middle_top_leg.offsetZ);
        GlStateManager.translate(-this.left_middle_top_leg.rotationPointX * scale, -this.left_middle_top_leg.rotationPointY * scale, -this.left_middle_top_leg.rotationPointZ * scale);
        this.left_middle_top_leg.render(scale);
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        GlStateManager.translate(this.left_middle_bottom_leg.offsetX, this.left_middle_bottom_leg.offsetY, this.left_middle_bottom_leg.offsetZ);
        GlStateManager.translate(this.left_middle_bottom_leg.rotationPointX * scale, this.left_middle_bottom_leg.rotationPointY * scale, this.left_middle_bottom_leg.rotationPointZ * scale);
        GlStateManager.scale(1.0F, 1.0F, 1.0F);
        GlStateManager.translate(-this.left_middle_bottom_leg.offsetX, -this.left_middle_bottom_leg.offsetY, -this.left_middle_bottom_leg.offsetZ);
        GlStateManager.translate(-this.left_middle_bottom_leg.rotationPointX * scale, -this.left_middle_bottom_leg.rotationPointY * scale, -this.left_middle_bottom_leg.rotationPointZ * scale);
        this.left_middle_bottom_leg.render(scale);
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        GlStateManager.translate(this.left_back_top_leg.offsetX, this.left_back_top_leg.offsetY, this.left_back_top_leg.offsetZ);
        GlStateManager.translate(this.left_back_top_leg.rotationPointX * scale, this.left_back_top_leg.rotationPointY * scale, this.left_back_top_leg.rotationPointZ * scale);
        GlStateManager.scale(1.0F, 1.0F, 1.0F);
        GlStateManager.translate(-this.left_back_top_leg.offsetX, -this.left_back_top_leg.offsetY, -this.left_back_top_leg.offsetZ);
        GlStateManager.translate(-this.left_back_top_leg.rotationPointX * scale, -this.left_back_top_leg.rotationPointY * scale, -this.left_back_top_leg.rotationPointZ * scale);
        this.left_back_top_leg.render(scale);
        GlStateManager.popMatrix();
        GlStateManager.pushMatrix();
        GlStateManager.translate(this.left_back_bottom_leg.offsetX, this.left_back_bottom_leg.offsetY, this.left_back_bottom_leg.offsetZ);
        GlStateManager.translate(this.left_back_bottom_leg.rotationPointX * scale, this.left_back_bottom_leg.rotationPointY * scale, this.left_back_bottom_leg.rotationPointZ * scale);
        GlStateManager.scale(1.0F, 1.0F, 1.0F);
        GlStateManager.translate(-this.left_back_bottom_leg.offsetX, -this.left_back_bottom_leg.offsetY, -this.left_back_bottom_leg.offsetZ);
        GlStateManager.translate(-this.left_back_bottom_leg.rotationPointX * scale, -this.left_back_bottom_leg.rotationPointY * scale, -this.left_back_bottom_leg.rotationPointZ * scale);
        this.left_back_bottom_leg.render(scale);
        GlStateManager.popMatrix();
    }

    @Override
    public void setRotationAngles(float p_78087_1_, float p_78087_2_, float p_78087_3_, float p_78087_4_, float p_78087_5_, float p_78087_6_, Entity entityIn) {
    	 
    	this.right_front_top_leg.rotateAngleY = MathHelper.cos(p_78087_1_ * 1.6662F) * 0.4F * p_78087_2_ + 0.1F;
    	this.right_front_bottom_leg.rotateAngleY = MathHelper.cos(p_78087_1_ * 1.6662F) * 0.4F * p_78087_2_ + 0.1F;
    	
    	this.right_middle_top_leg.rotateAngleY = MathHelper.cos(p_78087_1_ * 1.6662F + 0.5F) * 0.4F * p_78087_2_ - 0.13F;
    	this.right_middle_bottom_leg.rotateAngleY = MathHelper.cos(p_78087_1_ * 1.6662F + 0.5F) * 0.4F * p_78087_2_ - 0.13F;
    	
    	this.right_back_top_leg.rotateAngleY = MathHelper.cos(p_78087_1_ * 1.6662F + 1) * 0.4F * p_78087_2_ - 0.3F;
    	this.right_back_bottom_leg.rotateAngleY = MathHelper.cos(p_78087_1_ * 1.6662F + 1) * 0.4F * p_78087_2_ - 0.3F;
    	
    	this.left_front_top_leg.rotateAngleY = MathHelper.cos(p_78087_1_ * 1.6662F) * 0.4F * p_78087_2_ - 0.1F;
    	this.left_front_bottom_leg.rotateAngleY = MathHelper.cos(p_78087_1_ * 1.6662F) * 0.4F * p_78087_2_ - 0.1F;
    	
    	this.left_middle_top_leg.rotateAngleY = MathHelper.cos(p_78087_1_ * 1.6662F + 0.5F) * 0.4F * p_78087_2_ + 0.13F;
    	this.left_middle_bottom_leg.rotateAngleY = MathHelper.cos(p_78087_1_ * 1.6662F + 0.5F) * 0.4F * p_78087_2_ + 0.13F;
    	
    	this.left_back_top_leg.rotateAngleY = MathHelper.cos(p_78087_1_ * 1.6662F + 1) * 0.4F * p_78087_2_ + 0.3F;
    	this.left_back_bottom_leg.rotateAngleY = MathHelper.cos(p_78087_1_ * 1.6662F + 1) * 0.4F * p_78087_2_ + 0.3F;
    	
    	super.setRotationAngles(p_78087_1_, p_78087_2_, p_78087_3_, p_78087_4_, p_78087_5_, p_78087_6_, entityIn);
    }
    
    public void setRotationAngles(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}
