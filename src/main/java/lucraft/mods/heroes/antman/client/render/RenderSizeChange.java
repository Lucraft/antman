package lucraft.mods.heroes.antman.client.render;

import java.lang.reflect.Method;

import lucraft.mods.heroes.antman.entity.EntitySizeChange;
import lucraft.mods.lucraftcore.access.LucraftForgeLoading;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.client.resources.DefaultPlayerSkin;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;

@SuppressWarnings("rawtypes")
public class RenderSizeChange extends RendererLivingEntity {
	
	public RenderSizeChange(ModelBase par1ModelBase, float par2) {
		super(Minecraft.getMinecraft().getRenderManager(), par1ModelBase, par2);
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity) {
		if (entity instanceof EntitySizeChange) {
			setMainModel(((EntitySizeChange) entity).model);
		}
		RenderManager manager = Minecraft.getMinecraft().getRenderManager();
		return invokeGetEntityTexture(manager.getEntityRenderObject(((EntitySizeChange) entity).acquired), manager.getEntityRenderObject(((EntitySizeChange) entity).acquired).getClass(), ((EntitySizeChange) entity).acquired);
	}

	public static ResourceLocation invokeGetEntityTexture(Render rend, Class clz, EntityLivingBase ent) {
		ResourceLocation loc = getEntTexture(rend, clz, ent);
		if (loc != null) {
			return loc;
		}
		return DefaultPlayerSkin.getDefaultSkinLegacy();
	}

	@SuppressWarnings({ "unchecked" })
	private static ResourceLocation getEntTexture(Render rend, Class clz, EntityLivingBase ent) {
		try {
			Method m = clz.getDeclaredMethod(LucraftForgeLoading.runtimeObfuscationEnabled ? "getEntityTexture" : "func_110775_a", Entity.class);
			m.setAccessible(true);
			return (ResourceLocation) m.invoke(rend, ent);
		} catch (NoSuchMethodException e) {
			if (clz != RendererLivingEntity.class) {
				return invokeGetEntityTexture(rend, clz.getSuperclass(), ent);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return DefaultPlayerSkin.getDefaultSkinLegacy();
	}

	public void passSpecialRender(EntityLivingBase par1EntityLivingBase, double par2, double par4, double par6) {
	}

	@SuppressWarnings("unchecked")
	@Override
	public void doRender(EntityLivingBase entity, double par2, double par4, double par6, float par8, float par9) {
		super.doRender(entity, par2, par4, par6, par8, par9);
	}

	@Override
	public void renderName(EntityLivingBase arg0, double arg1, double arg2, double arg3) {
		
	}
	
	@Override
	protected void preRenderCallback(EntityLivingBase ent, float renderTick) {
		if (ent instanceof EntitySizeChange) {
			invokePreRenderCallback((((EntitySizeChange) ent).model).entRenderer, (((EntitySizeChange) ent).model).entRenderer.getClass(), (((EntitySizeChange) ent).model).SizeChangeEnt.acquired, renderTick);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void invokePreRenderCallback(Render rend, Class clz, Entity ent, float rendTick)
	{
		if(!(rend instanceof RendererLivingEntity) || !(ent instanceof EntityLivingBase))
		{
			return;
		}
		try
		{
			Method m = clz.getDeclaredMethod(LucraftForgeLoading.runtimeObfuscationEnabled ? "preRenderCallback" : "func_77041_b", EntityLivingBase.class, float.class);
			m.setAccessible(true);
			m.invoke(rend, ent, rendTick);
		}
		catch(NoSuchMethodException e)
		{
			if(clz != RendererLivingEntity.class)
			{
				invokePreRenderCallback(rend, clz.getSuperclass(), ent, rendTick);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void setMainModel(ModelBase base) {
		mainModel = base;
	}
}