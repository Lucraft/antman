package lucraft.mods.heroes.antman.client.gui;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import lucraft.mods.heroes.antman.AntMan;
import lucraft.mods.heroes.antman.container.ContainerPymParticleProducer;
import lucraft.mods.heroes.antman.fluids.AMFluids;
import lucraft.mods.heroes.antman.tileentity.TileEntityPymParticleProducer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Items;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiPymParticleProducer extends GuiContainer {

	private static final ResourceLocation producerGuiTextures = new ResourceLocation(AntMan.ASSETDIR + "textures/gui/pymParticleProducer.png");
	private static final ResourceLocation producerGuiProgressTextures = new ResourceLocation(AntMan.ASSETDIR + "textures/gui/pymParticleProducer_progress.png");
	private TileEntityPymParticleProducer tileProducer;

	public GuiPymParticleProducer(InventoryPlayer playerInv, TileEntityPymParticleProducer producer) {
		super(new ContainerPymParticleProducer(playerInv, producer));
		this.tileProducer = producer;
		this.xSize = 208;
		this.ySize = 183;
	}

	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {

	}
	
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawScreen(mouseX, mouseY, partialTicks);
		this.drawToolTips(mouseX, mouseY, FluidRegistry.WATER, 9, 30);
		this.drawToolTips(mouseX, mouseY, AMFluids.shrinkPymParticles, 143, 30);
		this.drawToolTips(mouseX, mouseY, AMFluids.growPymParticles, 164, 30);
	}
	
	public void drawToolTips(int mouseX, int mouseY, Fluid fluid, int x, int y) {
		int boxX = (this.width - this.xSize) / 2 + x;
		int boxY = (this.height - this.ySize) / 2 + y;

		int defaultX = 16;
		int defaultY = 60;

		if (mouseX > boxX && mouseX < boxX + defaultX && mouseY > boxY && mouseY < boxY + defaultY) {
			List<String> list = new ArrayList<String>();

			if (fluid == FluidRegistry.WATER)
				list.add(StatCollector.translateToLocal("tile.water.name") + ": " + getFluidAmount(tileProducer.water) + "/" + tileProducer.capacity + " mB");

			if (fluid == AMFluids.shrinkPymParticles) {
				list.add(StatCollector.translateToLocal("fluid.shrinkpymparticles") + ": " + getFluidAmount(tileProducer.shrinkPymParticles) + "/" + tileProducer.capacity + " mB");
			}
			
			if (fluid == AMFluids.growPymParticles) {
				list.add(StatCollector.translateToLocal("fluid.growpymparticles") + ": " + getFluidAmount(tileProducer.growPymParticles) + "/" + tileProducer.capacity + " mB");
			}

			this.drawHoveringText(list, mouseX, mouseY, fontRendererObj);
		}
	}
	
	private int getFluidAmount(FluidTank tank) {
		if(tank == null)
			return 0;
		else
			return tank.getFluidAmount();
	}
	
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(producerGuiTextures);
		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);

		if (TileEntityFurnace.isBurning(this.tileProducer)) {
			int k = this.getBurnLeftScaled(13);
			this.drawTexturedModalRect(i + 97, j + 32 + 12 - k, 208, 12 - k, 14, k + 1);
		}

		mc.getTextureManager().bindTexture(producerGuiProgressTextures);
		
		if(tileProducer.getStackInSlot(2) != null && tileProducer.getStackInSlot(2).getItem() == Items.redstone) {
			int l = this.getCookProgressScaled(88);
			this.drawTexturedModalRect(i + 58, j + 4, 0, 0, l, 78);
		} else if(tileProducer.getStackInSlot(2) != null && tileProducer.getStackInSlot(2).getItem() == Items.dye) {
			int l = this.getCookProgressScaled(109);
			this.drawTexturedModalRect(i + 58, j + 4, 106, 0, l, 78);
		}

		mc.getTextureManager().bindTexture(producerGuiTextures);
		
		renderTiledFluid(i + 9, j + 30, 16, 1, this.tileProducer.water);
		renderTiledFluid(i + 143, j + 30, 16, 1, this.tileProducer.shrinkPymParticles);
		renderTiledFluid(i + 164, j + 30, 16, 1, this.tileProducer.growPymParticles);
	}

	private int getCookProgressScaled(int pixels) {
		int i = this.tileProducer.getField(2);
		int j = this.tileProducer.getField(3);
		return j != 0 && i != 0 ? i * pixels / j : 0;
	}

	private int getBurnLeftScaled(int pixels) {
		int i = this.tileProducer.getField(1);

		if (i == 0) {
			i = 200;
		}

		return this.tileProducer.getField(0) * pixels / i;
	}

	public void renderTiledFluid(int x, int y, int width, float depth, FluidTank fluidStack) {
		if (fluidStack != null && fluidStack.getFluid() != null && fluidStack.getFluid().getFluid() != null) {
			float fluidTankHeight = 60F;
			float o = fluidTankHeight / fluidStack.getCapacity();
			int h = Math.round(o * fluidStack.getFluidAmount());

			int x2 = x;
			int y2 = (int) (y + fluidTankHeight - h);

			TextureAtlasSprite fluidSprite = mc.getTextureMapBlocks().getAtlasSprite(fluidStack.getFluid().getFluid().getStill(fluidStack.getFluid()).toString());
			setColorRGBA(fluidStack.getFluid().getFluid().getColor(fluidStack.getFluid()));
			renderTiledTextureAtlas(x2, y2, width, h, depth, fluidSprite);
			
			mc.renderEngine.bindTexture(producerGuiTextures);
			drawTexturedModalRect(x - 1, y - 1, 238, 0, 18, 62);
		}

	}

	protected static Minecraft mc = Minecraft.getMinecraft();

	public static void renderTiledTextureAtlas(int x, int y, int width, int height, float depth, TextureAtlasSprite sprite) {
		Tessellator tessellator = Tessellator.getInstance();
		WorldRenderer worldrenderer = tessellator.getWorldRenderer();
		worldrenderer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		mc.renderEngine.bindTexture(TextureMap.locationBlocksTexture);

		putTiledTextureQuads(worldrenderer, x, y, width, height, depth, sprite);

		tessellator.draw();
	}

	public static void putTiledTextureQuads(WorldRenderer renderer, int x, int y, int width, int height, float depth, TextureAtlasSprite sprite) {
		float u1 = sprite.getMinU();
		float v1 = sprite.getMinV();

		do {
			int renderHeight = Math.min(sprite.getIconHeight(), height);
			height -= renderHeight;

			float v2 = sprite.getInterpolatedV((16f * renderHeight) / (float) sprite.getIconHeight());

			int x2 = x;
			int width2 = width;
			do {
				int renderWidth = Math.min(sprite.getIconWidth(), width2);
				width2 -= renderWidth;

				float u2 = sprite.getInterpolatedU((16f * renderWidth) / (float) sprite.getIconWidth());

				renderer.pos(x2, y, depth).tex(u1, v1).endVertex();
				renderer.pos(x2, y + renderHeight, depth).tex(u1, v2).endVertex();
				renderer.pos(x2 + renderWidth, y + renderHeight, depth).tex(u2, v2).endVertex();
				renderer.pos(x2 + renderWidth, y, depth).tex(u2, v1).endVertex();

				x2 += renderWidth;
			} while (width2 > 0);

			y += renderHeight;
		} while (height > 0);
	}

	public static void setColorRGBA(int color) {
		float a = (float) alpha(color) / 255.0F;
		float r = (float) red(color) / 255.0F;
		float g = (float) green(color) / 255.0F;
		float b = (float) blue(color) / 255.0F;

		GlStateManager.color(r, g, b, a);
	}

	public static int alpha(int c) {
		return (c >> 24) & 0xFF;
	}

	public static int red(int c) {
		return (c >> 16) & 0xFF;
	}

	public static int green(int c) {
		return (c >> 8) & 0xFF;
	}

	public static int blue(int c) {
		return (c) & 0xFF;
	}

}