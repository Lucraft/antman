package lucraft.mods.heroes.antman.client.render;

import java.util.Map;

import org.lwjgl.opengl.GL11;

import com.google.common.collect.Maps;

import lucraft.mods.heroes.antman.client.models.ModelSizeChange;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class LayerSizeChangePlayerBipedArmor<T extends ModelBase> implements LayerRenderer<EntityLivingBase> {

	protected static final ResourceLocation ENCHANTED_ITEM_GLINT_RES = new ResourceLocation("textures/misc/enchanted_item_glint.png");
	protected T field_177189_c;
	protected T field_177186_d;
	private final RenderSizeChangePlayer renderer;
	private float colorR = 1.0F;
	private float colorG = 1.0F;
	private float colorB = 1.0F;
	private boolean field_177193_i;
	private static final Map<String, ResourceLocation> ARMOR_TEXTURE_RES_MAP = Maps.<String, ResourceLocation> newHashMap();

	public LayerSizeChangePlayerBipedArmor(RenderSizeChangePlayer rendererIn) {
		this.renderer = rendererIn;
		this.initArmor();
	}

	protected void initArmor() {
		this.field_177189_c = (T) new ModelBiped(0.5F);
		this.field_177186_d = (T) new ModelBiped(1.0F);
	}

	protected void func_177179_a(ModelBiped p_177179_1_, int p_177179_2_) {
		this.func_177194_a(p_177179_1_);

		switch (p_177179_2_) {
		case 1:
			p_177179_1_.bipedRightLeg.showModel = true;
			p_177179_1_.bipedLeftLeg.showModel = true;
			break;
		case 2:
			p_177179_1_.bipedBody.showModel = true;
			p_177179_1_.bipedRightLeg.showModel = true;
			p_177179_1_.bipedLeftLeg.showModel = true;
			break;
		case 3:
			p_177179_1_.bipedBody.showModel = true;
			p_177179_1_.bipedRightArm.showModel = true;
			p_177179_1_.bipedLeftArm.showModel = true;
			break;
		case 4:
			p_177179_1_.bipedHead.showModel = true;
			p_177179_1_.bipedHeadwear.showModel = true;
		}
	}

	protected void func_177194_a(ModelBiped p_177194_1_) {
		p_177194_1_.setInvisible(false);
	}

	protected ModelBiped getArmorModelHook(net.minecraft.entity.EntityLivingBase entity, net.minecraft.item.ItemStack itemStack, int slot, ModelBiped model) {
		return net.minecraftforge.client.ForgeHooksClient.getArmorModel(entity, itemStack, slot, model);
	}

	public void doRenderLayer(EntityLivingBase entitylivingbaseIn, float p_177141_2_, float p_177141_3_, float partialTicks, float p_177141_5_, float p_177141_6_, float p_177141_7_, float scale) {
		GlStateManager.pushMatrix();
		GlStateManager.depthMask(true);
		GlStateManager.pushMatrix();
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		
		this.renderLayer(entitylivingbaseIn, p_177141_2_, p_177141_3_, partialTicks, p_177141_5_, p_177141_6_, p_177141_7_, scale, 4, renderer.alpha, true);
		this.renderLayer(entitylivingbaseIn, p_177141_2_, p_177141_3_, partialTicks, p_177141_5_, p_177141_6_, p_177141_7_, scale, 3, renderer.alpha, true);
		this.renderLayer(entitylivingbaseIn, p_177141_2_, p_177141_3_, partialTicks, p_177141_5_, p_177141_6_, p_177141_7_, scale, 2, renderer.alpha, true);
		this.renderLayer(entitylivingbaseIn, p_177141_2_, p_177141_3_, partialTicks, p_177141_5_, p_177141_6_, p_177141_7_, scale, 1, renderer.alpha, true);

		Minecraft.getMinecraft().renderEngine.bindTexture(ModelSizeChange.white);
		this.renderLayer(entitylivingbaseIn, p_177141_2_, p_177141_3_, partialTicks, p_177141_5_, p_177141_6_, p_177141_7_, scale, 4, renderer.alpha - 0.3F, false);
		this.renderLayer(entitylivingbaseIn, p_177141_2_, p_177141_3_, partialTicks, p_177141_5_, p_177141_6_, p_177141_7_, scale, 3, renderer.alpha - 0.3F, false);
		this.renderLayer(entitylivingbaseIn, p_177141_2_, p_177141_3_, partialTicks, p_177141_5_, p_177141_6_, p_177141_7_, scale, 2, renderer.alpha - 0.3F, false);
		this.renderLayer(entitylivingbaseIn, p_177141_2_, p_177141_3_, partialTicks, p_177141_5_, p_177141_6_, p_177141_7_, scale, 1, renderer.alpha - 0.3F, false);

		GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
		this.renderLayer(entitylivingbaseIn, p_177141_2_, p_177141_3_, partialTicks, p_177141_5_, p_177141_6_, p_177141_7_, scale, 4, renderer.alpha - 0.3F, false);
		this.renderLayer(entitylivingbaseIn, p_177141_2_, p_177141_3_, partialTicks, p_177141_5_, p_177141_6_, p_177141_7_, scale, 3, renderer.alpha - 0.3F, false);
		this.renderLayer(entitylivingbaseIn, p_177141_2_, p_177141_3_, partialTicks, p_177141_5_, p_177141_6_, p_177141_7_, scale, 2, renderer.alpha - 0.3F, false);
		this.renderLayer(entitylivingbaseIn, p_177141_2_, p_177141_3_, partialTicks, p_177141_5_, p_177141_6_, p_177141_7_, scale, 1, renderer.alpha - 0.3F, false);
		GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);

		GlStateManager.popMatrix();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.disableBlend();
		GlStateManager.popMatrix();
	}

	public boolean shouldCombineTextures() {
		return false;
	}

	@SuppressWarnings("unchecked")
	private void renderLayer(EntityLivingBase entitylivingbaseIn, float p_177182_2_, float p_177182_3_, float p_177182_4_, float p_177182_5_, float p_177182_6_, float p_177182_7_, float p_177182_8_, int armorSlot, float alpha, boolean useArmorTex) {
		ItemStack itemstack = this.getCurrentArmor(entitylivingbaseIn, armorSlot);

		if (itemstack != null && itemstack.getItem() instanceof ItemArmor) {
			ItemArmor itemarmor = (ItemArmor) itemstack.getItem();
			T t = this.func_177175_a(armorSlot);
			t.setModelAttributes(this.renderer.getMainModel());
			t.setLivingAnimations(entitylivingbaseIn, p_177182_2_, p_177182_3_, p_177182_4_);
			t = (T) getArmorModelHook(entitylivingbaseIn, itemstack, armorSlot, (ModelBiped) t);
			this.func_177179_a((ModelBiped) t, armorSlot);
			boolean flag = this.isSlotForLeggings(armorSlot);
			if (useArmorTex)
				this.renderer.bindTexture(this.getArmorResource(entitylivingbaseIn, itemstack, flag ? 2 : 1, null));

			int i = itemarmor.getColor(itemstack);
			{
				if (i != -1) // Allow this for anything, not only cloth.
				{
					float f = (float) (i >> 16 & 255) / 255.0F;
					float f1 = (float) (i >> 8 & 255) / 255.0F;
					float f2 = (float) (i & 255) / 255.0F;
					GlStateManager.color(this.colorR * f, this.colorG * f1, this.colorB * f2, alpha);
					t.render(entitylivingbaseIn, p_177182_2_, p_177182_3_, p_177182_5_, p_177182_6_, p_177182_7_, p_177182_8_);
					if (useArmorTex)
						this.renderer.bindTexture(this.getArmorResource(entitylivingbaseIn, itemstack, flag ? 2 : 1, "overlay"));
				}
				{ // Non-colored
					GlStateManager.color(this.colorR, this.colorG, this.colorB, alpha);
					t.render(entitylivingbaseIn, p_177182_2_, p_177182_3_, p_177182_5_, p_177182_6_, p_177182_7_, p_177182_8_);
				}
				// Default
				if (!this.field_177193_i && itemstack.hasEffect()) {
					this.func_177183_a(entitylivingbaseIn, t, p_177182_2_, p_177182_3_, p_177182_4_, p_177182_5_, p_177182_6_, p_177182_7_, p_177182_8_);
				}
			}
		}
	}

	public ItemStack getCurrentArmor(EntityLivingBase entitylivingbaseIn, int armorSlot) {
		return entitylivingbaseIn.getCurrentArmor(armorSlot - 1);
	}

	public T func_177175_a(int p_177175_1_) {
		return (T) (this.isSlotForLeggings(p_177175_1_) ? this.field_177189_c : this.field_177186_d);
	}

	private boolean isSlotForLeggings(int armorSlot) {
		return armorSlot == 2;
	}

	private void func_177183_a(EntityLivingBase entitylivingbaseIn, T modelbaseIn, float p_177183_3_, float p_177183_4_, float p_177183_5_, float p_177183_6_, float p_177183_7_, float p_177183_8_, float p_177183_9_) {
		float f = (float) entitylivingbaseIn.ticksExisted + p_177183_5_;
		this.renderer.bindTexture(ENCHANTED_ITEM_GLINT_RES);
		GlStateManager.enableBlend();
		GlStateManager.depthFunc(514);
		GlStateManager.depthMask(false);
		float f1 = 0.5F;
		GlStateManager.color(f1, f1, f1, 1.0F);

		for (int i = 0; i < 2; ++i) {
			GlStateManager.disableLighting();
			GlStateManager.blendFunc(768, 1);
			float f2 = 0.76F;
			GlStateManager.color(0.5F * f2, 0.25F * f2, 0.8F * f2, 1.0F);
			GlStateManager.matrixMode(5890);
			GlStateManager.loadIdentity();
			float f3 = 0.33333334F;
			GlStateManager.scale(f3, f3, f3);
			GlStateManager.rotate(30.0F - (float) i * 60.0F, 0.0F, 0.0F, 1.0F);
			GlStateManager.translate(0.0F, f * (0.001F + (float) i * 0.003F) * 20.0F, 0.0F);
			GlStateManager.matrixMode(5888);
			modelbaseIn.render(entitylivingbaseIn, p_177183_3_, p_177183_4_, p_177183_6_, p_177183_7_, p_177183_8_, p_177183_9_);
		}

		GlStateManager.matrixMode(5890);
		GlStateManager.loadIdentity();
		GlStateManager.matrixMode(5888);
		GlStateManager.enableLighting();
		GlStateManager.depthMask(true);
		GlStateManager.depthFunc(515);
		GlStateManager.disableBlend();
	}

	@Deprecated // Use the more sensitive version getArmorResource below
	private ResourceLocation getArmorResource(ItemArmor p_177181_1_, boolean p_177181_2_) {
		return this.getArmorResource(p_177181_1_, p_177181_2_, (String) null);
	}

	@Deprecated // Use the more sensitive version getArmorResource below
	private ResourceLocation getArmorResource(ItemArmor p_177178_1_, boolean p_177178_2_, String p_177178_3_) {
		String s = String.format("textures/models/armor/%s_layer_%d%s.png", new Object[] { p_177178_1_.getArmorMaterial().getName(), Integer.valueOf(p_177178_2_ ? 2 : 1), p_177178_3_ == null ? "" : String.format("_%s", new Object[] { p_177178_3_ }) });
		ResourceLocation resourcelocation = (ResourceLocation) ARMOR_TEXTURE_RES_MAP.get(s);

		if (resourcelocation == null) {
			resourcelocation = new ResourceLocation(s);
			ARMOR_TEXTURE_RES_MAP.put(s, resourcelocation);
		}

		return resourcelocation;
	}

	/*
	 * =================================== FORGE START
	 * =========================================
	 */

	/**
	 * Hook to allow item-sensitive armor model. for LayerBipedArmor.
	 */

	/**
	 * More generic ForgeHook version of the above function, it allows for Items
	 * to have more control over what texture they provide.
	 *
	 * @param entity
	 *            Entity wearing the armor
	 * @param stack
	 *            ItemStack for the armor
	 * @param slot
	 *            Slot ID that the item is in
	 * @param type
	 *            Subtype, can be null or "overlay"
	 * @return ResourceLocation pointing at the armor's texture
	 */
	public ResourceLocation getArmorResource(net.minecraft.entity.Entity entity, ItemStack stack, int slot, String type) {
		ItemArmor item = (ItemArmor) stack.getItem();
		String texture = item.getArmorMaterial().getName();
		String domain = "minecraft";
		int idx = texture.indexOf(':');
		if (idx != -1) {
			domain = texture.substring(0, idx);
			texture = texture.substring(idx + 1);
		}
		String s1 = String.format("%s:textures/models/armor/%s_layer_%d%s.png", domain, texture, (slot == 2 ? 2 : 1), type == null ? "" : String.format("_%s", type));

		s1 = net.minecraftforge.client.ForgeHooksClient.getArmorTexture(entity, stack, s1, slot, type);
		ResourceLocation resourcelocation = (ResourceLocation) ARMOR_TEXTURE_RES_MAP.get(s1);

		if (resourcelocation == null) {
			resourcelocation = new ResourceLocation(s1);
			ARMOR_TEXTURE_RES_MAP.put(s1, resourcelocation);
		}

		return resourcelocation;
	}
}