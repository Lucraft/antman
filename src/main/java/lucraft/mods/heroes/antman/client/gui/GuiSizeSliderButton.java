package lucraft.mods.heroes.antman.client.gui;

import lucraft.mods.heroes.antman.util.AntManHelper;
import lucraft.mods.lucraftcore.util.LucraftCoreUtil;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.MathHelper;
import net.minecraft.util.StatCollector;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class GuiSizeSliderButton extends GuiButton {
	
	public float sliderValue;
	public boolean dragging;

	public GuiSizeSliderButton(int i, int j, int k) {
		this(i, j, k, 0.0F, 9.0F);
	}

	public GuiSizeSliderButton(int p_i45017_1_, int p_i45017_2_, int p_i45017_3_, float p_i45017_5_, float p_i45017_6_) {
		super(p_i45017_1_, p_i45017_2_, p_i45017_3_, 200, 20, "");
		this.sliderValue = toSliderValue(AntManHelper.getSavedScaleFromChestplate(Minecraft.getMinecraft().thePlayer));
		updateText();
	}

	protected int getHoverState(boolean mouseOver) {
		return 0;
	}

	protected void mouseDragged(Minecraft mc, int mouseX, int mouseY) {
		if (this.visible) {
			if (this.dragging) {
				this.sliderValue = (float) (mouseX - (this.xPosition + 4)) / (float) (this.width - 8);
				this.sliderValue = MathHelper.clamp_float(this.sliderValue, 0.0F, 1.0F);
				GuiSizeRegulator.size = (float) getValue();
			}

			mc.getTextureManager().bindTexture(buttonTextures);
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
			this.drawTexturedModalRect(this.xPosition + (int) (this.sliderValue * (float) (this.width - 8)), this.yPosition, 0, 66, 4, 20);
			this.drawTexturedModalRect(this.xPosition + (int) (this.sliderValue * (float) (this.width - 8)) + 4, this.yPosition, 196, 66, 4, 20);
			updateText();
		}
	}

	public boolean mousePressed(Minecraft mc, int mouseX, int mouseY) {
		if (super.mousePressed(mc, mouseX, mouseY)) {
			this.sliderValue = (float) (mouseX - (this.xPosition + 4)) / (float) (this.width - 8);
			this.sliderValue = MathHelper.clamp_float(this.sliderValue, 0.0F, 1.0F);
			this.dragging = true;
			return true;
		} else {
			return false;
		}
	}

	public void mouseReleased(int mouseX, int mouseY) {
		this.dragging = false;
	}

	public void updateText() {
		this.displayString = StatCollector.translateToLocal("antman.info.size") + ": " + getValue();
	}

	public double getValue() {
		float x = 1.0F;

		if (sliderValue <= 0.5F) {
			float minValue = 0.11F;
			float f = 1F - minValue;
			x = minValue + ((f * sliderValue * 2) / 1);
		}

		if (sliderValue > 0.5F) {
			float f = sliderValue - 0.5F;
			x = 1F + (f * 2 * 9F);
		}

		return LucraftCoreUtil.round(x, 2);
	}
	
	public float toSliderValue(float f) {
		float x = f;
		
		if(x <= 1F) {
			float minValue = 0.11F;
			float g = 1F - minValue;
			x = ((x - minValue) / g) / 2;
		}
		
		if(x > 1F) {
			x = ((x - 1) / 18) + 0.5F;
		}
		
		return x;
	}
}