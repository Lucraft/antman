package lucraft.mods.heroes.antman.client.render;

import lucraft.mods.heroes.antman.AntMan;
import lucraft.mods.heroes.antman.client.models.ModelAnt;
import lucraft.mods.heroes.antman.entity.EntityBulletAnt;
import lucraft.mods.heroes.antman.entity.EntityBulletAnt;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.util.ResourceLocation;

public class RenderBulletAnt extends RendererLivingEntity<EntityBulletAnt> {

	private static final ResourceLocation texBulletAnt = new ResourceLocation(AntMan.ASSETDIR + "textures/models/entity/bulletAnt.png");
	private static final ResourceLocation texCrazyAnt = new ResourceLocation(AntMan.ASSETDIR + "textures/models/entity/crazyAnt.png");

	public RenderBulletAnt() {
		super(Minecraft.getMinecraft().getRenderManager(), new ModelAnt(), 0.05F);
	}

	@Override
	protected void renderLivingLabel(EntityBulletAnt entityIn, String str, double x, double y, double z, int maxDistance) {
		if(entityIn.getCustomNameTag() != "")
			super.renderLivingLabel(entityIn, str, x, y, z, maxDistance);
	}
	
	protected void renderModel(EntityBulletAnt entitylivingbaseIn, float p_77036_2_, float p_77036_3_, float p_77036_4_, float p_77036_5_, float p_77036_6_, float p_77036_7_) {
		boolean flag = !entitylivingbaseIn.isInvisible();
		boolean flag1 = !flag && !entitylivingbaseIn.isInvisibleToPlayer(Minecraft.getMinecraft().thePlayer);
		
		if (flag || flag1) {
			if (!this.bindEntityTexture(entitylivingbaseIn)) {
				return;
			}

			if (flag1) {
				GlStateManager.pushMatrix();
				GlStateManager.color(1.0F, 1.0F, 1.0F, 0.15F);
				GlStateManager.depthMask(false);
				GlStateManager.enableBlend();
				GlStateManager.blendFunc(770, 771);
				GlStateManager.alphaFunc(516, 0.003921569F);
			}
			
			float scale = 0.4F;
			GlStateManager.scale(scale, scale, scale);
			GlStateManager.translate(0, 2.3F, 0);
			this.mainModel.render(entitylivingbaseIn, p_77036_2_, p_77036_3_, p_77036_4_, p_77036_5_, p_77036_6_, p_77036_7_);

			if (flag1) {
				GlStateManager.disableBlend();
				GlStateManager.alphaFunc(516, 0.1F);
				GlStateManager.popMatrix();
				GlStateManager.depthMask(true);
			}
		}
	}

	@Override
	protected ResourceLocation getEntityTexture(EntityBulletAnt entity) {
		return entity instanceof EntityBulletAnt ? texBulletAnt : texCrazyAnt;
	}

}
