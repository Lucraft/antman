package lucraft.mods.heroes.antman.client.render;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import com.mojang.realmsclient.gui.ChatFormatting;

import lucraft.mods.heroes.antman.client.gui.GuiSizeRegulator;
import lucraft.mods.heroes.antman.entity.AntManEntityData;
import lucraft.mods.heroes.antman.entity.EntitySizeChange;
import lucraft.mods.heroes.antman.items.AMItems;
import lucraft.mods.heroes.antman.items.ItemAntManArmor;
import lucraft.mods.heroes.antman.items.ItemAntManChestplate;
import lucraft.mods.heroes.antman.util.AMSizes;
import lucraft.mods.heroes.antman.util.AntManHelper;
import lucraft.mods.heroes.antman.util.IPymParticleContainer.PymParticleVersion;
import lucraft.mods.heroes.antman.util.ShrinkerArmorAbilities;
import lucraft.mods.heroes.antman.util.ShrinkerTypes;
import lucraft.mods.heroes.antman.util.ShrinkerTypesClientHelper;
import lucraft.mods.lucraftcore.access.LucraftForgeLoading;
import lucraft.mods.lucraftcore.events.RenderModelEvent;
import lucraft.mods.lucraftcore.events.SetupModelBipedEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.inventory.GuiContainerCreative;
import net.minecraft.client.gui.inventory.GuiInventory;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import net.minecraftforge.client.event.RenderLivingEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.common.gameevent.TickEvent.RenderTickEvent;
import net.minecraftforge.fml.relauncher.ReflectionHelper;

public class AMClientRenderer {

	public static int warnProgress = 0;

	private static Minecraft mc = Minecraft.getMinecraft();
	public static boolean showHud = true;

	@SubscribeEvent
	public void onRender(RenderTickEvent e) {
		if (e.phase == Phase.END && showHud)
			drawPymTankInfo(0, 4, (mc.currentScreen == null) && !mc.gameSettings.hideGUI && !mc.gameSettings.showDebugInfo, mc.ingameGUI, mc.getRenderItem());
	}

	@SubscribeEvent
	public void renderModel(RenderModelEvent e) {
		float scale = 1F;
		float transform = 0F;
		
		if(e.entityLiving instanceof EntityLivingBase) {
			if (AntManHelper.isSizeChangeable(e.entityLiving)) {
				AntManEntityData data = AntManEntityData.get(e.entityLiving);
				scale = data.size;

				if (e.entityLiving.isSneaking() && e.entityLiving == mc.thePlayer) {
					transform = -0.125F * scale + 0.125F;
				}

				if(mc.currentScreen instanceof GuiSizeRegulator)
					scale = 1;
				
				if (scale > 1 && (mc.currentScreen instanceof GuiInventory || mc.currentScreen instanceof GuiContainerCreative)) {
					scale = 1;
				}
				
				if(e.entityLiving instanceof EntityPlayer) {
					EntityPlayer player = (EntityPlayer) e.entityLiving;
					if(AntManHelper.hasArmorOn(player) && ((ItemAntManChestplate) AntManHelper.getAntManChestplate(player).getItem()).hasAbility(ShrinkerArmorAbilities.flight) && !player.onGround && !player.capabilities.isFlying) {
			            double d = Math.min(Math.sqrt((player.prevPosX - player.posX) * (player.prevPosX - player.posX) + (player.prevPosZ - player.posZ) * (player.prevPosZ - player.posZ)), 1.0D) * (player.moveForward == 0.0F ? 1.0F : player.moveForward);
			            GL11.glRotated(45.0D * d, 1.0D, 0.0D, 0.0D);
			            GlStateManager.translate(0, 0, 30 * d * 4 / 100 * scale);
					}
				}
				
			}
			
			if (e.entityLiving instanceof EntitySizeChange) {
				scale = ((EntitySizeChange) e.entityLiving).scale;
				transform = scale * (0.5F);
			}
			
			if(scale != 1F || transform != 0F) {
				GlStateManager.translate(0F, transform, 0F);
				GlStateManager.scale(scale, scale, scale);
			}
		}
	}

	@SubscribeEvent
	public void setupModel(SetupModelBipedEvent e) {
		if(AntManHelper.isSizeChangeable(e.entity)) {
			
			if(!AntManHelper.isNormalSize((EntityLivingBase) e.entity))
				e.f /= AntManEntityData.get((EntityLivingBase) e.entity).size;
			
			if(e.entity instanceof EntityPlayer && AntManHelper.hasArmorOn((EntityPlayer) e.entity) && ((ItemAntManChestplate) AntManHelper.getAntManChestplate((EntityPlayer) e.entity).getItem()).hasAbility(ShrinkerArmorAbilities.flight) && !e.entity.onGround && !((EntityPlayer)e.entity).capabilities.isFlying) {
				e.f1 = 0;
			}
		}
	}
	
	@SubscribeEvent
	public void renderSpecials(RenderLivingEvent.Specials.Pre<EntitySizeChange> e) {
		if(AntManEntityData.get(e.entity).size < AMSizes.normal / 3F) {
			e.setCanceled(true);
		}
	}

	public static final String[] thirdPersonDistanceNames = new String[] { "thirdPersonDistance", "field_78490_B" };
	public static final String[] cameraZoomNames = new String[] { "cameraZoom", "field_78503_V" };
	public static final String[] shadowSizeNames = new String[] { "shadowSize", "field_76989_e" };

	public static HashMap<Class<? extends EntityLivingBase>, Float> shadowSizes = new HashMap<Class<? extends EntityLivingBase>, Float>();

	@SubscribeEvent
	public void onRenderEntityPre(RenderLivingEvent.Pre<EntityLivingBase> e) {
		if (AntManHelper.isSizeChangeable(e.entity)) {
			if (!shadowSizes.containsKey(e.entity.getClass()))
				shadowSizes.put(e.entity.getClass(), (Float) ReflectionHelper.getPrivateValue(Render.class, e.renderer, LucraftForgeLoading.runtimeObfuscationEnabled ? shadowSizeNames[0] : shadowSizeNames[1]));

			float scale = AntManEntityData.get(e.entity).size;
			ReflectionHelper.setPrivateValue(Render.class, e.renderer, scale * shadowSizes.get(e.entity.getClass()), LucraftForgeLoading.runtimeObfuscationEnabled ? shadowSizeNames[0] : shadowSizeNames[1]);
		}
	}

	@SubscribeEvent
	public void onPlayerRenderPre(RenderPlayerEvent.Pre event) {
		float thirdPersonDistance = AntManEntityData.get(event.entityPlayer).size < 4 ? 4 * AntManEntityData.get(event.entityPlayer).size : 16;
		float cameraZoom = 1F;

		if (event.entityPlayer == Minecraft.getMinecraft().thePlayer) {
			EntityRenderer renderer = Minecraft.getMinecraft().entityRenderer;
			Field thirdPersonDistanceField = ReflectionHelper.findField(EntityRenderer.class, ObfuscationReflectionHelper.remapFieldNames(EntityRenderer.class.getName(), thirdPersonDistanceNames));
			try {
				Field modifier = Field.class.getDeclaredField("modifiers");
				modifier.setAccessible(true);
				modifier.setInt(thirdPersonDistanceField, thirdPersonDistanceField.getModifiers() & ~Modifier.FINAL);
				thirdPersonDistanceField.set(renderer, thirdPersonDistance);
			} catch (Exception e) {
				e.printStackTrace();
			}
			Field cameraZoomField = ReflectionHelper.findField(EntityRenderer.class, ObfuscationReflectionHelper.remapFieldNames(EntityRenderer.class.getName(), cameraZoomNames));
			try {
				Field modifier = Field.class.getDeclaredField("modifiers");
				modifier.setAccessible(true);
				modifier.setInt(cameraZoomField, cameraZoomField.getModifiers() & ~Modifier.FINAL);
				cameraZoomField.set(renderer, cameraZoom);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		GL11.glPushMatrix();
		if (event.entityPlayer.isSneaking()) {
			GL11.glTranslatef(0, 0.125F, 0);
			GL11.glTranslatef(0, -0.220F * AntManEntityData.get(event.entityPlayer).size, 0);

		}
	}

	@SubscribeEvent
	public void onPlayerRenderPost(RenderPlayerEvent.Post event) {
		GL11.glPopMatrix();
	}

	@SubscribeEvent
	public void toolTip(ItemTooltipEvent e) {
		boolean button = Keyboard.isKeyDown(mc.gameSettings.keyBindSneak.getKeyCode());

		if (e.itemStack.getItem() == AMItems.regulator && button) {
			e.toolTip.add(StatCollector.translateToLocal("antman.info.regulator1"));
			e.toolTip.add(StatCollector.translateToLocal("antman.info.regulator2"));
		}
		
		if(e.itemStack.getItem() == AMItems.waspWing) {
			e.toolTip.add(StatCollector.translateToLocal("antman.info.findinquantumrealm"));
		}
	}

	public static void drawPymTankInfo(int x, int y, boolean doRender, Gui gui, RenderItem renderItem) {
		if (mc.thePlayer != null && doRender) {
			if (AntManHelper.hasArmorOn(mc.thePlayer)) {
				
				GlStateManager.pushMatrix();
				GlStateManager.translate(x, y, 0D);
				
				GlStateManager.pushMatrix();
				EntityPlayer player = mc.thePlayer;
				ItemStack chestStack = player.getCurrentArmor(2);
				ItemAntManChestplate chestplate = (ItemAntManChestplate) chestStack.getItem();
				
				int shrink = (100 * chestplate.getPymParticles(PymParticleVersion.SHRINK, chestStack)) / chestplate.getMaxPymParticles(PymParticleVersion.SHRINK, chestStack);
				int grow = (100 * chestplate.getPymParticles(PymParticleVersion.GROW, chestStack)) / chestplate.getMaxPymParticles(PymParticleVersion.GROW, chestStack);
				
				GlStateManager.scale(2F, 2F, 2F);

				ItemStack inReg = AntManHelper.getItemInAntManArmorChestplate(chestStack);
				if(inReg != null) {
					renderItem.renderItemAndEffectIntoGUI(inReg, 4 / 2, 0);
				} else {
					ShrinkerTypes type = ((ItemAntManArmor)AntManHelper.getAntManChestplate(player).getItem()).getShrinkerType();
					mc.getTextureManager().bindTexture(ShrinkerTypesClientHelper.getHUDResourceLocation(type));
					gui.drawTexturedModalRect(4 / 2, 4 / 2, 0, 12, 16, 12);
				}
				
				ShrinkerTypes type = ((ItemAntManArmor)AntManHelper.getAntManChestplate(player).getItem()).getShrinkerType();
				mc.getTextureManager().bindTexture(ShrinkerTypesClientHelper.getHUDResourceLocation(type));

				gui.drawTexturedModalRect(40 / 2, 4 / 2, 0, 0, 52, 5);
				gui.drawTexturedModalRect(42 / 2, 6 / 2, 0, 5, shrink / 2, 3);

				gui.drawTexturedModalRect(40 / 2, 16 / 2, 0, 0, 52, 5);
				gui.drawTexturedModalRect(42 / 2, 18 / 2, 0, 8, grow / 2, 3);

				GlStateManager.popMatrix();
				mc.fontRendererObj.drawStringWithShadow(ShrinkerTypesClientHelper.getShrinkPymParticlesChatColor(type, false) + "" + shrink + ShrinkerTypesClientHelper.getShrinkPymParticlesChatColor(type, true) + "%", 150, 5, 0);
				mc.fontRendererObj.drawStringWithShadow(ChatFormatting.BLUE + "" + grow + ChatFormatting.DARK_BLUE + "%", 150, 17, 0);
				
				if (shrink <= 10 || grow <= 10) {
					mc.fontRendererObj.drawStringWithShadow(ChatFormatting.RED + StatCollector.translateToLocal("antman.info.refill"), 30, 29, 0);
				}
				
				GlStateManager.popMatrix();
			}
		}
	}

}
