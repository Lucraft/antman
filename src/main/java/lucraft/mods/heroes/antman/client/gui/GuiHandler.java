package lucraft.mods.heroes.antman.client.gui;

import lucraft.mods.heroes.antman.container.ContainerPymParticleProducer;
import lucraft.mods.heroes.antman.container.ContainerPymWorkbench;
import lucraft.mods.heroes.antman.container.ContainerRegulator;
import lucraft.mods.heroes.antman.items.InventoryRegulator;
import lucraft.mods.heroes.antman.tileentity.TileEntityPymParticleProducer;
import lucraft.mods.heroes.antman.tileentity.TileEntityPymWorkbench;
import lucraft.mods.heroes.antman.util.AntManHelper;
import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ContainerChest;
import net.minecraft.inventory.IInventory;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler {

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		BlockPos pos = new BlockPos(x, y, z);
		TileEntity tileEntity = world.getTileEntity(pos);

		switch (ID) {
		case GuiIDs.pymWorkbenchGuiId:
			return new ContainerPymWorkbench((TileEntityPymWorkbench) tileEntity, player.inventory, world, pos);
		case GuiIDs.pymParticleProducerGuiId:
			return new ContainerPymParticleProducer(player.inventory, (TileEntityPymParticleProducer) tileEntity);
		case GuiIDs.sizeRegulatorGuiId:
			return new ContainerRegulator(player, player.inventory, new InventoryRegulator(AntManHelper.getAntManChestplate(player)));
		case GuiIDs.quantumRealmChest:
			return new ContainerChest(player.inventory, (IInventory) tileEntity, player);
		default:
			return null;
		}
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		BlockPos pos = new BlockPos(x, y, z);
		TileEntity tileEntity = world.getTileEntity(pos);
		
		switch (ID) {
		case GuiIDs.pymWorkbenchGuiId:
			return new GuiPymWorkbench((TileEntityPymWorkbench) tileEntity, player.inventory, world, pos);
		case GuiIDs.pymParticleProducerGuiId:
			return new GuiPymParticleProducer(player.inventory, (TileEntityPymParticleProducer) tileEntity);
		case GuiIDs.sizeRegulatorGuiId:
			return new GuiSizeRegulator(new ContainerRegulator(player, player.inventory, new InventoryRegulator(AntManHelper.getAntManChestplate(player))));
		case GuiIDs.quantumRealmChest:
			return new GuiChest(player.inventory, (IInventory) tileEntity);
		default:
			return null;
		}
	}

}
