package lucraft.mods.heroes.antman.client.render;

import org.lwjgl.opengl.GL11;

import lucraft.mods.heroes.antman.AntMan;
import lucraft.mods.heroes.antman.client.models.ModelCarpenterAnt;
import lucraft.mods.heroes.antman.entity.EntityCarpenterAnt;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RendererLivingEntity;
import net.minecraft.util.ResourceLocation;

public class RenderCarpenterAnt extends RendererLivingEntity<EntityCarpenterAnt> {

	private static final ResourceLocation texCarpenterAnt = new ResourceLocation(AntMan.ASSETDIR + "textures/models/entity/carpenterAnt.png");

	public RenderCarpenterAnt() {
		super(Minecraft.getMinecraft().getRenderManager(), new ModelCarpenterAnt(), 0.05F);
	}

	@Override
	protected void renderLivingLabel(EntityCarpenterAnt entityIn, String str, double x, double y, double z, int maxDistance) {
		if(entityIn.getCustomNameTag() != "")
			super.renderLivingLabel(entityIn, str, x, y, z, maxDistance);
	}
	
	protected void renderModel(EntityCarpenterAnt entitylivingbaseIn, float p_77036_2_, float p_77036_3_, float p_77036_4_, float p_77036_5_, float p_77036_6_, float p_77036_7_) {
		boolean flag = !entitylivingbaseIn.isInvisible();
		boolean flag1 = !flag && !entitylivingbaseIn.isInvisibleToPlayer(Minecraft.getMinecraft().thePlayer);
		
		if (flag || flag1) {
			if (!this.bindEntityTexture(entitylivingbaseIn)) {
				return;
			}

			if (flag1) {
				GlStateManager.pushMatrix();
				GlStateManager.color(1.0F, 1.0F, 1.0F, 0.15F);
				GlStateManager.depthMask(false);
				GlStateManager.enableBlend();
				GlStateManager.blendFunc(770, 771);
				GlStateManager.alphaFunc(516, 0.003921569F);
			}
			
			float scale = 0.4F;
			GlStateManager.scale(scale, scale, scale);
			GlStateManager.translate(0, 2.3F, 0);
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			this.mainModel.render(entitylivingbaseIn, p_77036_2_, p_77036_3_, p_77036_4_, p_77036_5_, p_77036_6_, p_77036_7_);
			GL11.glDisable(GL11.GL_BLEND);
			
			if (flag1) {
				GlStateManager.disableBlend();
				GlStateManager.alphaFunc(516, 0.1F);
				GlStateManager.popMatrix();
				GlStateManager.depthMask(true);
			}
		}
	}

	@Override
	protected ResourceLocation getEntityTexture(EntityCarpenterAnt entity) {
		return texCarpenterAnt;
	}

}
