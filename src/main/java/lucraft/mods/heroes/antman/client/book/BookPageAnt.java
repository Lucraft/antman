package lucraft.mods.heroes.antman.client.book;

import lucraft.mods.lucraftcore.client.gui.book.BookChapter;
import lucraft.mods.lucraftcore.client.gui.book.BookPage;
import lucraft.mods.lucraftcore.util.LucraftCoreClientUtil;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class BookPageAnt extends BookPage {

	public BookPageAnt(BookChapter parent) {
		super(parent);
	}

	private EntityLivingBase ant;
	
	@Override
	public void renderLeftSide(Minecraft mc, int x, int y, int mouseX, int mouseY) {
		GlStateManager.pushMatrix();
		if(ant == null)
			ant = getAnt(mc.theWorld);
		
		mc.fontRendererObj.drawString(EnumChatFormatting.UNDERLINE + ant.getName(), x, y, 0);
		GlStateManager.color(1, 1, 1);
		drawEntityOnScreen(x + 50, y + 80, 180, 0, -40, mc.thePlayer.ticksExisted, ant);
		
		GlStateManager.popMatrix();
	}

	@Override
	public void renderRightSide(Minecraft mc, int x, int y, int mouseX, int mouseY) {
		boolean unicode = mc.fontRendererObj.getUnicodeFlag();
		mc.fontRendererObj.setUnicodeFlag(true);
		
		mc.fontRendererObj.drawSplitString(getDescription(), x + 10, y + 12, 105, 0);
		
		mc.fontRendererObj.setUnicodeFlag(unicode);
	}

	public EntityLivingBase getAnt(World world) {
		return null;
	}
	
	public String getDescription() {
		return null;
	}
	
	public void drawEntityOnScreen(int posX, int posY, int scale, float mouseX, float mouseY, int progress, EntityLivingBase ent) {
		GlStateManager.enableColorMaterial();
		GlStateManager.pushMatrix();
		GlStateManager.translate((float) posX, (float) posY, 50.0F);
		GlStateManager.scale((float) (-scale), (float) scale, (float) scale);
		GlStateManager.rotate(180.0F, 0.0F, 0.0F, 1.0F);
		float f = ent.renderYawOffset;
		float f1 = ent.rotationYaw;
		float f2 = ent.rotationPitch;
		float f3 = ent.prevRotationYawHead;
		float f4 = ent.rotationYawHead;
		GlStateManager.rotate(135.0F, 0.0F, 1.0F, 0.0F);
		RenderHelper.enableStandardItemLighting();
		GlStateManager.rotate(-135.0F, 0.0F, 1.0F, 0.0F);
		GlStateManager.rotate(-((float) Math.atan((double) (mouseY / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
		ent.renderYawOffset = (float) Math.atan((double) (mouseX / 40.0F)) * 20.0F;
		ent.rotationYaw = (float) Math.atan((double) (mouseX / 40.0F)) * 40.0F;
		ent.rotationPitch = -((float) Math.atan((double) (mouseY / 40.0F))) * 20.0F;
		ent.rotationYawHead = ent.rotationYaw;
		ent.prevRotationYawHead = ent.rotationYaw;
		GlStateManager.translate(0.0F, 0.0F, 0.0F);
		RenderManager rendermanager = Minecraft.getMinecraft().getRenderManager();
		rendermanager.setPlayerViewY(180.0F);
		rendermanager.setRenderShadow(false);
		GlStateManager.rotate((progress + LucraftCoreClientUtil.renderTick) * 2, 0F, 1F, 0F);
		rendermanager.renderEntityWithPosYaw(ent, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
		rendermanager.setRenderShadow(true);
		ent.renderYawOffset = f;
		ent.rotationYaw = f1;
		ent.rotationPitch = f2;
		ent.prevRotationYawHead = f3;
		ent.rotationYawHead = f4;
		GlStateManager.popMatrix();
		RenderHelper.disableStandardItemLighting();
		GlStateManager.disableRescaleNormal();
		GlStateManager.setActiveTexture(OpenGlHelper.lightmapTexUnit);
		GlStateManager.disableTexture2D();
		GlStateManager.setActiveTexture(OpenGlHelper.defaultTexUnit);
	}
	
}
