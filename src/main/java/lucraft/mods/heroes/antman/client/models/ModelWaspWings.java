package lucraft.mods.heroes.antman.client.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.lwjgl.opengl.GL11;

import lucraft.mods.heroes.antman.AMConfig;
import lucraft.mods.heroes.antman.entity.AntManEntityData;
import lucraft.mods.lucraftcore.util.IFakePlayerEntity;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

public class ModelWaspWings extends ModelAdvancedArmor {

	public List<ModelRenderer> wingTiles = new ArrayList<ModelRenderer>();
	public ModelRenderer wing1;
	public ModelRenderer wing2;
	public ModelRenderer wing3;
	public ModelRenderer wing7;
	public ModelRenderer wing8;
	public ModelRenderer wing9;
	public ModelRenderer wing4;
	public ModelRenderer wing5;
	public ModelRenderer wing6;
	public ModelRenderer wing10;
	public ModelRenderer wing11;
	public ModelRenderer wing12;

	public ModelWaspWings(float f, int width, int height, ResourceLocation lightTexLoc) {
		super(f, width, height, lightTexLoc);

		this.textureWidth = width;
		this.textureHeight = height;

		this.wing7 = new ModelRenderer(this, 64, 0);
		this.wing7.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.wing7.addBox(6.0F, 0.0F, 2.0F, 1, 17, 1, -0.2F);
		this.setRotateAngle(wing7, 0.13962634015954636F, 0.0F, 1.81898214642849F);
		this.wing4 = new ModelRenderer(this, 64, 0);
		this.wing4.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.wing4.addBox(-2.0F, 0.0F, 2.0F, 1, 17, 1, -0.2F);
		this.setRotateAngle(wing4, 0.13962634015954636F, 0.0F, -2.1855012893472994F);
		this.wing5 = new ModelRenderer(this, 64, 19);
		this.wing5.mirror = true;
		this.wing5.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.wing5.addBox(-10.4F, -0.3F, 2.5F, 9, 22, 0, 0.0F);
		this.setRotateAngle(wing5, 0.13962634015954636F, 0.0F, -2.1855012893472994F);
		this.wing11 = new ModelRenderer(this, 64, 19);
		this.wing11.mirror = true;
		this.wing11.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.wing11.addBox(-15.3F, -0.3F, 2.5F, 9, 22, 0, 0.0F);
		this.setRotateAngle(wing11, 0.13962634015954636F, 0.0F, -1.81898214642849F);
		this.wing12 = new ModelRenderer(this, 64, 0);
		this.wing12.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.wing12.addBox(-11.0F, -0.6F, 2.0F, 5, 1, 1, -0.2F);
		this.setRotateAngle(wing12, 0.13962634015954636F, 0.0F, -1.81898214642849F);
		this.wing10 = new ModelRenderer(this, 64, 0);
		this.wing10.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.wing10.addBox(-7.0F, 0.0F, 2.0F, 1, 17, 1, -0.2F);
		this.setRotateAngle(wing10, 0.13962634015954636F, 0.0F, -1.81898214642849F);
		this.wing3 = new ModelRenderer(this, 64, 0);
		this.wing3.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.wing3.addBox(1.0F, -0.6F, 2.0F, 5, 1, 1, -0.2F);
		this.setRotateAngle(wing3, 0.13962634015954636F, 0.0F, 2.1855012893472994F);
		this.wing6 = new ModelRenderer(this, 64, 0);
		this.wing6.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.wing6.addBox(-6.0F, -0.6F, 2.0F, 5, 1, 1, -0.2F);
		this.setRotateAngle(wing6, 0.13962634015954636F, 0.0F, -2.1855012893472994F);
		this.wing8 = new ModelRenderer(this, 64, 19);
		this.wing8.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.wing8.addBox(6.3F, -0.3F, 2.5F, 9, 22, 0, 0.0F);
		this.setRotateAngle(wing8, 0.13962634015954636F, 0.0F, 1.81898214642849F);
		this.wing2 = new ModelRenderer(this, 64, 19);
		this.wing2.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.wing2.addBox(1.3F, -0.3F, 2.5F, 9, 22, 0, 0.0F);
		this.setRotateAngle(wing2, 0.13962634015954636F, 0.0F, 2.1855012893472994F);
		this.wing1 = new ModelRenderer(this, 64, 0);
		this.wing1.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.wing1.addBox(1.0F, 0.0F, 2.0F, 1, 17, 1, -0.2F);
		this.setRotateAngle(wing1, 0.13962634015954636F, 0.0F, 2.1855012893472994F);
		this.wing9 = new ModelRenderer(this, 64, 0);
		this.wing9.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.wing9.addBox(6.0F, -0.6F, 2.0F, 5, 1, 1, -0.2F);
		this.setRotateAngle(wing9, 0.13962634015954636F, 0.0F, 1.81898214642849F);

		this.bipedBody.addChild(wing1);
		this.bipedBody.addChild(wing2);
		this.bipedBody.addChild(wing3);
		this.bipedBody.addChild(wing4);
		this.bipedBody.addChild(wing5);
		this.bipedBody.addChild(wing6);
		this.bipedBody.addChild(wing7);
		this.bipedBody.addChild(wing8);
		this.bipedBody.addChild(wing9);
		this.bipedBody.addChild(wing10);
		this.bipedBody.addChild(wing11);
		this.bipedBody.addChild(wing12);

		this.wingTiles.add(wing1);
		this.wingTiles.add(wing2);
		this.wingTiles.add(wing3);
		this.wingTiles.add(wing4);
		this.wingTiles.add(wing5);
		this.wingTiles.add(wing6);
		this.wingTiles.add(wing7);
		this.wingTiles.add(wing8);
		this.wingTiles.add(wing9);
		this.wingTiles.add(wing10);
		this.wingTiles.add(wing11);
		this.wingTiles.add(wing12);
	}

	public boolean normal = true;
	public Random rand = new Random();

	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		updateWings(entity);
		super.render(entity, f, f1, f2, f3, f4, f5);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glPopMatrix();
		super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
	}

	public void updateWings(Entity entity) {

		if (entity instanceof EntityPlayer && !(entity instanceof IFakePlayerEntity) && !AMConfig.disableFlyAnimations) {
			EntityPlayer player = (EntityPlayer) entity;
			for (ModelRenderer m : this.wingTiles) {
				float f = Math.abs(m.rotateAngleZ) - 0.8F;
				float progress = 1F - (float) (Math.min(Math.sqrt((player.prevPosX - player.posX) * (player.prevPosX - player.posX) + (player.prevPosZ - player.posZ) * (player.prevPosZ - player.posZ)), 1.0D) * (player.moveForward == 0.0F ? 1.0F : player.moveForward));
				float r = progress * f;
				if((m == wing4 || m == wing5 || m == wing6 || m == wing10 || m == wing11 || m == wing12))
					m.rotateAngleZ += r;
				else
					m.rotateAngleZ -= r;
				
				m.rotateAngleX = 0.3F;
			}
			
		}

	}

	public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
	
	@Override
	public void setRotationAngles(float f, float f2, float f3, float f4, float f5, float f6, Entity entityIn) {
		super.setRotationAngles(f, f2, f3, f4, f5, f6, entityIn);
		if(entityIn instanceof EntityPlayer && AntManEntityData.get((EntityLivingBase) entityIn).isFlying) {
			for(ModelRenderer m : this.wingTiles) {
				if(entityIn.motionX != 0 && entityIn.motionZ != 0)
					m.rotateAngleX = 0.1F + (float) MathHelper.cos(f * 5) / 2;
				else
					m.rotateAngleX = 0.1F + (float) MathHelper.cos(f3 * 5) / 8;
			}
		}
	}
}
