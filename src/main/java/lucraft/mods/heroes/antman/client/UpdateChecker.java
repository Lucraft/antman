package lucraft.mods.heroes.antman.client;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

import lucraft.mods.heroes.antman.AntMan;

public class UpdateChecker {

	private static String currentVersion = AntMan.VERSION;
	public static String newestVersion;
	public static String updateStatus = "NULL";
	public static boolean show = false;
	public static boolean failedConnection = false;

	public static void init() {
		getNewestVersion();

		if (newestVersion != null) {
			if (newestVersion.equalsIgnoreCase(currentVersion)) {
				updateStatus = "Your version is up to date!";
			} else {
				show = true;
				updateStatus = "New version is available!";
			}
		} else {
			show = true;
			failedConnection = true;
			updateStatus = "Failed to connect to check if update is available!";
		}
	}

	private static void getNewestVersion() {
		try {
			URL url = new URL("https://docs.google.com/uc?authuser=0&id=0B6_wwPkl6fmOVFhpcmxrVmxCbWM&export=download");
			Scanner s = new Scanner(url.openStream());
			newestVersion = s.next();
//			newestVersion = newestVersion + " " + s.next();
			s.close();
		} catch (IOException ex) {
			ex.printStackTrace();
//			newestVersion = AntMan.VERSION;
		}
	}
}