package lucraft.mods.heroes.antman.client.models;

import java.util.ArrayList;
import java.util.Random;

import org.lwjgl.opengl.GL11;

import lucraft.mods.heroes.antman.AntMan;
import lucraft.mods.heroes.antman.entity.EntitySizeChange;
import lucraft.mods.lucraftcore.util.EntityModelUtil;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

public class ModelSizeChange extends ModelBase {
	public EntitySizeChange SizeChangeEnt;

	public Render entRenderer;

	public Random rand;

	public ArrayList<ModelRenderer> modelList;

	public float renderYaw;

	public static ResourceLocation white = new ResourceLocation(AntMan.ASSETDIR + "textures/models/white.png");

	public ModelSizeChange() {
	}

	public ModelSizeChange(EntitySizeChange ent) {
		this.SizeChangeEnt = ent;
		this.rand = new Random();

		if (ent != null) {
			RenderManager manager = Minecraft.getMinecraft().getRenderManager();
			this.entRenderer = manager.getEntityRenderObject(ent.acquired);

			if (manager.renderEngine != null && manager.livingPlayer != null && ent.acquired != null) {
				manager.getEntityRenderObject(ent.acquired).doRender(ent.acquired, 0.0D, -500D, 0.0D, 0.0F, 1.0F);
			}

			this.modelList = EntityModelUtil.getCopiedModelCubes(EntityModelUtil.getModelCubes(ent.acquired), this, ent.acquired);

			for (ModelRenderer cube : modelList) {
				cube.rotationPointY -= 8.0D;
			}
		}

		this.renderYaw = ent.acquired.renderYawOffset;
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		if (SizeChangeEnt.acquired == Minecraft.getMinecraft().thePlayer && Minecraft.getMinecraft().gameSettings.thirdPersonView == 0) {
			return;
		}

		if(Minecraft.getMinecraft().gameSettings.fancyGraphics == false)
			return;
		
		renderModel(entity, f, f1, f2, f3, f4, f5, MathHelper.clamp_float(1 - (SizeChangeEnt.progress / 10F), 0, 1));
	}

	public void renderModel(Entity entity, float f, float f1, float f2, float f3, float f4, float f5, float progress) {
		GlStateManager.pushMatrix();

		GlStateManager.rotate(renderYaw, 0.0F, 1.0F, 0.0F);

		GlStateManager.depthMask(true);

		GlStateManager.pushMatrix();
		GlStateManager.enableBlend();
		GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		float color = 1F;
		GlStateManager.color(color, color, color, progress);
		
		for (int i = 0; i < modelList.size(); i++) {
			ModelRenderer cube = modelList.get(i);
			cube.render(f5);
		}
		renderArmor(SizeChangeEnt.acquired, f, f1, f2, f3, f4, f5);

		GlStateManager.color(color, color, color, progress - 0.3F);
		
		for (int i = 0; i < modelList.size(); i++) {
			ModelRenderer cube = modelList.get(i);
			Minecraft.getMinecraft().renderEngine.bindTexture(white);
			cube.render(f5);
		}
		renderArmor(SizeChangeEnt.acquired, f, f1, f2, f3, f4, f5);

		for (int j = 0; j < 3; j++) {
			for (int i = 0; i < modelList.size(); i++) {
				ModelRenderer cube = modelList.get(i);
				GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
				cube.render(f5);
				GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
			}
			renderArmor(SizeChangeEnt.acquired, f, f1, f2, f3, f4, f5);
		}

		GlStateManager.popMatrix();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		GlStateManager.disableBlend();
		GlStateManager.popMatrix();
	}
	
	public void renderArmor(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
//		if(entity instanceof EntityPlayer) {
//			EntityPlayer player = (EntityPlayer)entity;
//			for(int i = 0; i < 4; i++) {
//				ItemStack armor = player.getCurrentArmor(i);
//				if(armor != null && armor.getItem() instanceof ItemArmor) {
//					ItemArmor armorItem = (ItemArmor) armor.getItem();
//					ModelBiped model = armorItem.getArmorModel(player, armor, i, (ModelBiped) EntityModelUtil.getPossibleModel(entRenderer));
//					Minecraft.getMinecraft().renderEngine.bindTexture(new ResourceLocation(armorItem.getArmorTexture(armor, player, i, null)));
//					model.render(player, f, f1, f2, f3, f4, f5);
//				}
//			}
//		}
	}

}