package lucraft.mods.heroes.antman.client.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import com.mojang.realmsclient.gui.ChatFormatting;

import lucraft.mods.heroes.antman.AntMan;
import lucraft.mods.heroes.antman.client.render.AMClientRenderer;
import lucraft.mods.heroes.antman.container.ContainerRegulator;
import lucraft.mods.heroes.antman.items.AMItems;
import lucraft.mods.heroes.antman.items.InventoryRegulator;
import lucraft.mods.heroes.antman.network.MessageSetChestplateSize;
import lucraft.mods.heroes.antman.network.PacketDispatcher;
import lucraft.mods.heroes.antman.util.AntManHelper;
import lucraft.mods.heroes.antman.util.ShrinkerTypesClientHelper;
import lucraft.mods.lucraftcore.client.gui.buttons.GuiButton10x;
import lucraft.mods.lucraftcore.util.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

public class GuiSizeRegulator extends GuiContainer {

	private static ResourceLocation tex = new ResourceLocation(AntMan.ASSETDIR + "textures/gui/regulators/default.png");
	private static final ResourceLocation red_tex = new ResourceLocation(AntMan.ASSETDIR + "textures/gui/regulators/red.png");
	private static final ResourceLocation blue_tex = new ResourceLocation(AntMan.ASSETDIR + "textures/gui/regulators/blue.png");
	public static double size = 1F;
	
	private int progress = 0;

	public boolean canRegulateSize;
	private final InventoryRegulator inventory;
	
	public GuiSizeRegulator(ContainerRegulator containerItem) {
		super(containerItem);
		this.inventory = containerItem.inventory;
		this.xSize = 256;
		this.ySize = 244;
		
		canRegulateSize = containerItem.canRegulateSize;
	}
	
	@Override
	public void initGui() {
		super.initGui();
		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;
		
		if(canRegulateSize) {
			this.buttonList.add(new GuiSizeSliderButton(0, i + 28, j + 138));
			this.buttonList.add(new GuiButton(1, i + 10, j + 164, 60, 20, StatCollector.translateToLocal("antman.info.save")));
		}
		
		this.buttonList.add(new GuiButton(2, i + this.xSize - 70, j + 164, 60, 20, StatCollector.translateToLocal("gui.cancel")));
		this.buttonList.add(new GuiButton10x(3, i + ((ContainerRegulator)this.inventorySlots).regSlotX + 20, j + ((ContainerRegulator)this.inventorySlots).regSlotY + 9, "?"));
	}

	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		super.actionPerformed(button);
		
		if(button.id == 1) {
			PacketDispatcher.sendToServer(new MessageSetChestplateSize((float) size));
		}
		
		if(button.id == 2) {
			Minecraft.getMinecraft().thePlayer.closeScreen();
		}
	}
	
	@Override
	public void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		GlStateManager.pushMatrix();
		int i = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;
		this.mc.getTextureManager().bindTexture(tex);
		this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize - 20);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		
		if(canRegulateSize) {
			progress++;
			if(progress == 360)
				progress = 0;
			
			float sliderValue = ((GuiSizeSliderButton)this.buttonList.get(0)).sliderValue;
			if(sliderValue == 0.5F)
				this.buttonList.get(1).enabled = false;
			else
				this.buttonList.get(1).enabled = true;
				
			// Background
			GlStateManager.color(1F, 1F, 1F, 1F - sliderValue);
			this.mc.getTextureManager().bindTexture(red_tex);
			this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);
			
			GlStateManager.color(1F, 1F, 1F, sliderValue);
			this.mc.getTextureManager().bindTexture(blue_tex);
			this.drawTexturedModalRect(i, j, 0, 0, this.xSize, this.ySize);
			GlStateManager.color(1F, 1F, 1F, 1F);
			
			// Player
			int x = i + this.xSize / 2;
			int y = j + this.ySize / 2 + 7;
			
			GlStateManager.pushMatrix();
			float f = (float) ((GuiSizeSliderButton)this.buttonList.get(0)).getValue();
			if(f > 1F) {
				f = 1F;
				this.drawString(Minecraft.getMinecraft().fontRendererObj, "x" + ((GuiSizeSliderButton)this.buttonList.get(0)).getValue(), x + 20, y - 5, 0xffffff);
			}
			drawEntityOnScreen(x, y, (int) (40 * f), 1, 1, Minecraft.getMinecraft().thePlayer);
			GlStateManager.popMatrix();
			
			//PymParticles
			AMClientRenderer.drawPymTankInfo(i + 18, j + 22, true, this, this.itemRender);
		} else {
//			this.mc.getTextureManager().bindTexture(custom_tex);
//			this.drawTexturedModalRect(i + 21, j + 21, 0, 0, 214, 140);
			this.drawEntityOnScreen(i + this.xSize / 2, j + this.ySize / 2 + 23, 60, 0, 0, Minecraft.getMinecraft().thePlayer);
		}
		
		GlStateManager.color(1F, 1F, 1F, 1F);
		this.mc.getTextureManager().bindTexture(tex);
		this.drawTexturedModalRect(i + ((ContainerRegulator)this.inventorySlots).regSlotX - 3, j + ((ContainerRegulator)this.inventorySlots).regSlotY - 3, 0, 224, 22, 22);
		
		GL11.glDisable(GL11.GL_BLEND);
		GlStateManager.popMatrix();
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		super.drawScreen(mouseX, mouseY, partialTicks);
		
		int bx = this.buttonList.get(buttonList.size() - 1).xPosition;
		int by = this.buttonList.get(buttonList.size() - 1).yPosition;
		
		if(mouseX >= bx && mouseX <= bx + 9 && mouseY >= by && mouseY <= by + 9) {
			List<String> desc = new ArrayList<String>();
			
			String prefix = "     = ";
			desc.add(prefix + StatCollector.translateToLocal("antman.info.regulatorbutton1"));
			desc.add("");
			desc.add(prefix + StatCollector.translateToLocal("antman.info.regulatorbutton2"));
			desc.add("");
			desc.add(prefix + StatCollector.translateToLocal("antman.info.regulatorbutton3"));
			desc.add("");
			desc.add(prefix + StatCollector.translateToLocal("antman.info.regulatorbutton4"));
			desc.add("");
			desc.add(ChatFormatting.RED + StatCollector.translateToLocal("antman.info.regulatorbutton5"));
			desc.add(ChatFormatting.RED + StatCollector.translateToLocal("antman.info.regulatorbutton6"));
			
			LCRenderHelper.drawStringList(desc, mouseX + 12, mouseY - 32, 10, true);
			this.itemRender.renderItemAndEffectIntoGUI(new ItemStack(inventory.regulatorItem), mouseX + 12, mouseY - 36);
			this.itemRender.renderItemAndEffectIntoGUI(new ItemStack(AMItems.discShrink), mouseX + 12, mouseY - 16);
			this.itemRender.renderItemAndEffectIntoGUI(new ItemStack(AMItems.discGrow), mouseX + 12, mouseY + 4);
			
			GlStateManager.enableAlpha();
			mc.renderEngine.bindTexture(ShrinkerTypesClientHelper.getHUDResourceLocation(AntManHelper.getShrinkerTypeFromHelmet(mc.thePlayer)));
			this.drawTexturedModalRect(mouseX + 12, mouseY + 26, 0, 12, 16, 12);
			 GlStateManager.disableAlpha();
		}
	}
	
	public void drawEntityOnScreen(int posX, int posY, int scale, float mouseX, float mouseY, EntityLivingBase ent) {
		GlStateManager.enableColorMaterial();
		GlStateManager.pushMatrix();
		GlStateManager.translate((float) posX, (float) posY, 50.0F);
		GlStateManager.scale((float) (-scale), (float) scale, (float) scale);
		GlStateManager.rotate(180.0F, 0.0F, 0.0F, 1.0F);
		float f = ent.renderYawOffset;
		float f1 = ent.rotationYaw;
		float f2 = ent.rotationPitch;
		float f3 = ent.prevRotationYawHead;
		float f4 = ent.rotationYawHead;
		GlStateManager.rotate(135.0F, 0.0F, 1.0F, 0.0F);
		RenderHelper.enableStandardItemLighting();
		GlStateManager.rotate(-135.0F, 0.0F, 1.0F, 0.0F);
		GlStateManager.rotate(-((float) Math.atan((double) (mouseY / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
		ent.renderYawOffset = (float) Math.atan((double) (mouseX / 40.0F)) * 20.0F;
		ent.rotationYaw = (float) Math.atan((double) (mouseX / 40.0F)) * 40.0F;
		ent.rotationPitch = -((float) Math.atan((double) (mouseY / 40.0F))) * 20.0F;
		ent.rotationYawHead = ent.rotationYaw;
		ent.prevRotationYawHead = ent.rotationYaw;
		GlStateManager.translate(0.0F, 0.0F, 0.0F);
		RenderManager rendermanager = Minecraft.getMinecraft().getRenderManager();
		rendermanager.setPlayerViewY(180.0F);
		rendermanager.setRenderShadow(false);
		GlStateManager.rotate(progress, 0F, 1F, 0F);
		rendermanager.renderEntityWithPosYaw(ent, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
		rendermanager.setRenderShadow(true);
		ent.renderYawOffset = f;
		ent.rotationYaw = f1;
		ent.rotationPitch = f2;
		ent.prevRotationYawHead = f3;
		ent.rotationYawHead = f4;
		GlStateManager.popMatrix();
		RenderHelper.disableStandardItemLighting();
		GlStateManager.disableRescaleNormal();
		GlStateManager.setActiveTexture(OpenGlHelper.lightmapTexUnit);
		GlStateManager.disableTexture2D();
		GlStateManager.setActiveTexture(OpenGlHelper.defaultTexUnit);
	}

}