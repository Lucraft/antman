package lucraft.mods.heroes.antman.client.book;

import java.util.ArrayList;
import java.util.List;

import lucraft.mods.heroes.antman.util.AntManHelper;
import lucraft.mods.heroes.antman.util.ShrinkerArmorAbilities;
import lucraft.mods.heroes.antman.util.ShrinkerTypes;
import lucraft.mods.lucraftcore.client.gui.book.BookChapter;
import lucraft.mods.lucraftcore.client.gui.book.BookPage;
import lucraft.mods.lucraftcore.util.LucraftCoreClientUtil;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StatCollector;

public class BookPageAntManSuits extends BookPage {

	public ShrinkerTypes type;
	
	public BookPageAntManSuits(BookChapter parent, ShrinkerTypes type) {
		super(parent);
		this.type = type;
	}

	@Override
	public void renderLeftSide(Minecraft mc, int x, int y, int mouseX, int mouseY) {
		mc.fontRendererObj.drawString(EnumChatFormatting.UNDERLINE + type.getDisplayName(), x, y, 0);

		boolean unicode = mc.fontRendererObj.getUnicodeFlag();
		mc.fontRendererObj.setUnicodeFlag(true);
		
		mc.fontRendererObj.drawString(type.getDescription(), x, y + 10, 0);
		
		//----------------------------------------------------------------------
		
		ItemStack heldItem = mc.thePlayer.getHeldItem();
		mc.thePlayer.setCurrentItemOrArmor(0, null);
		ItemStack helmet = mc.thePlayer.getCurrentArmor(3);
		mc.thePlayer.setCurrentItemOrArmor(4, type.getHelmet());
		ItemStack chestplate = mc.thePlayer.getCurrentArmor(2);
		mc.thePlayer.setCurrentItemOrArmor(3, type.getChestplate());
		ItemStack legs = mc.thePlayer.getCurrentArmor(1);
		mc.thePlayer.setCurrentItemOrArmor(2, type.getLegs());
		ItemStack boots = mc.thePlayer.getCurrentArmor(0);
		mc.thePlayer.setCurrentItemOrArmor(1, type.getBoots());
		
		GlStateManager.color(1, 1, 1);
		drawEntityOnScreen(x + 50, y + 125, 50, 0, 0, mc.thePlayer.ticksExisted, mc.thePlayer);
		
		mc.thePlayer.setCurrentItemOrArmor(0, heldItem);
		mc.thePlayer.setCurrentItemOrArmor(1, boots);
		mc.thePlayer.setCurrentItemOrArmor(2, legs);
		mc.thePlayer.setCurrentItemOrArmor(3, chestplate);
		mc.thePlayer.setCurrentItemOrArmor(4, helmet);
		
		//----------------------------------------------------------------------
		
		mc.fontRendererObj.setUnicodeFlag(unicode);
	}

	@Override
	public void renderRightSide(Minecraft mc, int x, int y, int mouseX, int mouseY) {
		GlStateManager.pushMatrix();
		GlStateManager.translate(x + 45, y + 5, 0);
		GlStateManager.scale(2, 2, 2);
		mc.getRenderItem().renderItemIntoGUI(new ItemStack(AntManHelper.getRegulatorFromArmor(type.getChestplate())), 0, 0);
		GlStateManager.popMatrix();
		
		boolean unicode = mc.fontRendererObj.getUnicodeFlag();
		mc.fontRendererObj.setUnicodeFlag(true);
		
		// Ability List
		List<String> list = new ArrayList<String>();
		
		for(ShrinkerArmorAbilities ab : type.getAbilites()) {
			list.add(ab.getDisplayName());
		}
		
		int listRenderLength = 11 + list.size() * 10;
		
//		if(type.hasSymbol())
//			listRenderLength -= 32;
		
		GlStateManager.translate(0, 80 - listRenderLength / 2, 0);
		
		mc.fontRendererObj.drawString(EnumChatFormatting.UNDERLINE + StatCollector.translateToLocal("book.chapter.superpowers.abilities") + ":", x + 5, y, 0);
		if(list != null && list.size() > 0) {
			for(int i = 0; i < list.size(); i++) {
				mc.fontRendererObj.drawString("- " + list.get(i), x + 10, y + 11 + i * 10, 0);
			}
		}
		
		mc.fontRendererObj.setUnicodeFlag(unicode);
	}

	public void drawEntityOnScreen(int posX, int posY, int scale, float mouseX, float mouseY, int progress, EntityLivingBase ent) {
		GlStateManager.enableColorMaterial();
		GlStateManager.pushMatrix();
		GlStateManager.translate((float) posX, (float) posY, 50.0F);
		GlStateManager.scale((float) (-scale), (float) scale, (float) scale);
		GlStateManager.rotate(180.0F, 0.0F, 0.0F, 1.0F);
		float f = ent.renderYawOffset;
		float f1 = ent.rotationYaw;
		float f2 = ent.rotationPitch;
		float f3 = ent.prevRotationYawHead;
		float f4 = ent.rotationYawHead;
		GlStateManager.rotate(135.0F, 0.0F, 1.0F, 0.0F);
		RenderHelper.enableStandardItemLighting();
		GlStateManager.rotate(-135.0F, 0.0F, 1.0F, 0.0F);
		GlStateManager.rotate(-((float) Math.atan((double) (mouseY / 40.0F))) * 20.0F, 1.0F, 0.0F, 0.0F);
		ent.renderYawOffset = (float) Math.atan((double) (mouseX / 40.0F)) * 20.0F;
		ent.rotationYaw = (float) Math.atan((double) (mouseX / 40.0F)) * 40.0F;
		ent.rotationPitch = -((float) Math.atan((double) (mouseY / 40.0F))) * 20.0F;
		ent.rotationYawHead = ent.rotationYaw;
		ent.prevRotationYawHead = ent.rotationYaw;
		GlStateManager.translate(0.0F, 0.0F, 0.0F);
		RenderManager rendermanager = Minecraft.getMinecraft().getRenderManager();
		rendermanager.setPlayerViewY(180.0F);
		rendermanager.setRenderShadow(false);
		GlStateManager.rotate((progress + LucraftCoreClientUtil.renderTick) * 2, 0F, 1F, 0F);
		rendermanager.renderEntityWithPosYaw(ent, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
		rendermanager.setRenderShadow(true);
		ent.renderYawOffset = f;
		ent.rotationYaw = f1;
		ent.rotationPitch = f2;
		ent.prevRotationYawHead = f3;
		ent.rotationYawHead = f4;
		GlStateManager.popMatrix();
		RenderHelper.disableStandardItemLighting();
		GlStateManager.disableRescaleNormal();
		GlStateManager.setActiveTexture(OpenGlHelper.lightmapTexUnit);
		GlStateManager.disableTexture2D();
		GlStateManager.setActiveTexture(OpenGlHelper.defaultTexUnit);
	}
	
}
