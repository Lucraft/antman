package lucraft.mods.heroes.antman.client;

import com.mojang.realmsclient.gui.ChatFormatting;

import lucraft.mods.heroes.antman.AMConfig;
import lucraft.mods.heroes.antman.AntMan;
import lucraft.mods.heroes.antman.client.render.AMClientRenderer;
import lucraft.mods.heroes.antman.entity.AntManEntityData;
import lucraft.mods.heroes.antman.entity.EntitySizeChange;
import lucraft.mods.heroes.antman.entity.EntitySizeChangePlayer;
import lucraft.mods.heroes.antman.network.MessageFlyKeys;
import lucraft.mods.heroes.antman.network.MessageSendInfoToServer;
import lucraft.mods.heroes.antman.network.MessageShootLaser;
import lucraft.mods.heroes.antman.network.MessageSizeKey;
import lucraft.mods.heroes.antman.network.PacketDispatcher;
import lucraft.mods.heroes.antman.network.SyncTracker;
import lucraft.mods.heroes.antman.util.AMSounds;
import lucraft.mods.heroes.antman.util.AntManHelper;
import lucraft.mods.heroes.antman.util.ShrinkerArmorAbilities;
import lucraft.mods.heroes.antman.util.ShrinkerTypes;
import lucraft.mods.lucraftcore.client.LucraftCoreKeyBindings.LucraftKeyTypes;
import lucraft.mods.lucraftcore.events.LucraftCoreKeyEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.event.ClickEvent;
import net.minecraft.event.HoverEvent;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.StatCollector;
import net.minecraftforge.event.entity.PlaySoundAtEntityEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class AMClientEvents {

	private Minecraft mc;

	public AMClientEvents() {
		this.mc = Minecraft.getMinecraft();
	}

	public static boolean hasShownUpdate = false;
	public static boolean hasShownJEI = false;

	@SubscribeEvent
	public void onWorldJoin(PlayerEvent.PlayerLoggedInEvent e) {
		if (!hasShownUpdate) {
			AntManHelper.sendInfoMessage(e.player, UpdateChecker.updateStatus, false);
			if (!UpdateChecker.failedConnection && !UpdateChecker.newestVersion.equalsIgnoreCase(AntMan.VERSION))
				sendDownloadMessageToPlayer(e.player);
			hasShownUpdate = true;
		}

		if (!hasShownJEI && !Loader.isModLoaded("JEI")) {
			AntManHelper.sendInfoMessage(e.player, "antman.info.recipes");
			hasShownJEI = true;
		}
	}

	private static final ChatStyle download = new ChatStyle();

	public static void sendDownloadMessageToPlayer(EntityPlayer player) {
		IChatComponent chat = new ChatComponentText("");
		download.setChatHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ChatComponentText(StatCollector.translateToLocal("lucraftcore.info.clickdownload"))));

		chat.appendText(ChatFormatting.DARK_GREEN + "[");
		ChatStyle data = download.createShallowCopy();
		data.setChatClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "http://www.curse.com/mc-mods/Minecraft/231447-antman"));
		chat.appendSibling(new ChatComponentText(ChatFormatting.GREEN + "Download").setChatStyle(data));
		chat.appendText(ChatFormatting.DARK_GREEN + "] ");
		chat.appendSibling(new ChatComponentText(ChatFormatting.GRAY + UpdateChecker.newestVersion));

		player.addChatMessage(chat);
	}

	@SubscribeEvent
	public void lucraftKey(LucraftCoreKeyEvent evt) {
		if (evt.type == LucraftKeyTypes.HELMET && AntManHelper.hasHelmetOn(mc.thePlayer))
			PacketDispatcher.sendToServer(new MessageSendInfoToServer(MessageSendInfoToServer.InfoType.HELMET));

		if (AntManHelper.hasArmorOn(mc.thePlayer) && mc.inGameHasFocus) {

			// Size
			if (evt.type == LucraftKeyTypes.ARMOR_1)
				PacketDispatcher.sendToServer(new MessageSizeKey());

			// Regulator GUI
			if (evt.type == LucraftKeyTypes.ARMOR_2)
				PacketDispatcher.sendToServer(new MessageSendInfoToServer(MessageSendInfoToServer.InfoType.REGULATOR_GUI));

			// Laser
			if (evt.type == LucraftKeyTypes.ARMOR_3 && laserCooldown == 0 && AntManHelper.hasArmorOn(mc.thePlayer, ShrinkerTypes.MCU_YELLOWJACKET)) {
				PacketDispatcher.sendToServer(new MessageShootLaser());
				laserCooldown = 20 * 2;
			}

			// HUD toggle
			if (evt.type == LucraftKeyTypes.ARMOR_4)
				AMClientRenderer.showHud = !AMClientRenderer.showHud;
		}
		//
		// } else if (evt.eventType == LucraftKeyEventTypes.GETNAME &&
		// mc.thePlayer != null) {
		//
		// if(evt.type == LucraftKeyTypes.HELMET &&
		// AntManHelper.hasHelmetOn(mc.thePlayer))
		// evt.keyCategory = AntMan.MODID;
		//
		// if(AntManHelper.hasArmorOn(mc.thePlayer)) {
		// if(evt.type != LucraftKeyTypes.ARMOR_5)
		// evt.keyCategory = AntMan.MODID;
		//
		// // Size
		// if(evt.type == LucraftKeyTypes.ARMOR_1)
		// evt.keyDesc =
		// StatCollector.translateToLocal("antman.keybinding.keySize");
		//
		// // Regulator GUI
		// if(evt.type == LucraftKeyTypes.ARMOR_2)
		// evt.keyDesc =
		// StatCollector.translateToLocal("antman.keybinding.sizeGUI");
		//
		// // Laser
		// if(evt.type == LucraftKeyTypes.ARMOR_3)
		// evt.keyDesc =
		// StatCollector.translateToLocal("antman.keybinding.laser");
		//
		// // HUD toggle
		// if(evt.type == LucraftKeyTypes.ARMOR_4)
		// evt.keyDesc =
		// StatCollector.translateToLocal("antman.keybinding.togglehud");
		// }
		//
		// }
	}

	public static int laserCooldown = 20 * 2;
	public static EntityLivingBase entity = null;

	private static boolean lastFlyState = false;
	private static boolean lastDescendState = false;

	private static boolean lastForwardState = false;
	private static boolean lastBackwardState = false;
	private static boolean lastLeftState = false;
	private static boolean lastRightState = false;

	@SubscribeEvent
	public void onUpdate(TickEvent.ClientTickEvent e) {
		laserCooldown--;
		if (laserCooldown < 0)
			laserCooldown = 0;

		if (AMConfig.renderSizeChange) {
			if (!(e.phase == TickEvent.Phase.END && Minecraft.getMinecraft().theWorld != null)) {
				return;
			}

			if (entity != null) {
				if(entity instanceof EntityPlayer)
					entity.worldObj.spawnEntityInWorld(new EntitySizeChangePlayer(entity.worldObj, entity));
				else
					entity.worldObj.spawnEntityInWorld(new EntitySizeChange(entity.worldObj, entity));
				entity = null;
			}
		}

		if (mc.thePlayer != null && AntManHelper.hasArmorOn(mc.thePlayer) && AntManHelper.getShrinkerTypeFromHelmet(mc.thePlayer).getAbilites().contains(ShrinkerArmorAbilities.flight)) {
			boolean flyState;
			boolean descendState;
			flyState = mc.gameSettings.keyBindJump.isKeyDown();
			descendState = mc.gameSettings.keyBindSneak.isKeyDown();

			boolean forwardState = mc.gameSettings.keyBindForward.isKeyDown();
			boolean backwardState = mc.gameSettings.keyBindBack.isKeyDown();
			boolean leftState = mc.gameSettings.keyBindLeft.isKeyDown();
			boolean rightState = mc.gameSettings.keyBindRight.isKeyDown();

			if (flyState != lastFlyState || descendState != lastDescendState || forwardState != lastForwardState || backwardState != lastBackwardState || leftState != lastLeftState || rightState != lastRightState) {
				lastFlyState = flyState;
				lastDescendState = descendState;

				lastForwardState = forwardState;
				lastBackwardState = backwardState;
				lastLeftState = leftState;
				lastRightState = rightState;
				PacketDispatcher.sendToServer(new MessageFlyKeys(flyState, descendState, forwardState, backwardState, leftState, rightState));
				SyncTracker.processKeyUpdate(mc.thePlayer, flyState, descendState, forwardState, backwardState, leftState, rightState);
			}
		}
	}

	@SubscribeEvent
	public void onUpdate(PlaySoundAtEntityEvent e) {
		if (AntManHelper.isSizeChangeable(e.entity) && AntManEntityData.get((EntityLivingBase) e.entity).size != 1F) {
			if (e.name.equalsIgnoreCase(AMSounds.shrink) || e.name.equalsIgnoreCase(AMSounds.grow) || e.name.equalsIgnoreCase(AMSounds.jetpack) || e.name.equalsIgnoreCase(AMSounds.wings)) {
				// nothing
			} else {
				EntityLivingBase en = (EntityLivingBase) e.entity;
				e.newPitch = e.pitch / AntManEntityData.get(en).size;
			}
		}
	}
}
