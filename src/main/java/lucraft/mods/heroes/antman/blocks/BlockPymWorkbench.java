package lucraft.mods.heroes.antman.blocks;

import lucraft.mods.heroes.antman.AntMan;
import lucraft.mods.heroes.antman.client.gui.GuiIDs;
import lucraft.mods.heroes.antman.proxies.AntManProxy;
import lucraft.mods.heroes.antman.tileentity.TileEntityPymParticleProducer;
import lucraft.mods.heroes.antman.tileentity.TileEntityPymWorkbench;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class BlockPymWorkbench extends BlockContainer {

	private String name = "pymWorkbench";
	// private String assetName = AntMan.ASSETDIR + name;

	public BlockPymWorkbench() {
		super(Material.iron);

		this.setUnlocalizedName(name);
		this.setCreativeTab(AntManProxy.tabAntMan);
		this.setHardness(5F);
		this.setResistance(5F);
		this.setHarvestLevel("pickaxe", 1);

		GameRegistry.registerBlock(this, name);
		GameRegistry.registerTileEntity(TileEntityPymWorkbench.class, name);
		AntMan.proxy.registerBlockModel(this, name);
	}

	public int getRenderType() {
		return 3;
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileEntityPymWorkbench();
	}

	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumFacing side, float x, float y, float z) {

		if (!world.isRemote && !player.isSneaking()) {
			player.openGui(AntMan.instance, GuiIDs.pymWorkbenchGuiId, world, pos.getX(), pos.getY(), pos.getZ());
			return true;
		} else {
			return false;
		}
	}

	public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
		TileEntity tileentity = worldIn.getTileEntity(pos);

		if (tileentity instanceof TileEntityPymParticleProducer) {
			InventoryHelper.dropInventoryItems(worldIn, pos, (TileEntityPymParticleProducer) tileentity);
			worldIn.updateComparatorOutputLevel(pos, this);
		}

		super.breakBlock(worldIn, pos, state);
	}

	@Override
	public boolean isOpaqueCube() {
		return false;
	}

}
