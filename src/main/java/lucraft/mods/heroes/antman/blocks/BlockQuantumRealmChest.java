package lucraft.mods.heroes.antman.blocks;

import lucraft.mods.heroes.antman.AntMan;
import lucraft.mods.heroes.antman.client.gui.GuiIDs;
import lucraft.mods.heroes.antman.proxies.AntManProxy;
import lucraft.mods.heroes.antman.tileentity.TileEntityQuantumRealmChest;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class BlockQuantumRealmChest extends BlockContainer {

	public BlockQuantumRealmChest() {
		super(Material.rock);
		String name = "quantumRealmChest";
		this.setUnlocalizedName(name);
		this.setCreativeTab(AntManProxy.tabAntMan);
		this.setHardness(10F);
		this.setResistance(10F);
		this.setHarvestLevel("pickaxe", 2);

		GameRegistry.registerBlock(this, name);
		GameRegistry.registerTileEntity(TileEntityQuantumRealmChest.class, name);
		AntMan.proxy.registerBlockModel(this, name);
	}

	public int getRenderType() {
		return 3;
	}

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileEntityQuantumRealmChest();
	}

	public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumFacing side, float x, float y, float z) {

		if (!world.isRemote && !player.isSneaking()) {
			player.openGui(AntMan.instance, GuiIDs.quantumRealmChest, world, pos.getX(), pos.getY(), pos.getZ());
			return true;
		} else {
			return false;
		}
	}

	public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
		TileEntity tileentity = worldIn.getTileEntity(pos);

		if (tileentity instanceof TileEntityQuantumRealmChest) {
			InventoryHelper.dropInventoryItems(worldIn, pos, (TileEntityQuantumRealmChest) tileentity);
			worldIn.updateComparatorOutputLevel(pos, this);
		}

		super.breakBlock(worldIn, pos, state);
	}

}
