package lucraft.mods.heroes.antman.blocks;

import net.minecraft.block.Block;

public class AMBlocks {

	public static Block pymWorkbench;
	public static Block pymParticleProducer_active;
	public static Block pymParticleProducer_inactive;
	
	public static Block quantumRealmBlock;
	public static Block quantumRealmChest;
	
	public static void init() {
		pymWorkbench = new BlockPymWorkbench();
		pymParticleProducer_active = new BlockPymParticleProducer(true);
		pymParticleProducer_inactive = new BlockPymParticleProducer(false);
		
		quantumRealmBlock = new BlockQuantumRealm();
		quantumRealmChest = new BlockQuantumRealmChest();
	}

}