package lucraft.mods.heroes.antman.blocks;

import lucraft.mods.heroes.antman.AntMan;
import lucraft.mods.heroes.antman.proxies.AntManProxy;
import lucraft.mods.lucraftcore.blocks.BlockBase;
import net.minecraft.block.material.Material;

public class BlockQuantumRealm extends BlockBase {

	public BlockQuantumRealm() {
		super(Material.rock, "quantumRealmBlock");
		AntMan.proxy.registerBlockModel(this, "quantumRealmBlock");

		this.setCreativeTab(AntManProxy.tabAntMan);
		this.setHardness(99999F);
		this.setBlockUnbreakable();
		this.setResistance(99999F);
		this.setStepSound(soundTypeStone);
		this.setLightLevel(5F);
	}
	
}
