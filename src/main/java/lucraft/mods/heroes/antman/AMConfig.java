package lucraft.mods.heroes.antman;

import java.util.HashMap;

import lucraft.mods.heroes.antman.blocks.AMBlocks;
import lucraft.mods.heroes.antman.items.AMItems;
import net.minecraft.item.Item;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class AMConfig {

	public static HashMap<Item, Boolean> recipes = new HashMap<Item, Boolean>();
	
	public static boolean renderSizeChange;
	public static boolean disableOutlines;
	public static boolean disableDamageReduction;
	public static boolean disableQuantumRealmDamage;
	public static boolean disableFlyAnimations;
	public static int sizeChangeTicks;
	public static int pymParticlesForSizeChange;
	public static int quantumRealmDimensionId;
	public static int quantumRealmBiomeId;
	
	public static void preInit(FMLPreInitializationEvent e) {
		Configuration config = new Configuration(e.getSuggestedConfigurationFile());
		config.load();
		
		renderSizeChange = config.getBoolean("Size Change FX", "General", true, "[Client side] When enabled copies from entity will appear while changing the size");
		disableOutlines = config.getBoolean("Disable Outlines", "General", false, "[Client side] If enabled, you will see the full entity on size-changing");
		disableDamageReduction = config.getBoolean("Disable Damage Reduction", "General", false, "When disabled you'll get less damage when you're small/big");
		disableQuantumRealmDamage = config.getBoolean("Disable QR Damage", "General", false, "When disabled you'll get damage without armor in the Quantum Realm");
		disableFlyAnimations = config.getBoolean("Disable Fly Animations", "General", false, "When disabled Wasp Wing animation will be disabled");
		sizeChangeTicks = config.getInt("Size Change Ticks", "General", 20, 4, 1000, "Time (in ticks) the size changing take");
		pymParticlesForSizeChange = config.getInt("Size Change Particles", "General", 50, 10, 1000, "Pym Particles that will be consumed on size-changing");
		quantumRealmDimensionId = config.getInt("QR Dim Id", "General", 50, -255, 255, "Change that if it conflicts with other mods");
		quantumRealmBiomeId = config.getInt("QR Biome Id", "General", 50, -255, 255, "Change that if it conflicts with other mods");
		
		for(Item items : AMItems.items) {
			recipes.put(items, config.get("Recipes", items.getUnlocalizedName().replace("item.", ""), true).getBoolean());
		}
		
		recipes.put(Item.getItemFromBlock(AMBlocks.pymParticleProducer_inactive), config.get("Recipes", "pymParticleProducer", true).getBoolean());
		
		config.save();
	}
	
}
