package lucraft.mods.heroes.antman.proxies;

import java.util.ArrayList;

import lucraft.mods.heroes.antman.AntMan;
import lucraft.mods.heroes.antman.blocks.AMBlocks;
import lucraft.mods.heroes.antman.client.gui.GuiHandler;
import lucraft.mods.heroes.antman.dimensions.AMDimensions;
import lucraft.mods.heroes.antman.entity.AntManEntityEvents;
import lucraft.mods.heroes.antman.entity.EntityBulletAnt;
import lucraft.mods.heroes.antman.entity.EntityCarpenterAnt;
import lucraft.mods.heroes.antman.entity.EntityGrowDisc;
import lucraft.mods.heroes.antman.entity.EntityLaser;
import lucraft.mods.heroes.antman.entity.EntityShrinkDisc;
import lucraft.mods.heroes.antman.fluids.AMFluids;
import lucraft.mods.heroes.antman.items.AMItems;
import lucraft.mods.heroes.antman.network.PacketDispatcher;
import lucraft.mods.heroes.antman.network.SyncTracker;
import lucraft.mods.heroes.antman.recipes.AMRecipes;
import lucraft.mods.heroes.antman.util.BucketHandler;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.DamageSource;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AntManProxy {

	public static DamageSource dmgSuffocate = new DamageSource("suffocate");
	
	public void preInit() {
		AMItems.init();
		AMBlocks.init();
	}
	
	public void init() {
		AMDimensions.init();
		
		NetworkRegistry.INSTANCE.registerGuiHandler(AntMan.instance, new GuiHandler());
		MinecraftForge.EVENT_BUS.register(new AntManEntityEvents());
		MinecraftForge.EVENT_BUS.register(new SyncTracker());
		MinecraftForge.EVENT_BUS.register(BucketHandler.INSTANCE);
		PacketDispatcher.registerPackets();
		
		EntityRegistry.registerModEntity(EntityShrinkDisc.class, "discShrink", 42, AntMan.instance, 40, 1, true);
		EntityRegistry.registerModEntity(EntityGrowDisc.class, "discGrow", 43, AntMan.instance, 40, 1, true);
		EntityRegistry.registerModEntity(EntityLaser.class, "laser", 44, AntMan.instance, 40, 1, true);
		EntityRegistry.registerModEntity(EntityBulletAnt.class, "bulletAnt", 45, AntMan.instance, 40, 1, true, 0x703a25, 0x432316);
		EntityRegistry.registerModEntity(EntityCarpenterAnt.class, "carpenterAnt", 46, AntMan.instance, 40, 1, true, 0x432316, 0x703a25);
		
		ArrayList<BiomeGenBase> all_biomesList = new ArrayList<BiomeGenBase>();
		for( BiomeGenBase biome : BiomeGenBase.getBiomeGenArray() ) {
			if( biome != null ) {
				all_biomesList.add(biome);
			}
		}
		BiomeGenBase[] all_biomes_array = new BiomeGenBase[all_biomesList.size()];
		BiomeGenBase[] all_biomes = all_biomesList.toArray(all_biomes_array);
		
		EntityRegistry.addSpawn(EntityBulletAnt.class, 2, 2, 3, EnumCreatureType.AMBIENT, all_biomes);
		EntityRegistry.addSpawn(EntityCarpenterAnt.class, 2, 2, 2, EnumCreatureType.AMBIENT, all_biomes);
	}
	
	public void postInit() {
		AMFluids.init();
		AMRecipes.init();
	}

	public static CreativeTabs tabAntMan = new CreativeTabs("tabAntMan") {
		@Override
		@SideOnly(Side.CLIENT)
		public Item getTabIconItem() {
			return AMItems.discShrink;
		}
	};

	// -----------------------------------------------------------------------
	
	public void registerItemModel(Item item, String resource) {
	}

	public void registerItemModel(Item item, int meta, String resource) {
	}

	public void registerBlockModel(Block block, String resource) {
	}

	public void registerBlockModel(Block block, int meta, String resource) {
	}
	
	public void registerFluidModels(Fluid fluid) {
	}
	
	public EntityPlayer getPlayerEntity(MessageContext ctx) {
		return ctx.getServerHandler().playerEntity;
	}

}
