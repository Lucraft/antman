package lucraft.mods.heroes.antman.proxies;

import java.util.HashMap;

import lucraft.mods.heroes.antman.AntMan;
import lucraft.mods.heroes.antman.client.AMClientEvents;
import lucraft.mods.heroes.antman.client.UpdateChecker;
import lucraft.mods.heroes.antman.client.book.BookPageAntManSuits;
import lucraft.mods.heroes.antman.client.book.BookPageBulletAnt;
import lucraft.mods.heroes.antman.client.book.BookPageCarpenterAnt;
import lucraft.mods.heroes.antman.client.models.ModelSizeChange;
import lucraft.mods.heroes.antman.client.render.AMClientRenderer;
import lucraft.mods.heroes.antman.client.render.RenderBulletAnt;
import lucraft.mods.heroes.antman.client.render.RenderCarpenterAnt;
import lucraft.mods.heroes.antman.client.render.RenderGrowDisc;
import lucraft.mods.heroes.antman.client.render.RenderLaser;
import lucraft.mods.heroes.antman.client.render.RenderShrinkDisc;
import lucraft.mods.heroes.antman.client.render.RenderSizeChange;
import lucraft.mods.heroes.antman.client.render.RenderSizeChangePlayer;
import lucraft.mods.heroes.antman.entity.EntityBulletAnt;
import lucraft.mods.heroes.antman.entity.EntityCarpenterAnt;
import lucraft.mods.heroes.antman.entity.EntityGrowDisc;
import lucraft.mods.heroes.antman.entity.EntityLaser;
import lucraft.mods.heroes.antman.entity.EntityShrinkDisc;
import lucraft.mods.heroes.antman.entity.EntitySizeChange;
import lucraft.mods.heroes.antman.entity.EntitySizeChangePlayer;
import lucraft.mods.heroes.antman.util.ShrinkerTypes;
import lucraft.mods.lucraftcore.client.gui.book.BookChapter;
import lucraft.mods.lucraftcore.client.gui.book.BookPageChapterTitle;
import lucraft.mods.lucraftcore.client.gui.book.heroguide.BookHeroGuide;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;
import net.minecraft.client.resources.model.ModelBakery;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class AntManClientProxy extends AntManProxy {

	@Override
	public void preInit() {
		UpdateChecker.init();
		super.preInit();
	}

	@SuppressWarnings({ "deprecation", "unchecked", "rawtypes" })
	@Override
	public void init() {
		super.init();
		MinecraftForge.EVENT_BUS.register(new AMClientRenderer());
		MinecraftForge.EVENT_BUS.register(new AMClientEvents());

		RenderingRegistry.registerEntityRenderingHandler(EntityShrinkDisc.class, new RenderShrinkDisc(Minecraft.getMinecraft().getRenderManager(), Minecraft.getMinecraft().getRenderItem()));
		RenderingRegistry.registerEntityRenderingHandler(EntityGrowDisc.class, new RenderGrowDisc(Minecraft.getMinecraft().getRenderManager(), Minecraft.getMinecraft().getRenderItem()));
		RenderingRegistry.registerEntityRenderingHandler(EntitySizeChange.class, new RenderSizeChange(new ModelSizeChange(), 0F));
		RenderingRegistry.registerEntityRenderingHandler(EntitySizeChangePlayer.class, new RenderSizeChangePlayer(Minecraft.getMinecraft().getRenderManager()));
		RenderingRegistry.registerEntityRenderingHandler(EntityLaser.class, new RenderLaser(Minecraft.getMinecraft().getRenderManager()));
		RenderingRegistry.registerEntityRenderingHandler(EntityBulletAnt.class, new RenderBulletAnt());
		RenderingRegistry.registerEntityRenderingHandler(EntityCarpenterAnt.class, new RenderCarpenterAnt());
	}

	@Override
	public void postInit() {
		super.postInit();
		initModels();
		
		addBookPages();
	}

	// -----------------------------------------------------------------------

	private void addBookPages() {
		BookChapter chapter = new BookChapter("antman", BookHeroGuide.guide);
		
		chapter.addPage(new BookPageChapterTitle(chapter));
		for(ShrinkerTypes type : ShrinkerTypes.values()) {
			chapter.addPage(new BookPageAntManSuits(chapter, type));
		}
		
		BookHeroGuide.guide.addChapter(chapter);
		
		//--------
		
		BookChapter chapterAnts = new BookChapter("antman_ants", BookHeroGuide.guide);
		chapterAnts.addPage(new BookPageChapterTitle(chapterAnts));
		chapterAnts.addPage(new BookPageBulletAnt(chapterAnts));
		chapterAnts.addPage(new BookPageCarpenterAnt(chapterAnts));
		BookHeroGuide.guide.addChapter(chapterAnts);
		
	}

	public static HashMap<ItemStack, String> models = new HashMap<ItemStack, String>();
	public static HashMap<ItemStack, Integer> modelMeta = new HashMap<ItemStack, Integer>();
	
	public void initModels() {
		for(ItemStack stack : models.keySet()) {
			ModelBakery.registerItemVariants(stack.getItem(), new ResourceLocation(AntMan.ASSETDIR + models.get(stack)));
			Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(stack.getItem(), modelMeta.get(stack), new ModelResourceLocation(AntMan.ASSETDIR + models.get(stack), "inventory"));
		}
	}
	
	@Override
	public void registerItemModel(Item item, String resource) {
		this.registerItemModel(item, 0, resource);
	}

	@Override
	public void registerItemModel(Item item, int meta, String resource) {
		ItemStack stack = new ItemStack(item);
		models.put(stack, resource);
		modelMeta.put(stack, meta);
	}

	@Override
	public void registerBlockModel(Block block, String resource) {
		this.registerBlockModel(block, 0, resource);
	}

	@Override
	public void registerBlockModel(Block block, int meta, String resource) {
		this.registerItemModel(Item.getItemFromBlock(block), meta, resource);
	}

	@Override
	public void registerFluidModels(Fluid fluid) {
		Item item = Item.getItemFromBlock(fluid.getBlock());
		ModelBakery.registerItemVariants(item);
		ModelLoader.registerItemVariants(item);
		FluidStateMapper mapper = new FluidStateMapper(fluid);
		ModelLoader.setCustomMeshDefinition(item, mapper);
		ModelLoader.setCustomStateMapper(fluid.getBlock(), mapper);
	}

	@Override
	public EntityPlayer getPlayerEntity(MessageContext ctx) {
		return (ctx.side.isClient() ? Minecraft.getMinecraft().thePlayer : super.getPlayerEntity(ctx));
	}

	public static class FluidStateMapper extends StateMapperBase implements ItemMeshDefinition {

		public final Fluid fluid;
		public final ModelResourceLocation location;

		public FluidStateMapper(Fluid fluid) {
			this.fluid = fluid;

			this.location = new ModelResourceLocation(new ResourceLocation(AntMan.ASSETDIR + fluid.getName()), fluid.getName());
		}

		@Override
		protected ModelResourceLocation getModelResourceLocation(IBlockState state) {
			return location;
		}

		@Override
		public ModelResourceLocation getModelLocation(ItemStack stack) {
			return location;
		}
	}

}
