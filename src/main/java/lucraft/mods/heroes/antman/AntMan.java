package lucraft.mods.heroes.antman;

import lucraft.mods.heroes.antman.proxies.AntManProxy;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = AntMan.MODID, version = AntMan.VERSION, name = AntMan.MODID, dependencies = "required-before:LucraftCore@[1.8.9-1.2.0,)")
public class AntMan {
	
	public static final String MODID = "AntMan";
	public static final String ASSETDIR = MODID + ":";
	public static final String VERSION = "1.8.9-1.2.0";

	static {
		FluidRegistry.enableUniversalBucket();
	}
	
	@SidedProxy(clientSide = "lucraft.mods.heroes.antman.proxies.AntManClientProxy", serverSide = "lucraft.mods.heroes.antman.proxies.AntManProxy")
	public static AntManProxy proxy;
	
	@Instance(value = AntMan.MODID)
	public static AntMan instance;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e) {
		proxy.preInit();
		AMConfig.preInit(e);
	}
	
	@EventHandler
	public void init(FMLInitializationEvent e) {
		proxy.init();
	}
	
	@EventHandler
	public void init(FMLPostInitializationEvent e) {
		proxy.postInit();
	}
}
