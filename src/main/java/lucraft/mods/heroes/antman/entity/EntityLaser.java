package lucraft.mods.heroes.antman.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityLaser extends EntityThrowable {

	public EntityLaser(World world, EntityLivingBase entity, double x, double y, double z) {
		super(world, entity);
		this.setPosition(x, y, z);
	}

	public EntityLaser(World world) {
		super(world);
	}

	public EntityLaser(World world, double par2, double par4, double par6) {
		super(world, par2, par4, par6);
	}
	
	@Override
	public void onUpdate() {
		super.onUpdate();
		this.worldObj.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, posX, posY, posZ, 0.0D, 0.0D, 0.0D);;
	}

	@Override
	protected void onImpact(MovingObjectPosition mop) {

		if (mop.entityHit != null) {
			mop.entityHit.attackEntityFrom(DamageSource.causeThrownDamage(this, this.getThrower()), 4);
			mop.entityHit.setFire(15);
		}

		if(!this.worldObj.isRemote) {
			this.worldObj.newExplosion((Entity) null, this.posX, this.posY, this.posZ, 0.5F, true, false);
		}
		
		this.worldObj.spawnParticle(EnumParticleTypes.FLAME, posX, posY, posZ, 0.0D, 0.0D, 0.0D);;

		this.setDead();

	}

	@Override
	protected float getGravityVelocity() {
		return 0.001F;
	}

	@Override
	protected float getVelocity() {
		return 5F;
	}
	
}
