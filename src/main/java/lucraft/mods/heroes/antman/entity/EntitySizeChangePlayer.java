package lucraft.mods.heroes.antman.entity;

import lucraft.mods.lucraftcore.modhelpers.antman.INotSizeChangeableEntity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class EntitySizeChangePlayer extends EntityLivingBase implements INotSizeChangeableEntity {

	public EntityLivingBase acquired;
	public float alpha = 1F;
	public float scale;
	public int progress;
	
	public EntitySizeChangePlayer(World worldIn) {
		super(worldIn);
		setSize(0.1F, 0.1F);
		noClip = true;
		renderDistanceWeight = 10D;
		ignoreFrustumCheck = true;
	}
	
	public EntitySizeChangePlayer(World par1World, EntityLivingBase ac) {
		super(par1World);
		this.acquired = ac;
		setSize(ac.width, ac.height);
		noClip = true;
		renderDistanceWeight = 10D;
		ignoreFrustumCheck = true;
		scale = AntManEntityData.get(acquired).size;
		
		setLocationAndAngles(acquired.posX, acquired.posY, acquired.posZ, acquired.rotationYaw, acquired.rotationPitch);
		
		//----------------------------------------------------------------------------
		
//		this.swingProgress = acquired.swingProgress;
//		this.prevSwingProgress = acquired.prevSwingProgress;
//		this.prevRenderYawOffset = acquired.prevRenderYawOffset;
//		this.renderYawOffset = acquired.renderYawOffset;
//		this.prevRotationYawHead = acquired.prevRotationYawHead;
//		this.rotationYawHead = acquired.rotationYawHead;
//		this.prevRotationPitch = acquired.prevRotationPitch;
//		this.rotationPitch = acquired.rotationPitch;
//		this.limbSwingAmount = acquired.limbSwingAmount;
//		this.prevLimbSwingAmount = acquired.prevLimbSwingAmount;
//		this.limbSwing = acquired.limbSwing;
		
		this.swingProgress = acquired.swingProgress;
		this.prevSwingProgress = acquired.swingProgress;
		this.prevRenderYawOffset = acquired.renderYawOffset;
		this.renderYawOffset = acquired.renderYawOffset;
		this.prevRotationYawHead = acquired.rotationYawHead;
		this.rotationYawHead = acquired.rotationYawHead;
		this.prevRotationPitch = acquired.rotationPitch;
		this.rotationPitch = acquired.rotationPitch;
		this.limbSwingAmount = acquired.limbSwingAmount;
		this.prevLimbSwingAmount = acquired.limbSwingAmount;
		this.limbSwing = acquired.limbSwing;
	}

	@Override
	public void onUpdate() {
		prevPosX = posX;
		prevPosY = posY;
		prevPosZ = posZ;

		progress++;
		
		if (progress >= 10) {
			setDead();
			return;
		}
	}
	
	@Override
	public boolean shouldRenderInPass(int pass) {
		return pass == 1;
	}

	@Override
	public boolean canBeCollidedWith() {
		return false;
	}

	@Override
	public boolean canBePushed() {
		return false;
	}

	@Override
	public boolean isEntityAlive() {
		return !this.isDead;
	}

	@Override
	public void setHealth(float par1) {
	}

	@Override
	public boolean writeToNBTOptional(NBTTagCompound par1NBTTagCompound) {
		return false;
	}

	@Override
	protected void entityInit() {
		super.entityInit();
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound nbttagcompound) {
	}

	@Override
	public void writeEntityToNBT(NBTTagCompound nbttagcompound) {
	}

	@Override
	public ItemStack getHeldItem() {
		return acquired.getHeldItem();
	}

	@Override
	public ItemStack getEquipmentInSlot(int i) {
		return acquired.getEquipmentInSlot(i);
	}

	@Override
	public ItemStack getCurrentArmor(int slotIn) {
		return acquired.getCurrentArmor(slotIn);
	}

	@Override
	public void setCurrentItemOrArmor(int i, ItemStack itemstack) {
	}

	@Override
	public ItemStack[] getInventory() {
		return new ItemStack[0];
	}

}
