package lucraft.mods.heroes.antman.entity;

import lucraft.mods.heroes.antman.dimensions.AMDimensions;
import lucraft.mods.heroes.antman.items.AMItems;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.item.Item;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntityShrinkDisc extends EntityThrowable {
	
	public boolean shrink;
	
	public EntityShrinkDisc(World worldIn) {
		super(worldIn);
	}

	public EntityShrinkDisc(World worldIn, EntityLivingBase throwerIn, boolean sf) {
		super(worldIn, throwerIn);
		this.shrink = sf;
	}

	public EntityShrinkDisc(World worldIn, double x, double y, double z) {
		super(worldIn, x, y, z);
	}

	/**
	 * Called when this EntityThrowable hits a block or entity.
	 */
	protected void onImpact(MovingObjectPosition mop) {
		if (!this.worldObj.isRemote && mop.entityHit != null) {
			if(mop.entityHit instanceof EntityLivingBase && mop.entityHit.dimension != AMDimensions.quantumRealmDimensionId) {
				sizeChange((EntityLivingBase) mop.entityHit);
			}
		}

		for (int k = 0; k < 8; ++k) {
			this.worldObj.spawnParticle(EnumParticleTypes.ITEM_CRACK, this.posX, this.posY, this.posZ, ((double) this.rand.nextFloat() - 0.5D) * 0.08D, ((double) this.rand.nextFloat() - 0.5D) * 0.08D, ((double) this.rand.nextFloat() - 0.5D) * 0.08D, new int[] { Item.getIdFromItem(AMItems.discShrink) });
		}

		if (!this.worldObj.isRemote) {
			this.setDead();
		}
	}
	
	public void sizeChange(EntityLivingBase entity) {
		AntManEntityData.get(entity).makeOneStepSmaller();
	}
}