package lucraft.mods.heroes.antman.entity;

import lucraft.mods.heroes.antman.AMConfig;
import lucraft.mods.heroes.antman.util.AntManHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EntityDamageSource;
import net.minecraftforge.event.entity.EntityEvent.EntityConstructing;
import net.minecraftforge.event.entity.living.LivingEvent.LivingJumpEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.EntityInteractEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerChangedDimensionEvent;

public class AntManEntityEvents {

	@SubscribeEvent
	public void onClonePlayer(PlayerEvent.Clone event) {
		NBTTagCompound compound = new NBTTagCompound();
		AntManEntityData.get(event.original).saveNBTData(compound);
		AntManEntityData.get(event.entityPlayer).loadNBTData(compound);
	}

	@SubscribeEvent
	public void onEntityConstructing(EntityConstructing event) {
		if (AntManHelper.isSizeChangeable(event.entity) && AntManEntityData.get((EntityLivingBase) event.entity) == null) {
			AntManEntityData.register((EntityLivingBase) event.entity);
		}

		if (AntManHelper.isSizeChangeable(event.entity) && event.entity.getExtendedProperties(AntManEntityData.EXT_PROP_NAME) == null) {
			event.entity.registerExtendedProperties(AntManEntityData.EXT_PROP_NAME, new AntManEntityData((EntityLivingBase) event.entity));
		}
	}

	@SubscribeEvent
	public void onDeath(net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerRespawnEvent e) {
		if (AntManHelper.isSizeChangeable(e.player))
			AntManEntityData.get(e.player).onRespawn();
	}
	
	@SubscribeEvent
	public void onUpdate(LivingUpdateEvent e) {
		if (AntManHelper.isSizeChangeable(e.entity))
			AntManEntityData.get(e.entityLiving).onUpdate();
	}

	@SubscribeEvent
	public void onDamage(LivingFallEvent e) {
		if(!AntManHelper.isNormalSize(e.entityLiving)) {
			float scale = AntManEntityData.get(e.entityLiving).size;
			if (scale > 0.5)
				e.distance /= scale;
			else
				e.distance *= scale;
			
			if (scale > 1 && e.entityLiving.isSneaking()) {
				float strength = e.entity.width / 1000F;
				System.out.println("" + strength);
				e.entity.worldObj.createExplosion(e.entity, e.entity.posX, e.entity.posY, e.entity.posZ, strength, false);
			}
		}
	}
	
	@SubscribeEvent
	public void onDamage(LivingHurtEvent e) {
		if(e.source instanceof EntityDamageSource && ((EntityDamageSource)e.source).getSourceOfDamage() != null && ((EntityDamageSource)e.source).getSourceOfDamage() instanceof EntityLivingBase) {
			EntityLivingBase en = (EntityLivingBase) ((EntityDamageSource)e.source).getSourceOfDamage();
			e.ammount += AntManHelper.getExtraDmgFromSize(AntManEntityData.get(en).size);
		}
		
		if(!AMConfig.disableDamageReduction && !AntManHelper.isNormalSize(e.entityLiving)) {
			float scale = AntManEntityData.get(e.entityLiving).size;
			
			if (scale > 0.5)
				e.ammount /= scale / 2;
			else
				e.ammount *= scale / 2;
		}
	}

	@SubscribeEvent
	public void onEntityJump(LivingJumpEvent event) {
		if (AntManHelper.isSizeChangeable(event.entity)) {
			float scale = AntManEntityData.get(event.entityLiving).size;
			if (scale > 1.2)
				event.entityLiving.motionY *= scale / 1.5F;
		}
	}
	
	@SubscribeEvent
	public void onDimChange(PlayerChangedDimensionEvent e) {
//		if(e.toDim != AMDimensions.quantumRealmDimensionId) {
//			e.player.capabilities.allowFlying = e.player.capabilities.isCreativeMode;
//		}
	}
	
	@SubscribeEvent
	public void onInteract(EntityInteractEvent e) {
//		if(e.target instanceof EntityLivingBase && AntManEntityData.get(e.entityPlayer).amSize == AntManSizes.SMALL && AntManHelper.isSizeChangeable(e.target)) {
//			AntManSizes pS = AntManEntityData.get(e.entityPlayer).amSize;
//			AntManSizes enS = AntManEntityData.get((EntityLivingBase) e.target).amSize;
//			
//			if(pS.getScale() < enS.getScale() && e.entityPlayer.height < e.target.height) {
//				e.entityPlayer.mountEntity(e.target);
//				e.target.updateRiderPosition();
//			}
//		}
	}
	
	@SubscribeEvent
	public void onUse(PlayerInteractEvent e) {
		
	}

}
