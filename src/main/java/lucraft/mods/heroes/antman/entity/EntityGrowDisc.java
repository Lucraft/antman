package lucraft.mods.heroes.antman.entity;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.world.World;

public class EntityGrowDisc extends EntityShrinkDisc {

	public EntityGrowDisc(World worldIn) {
		super(worldIn);
	}

	public EntityGrowDisc(World worldIn, EntityLivingBase throwerIn, boolean sf) {
		super(worldIn, throwerIn, sf);

	}

	public EntityGrowDisc(World worldIn, double x, double y, double z) {
		super(worldIn, x, y, z);
	}
	
	@Override
	public void sizeChange(EntityLivingBase entity) {
		AntManEntityData.get(entity).makeOneStepBigger();
	}
	
}
