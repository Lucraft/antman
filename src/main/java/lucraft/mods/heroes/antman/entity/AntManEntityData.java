package lucraft.mods.heroes.antman.entity;

import java.util.ArrayList;

import lucraft.mods.heroes.antman.AMConfig;
import lucraft.mods.heroes.antman.dimensions.AMDimensions;
import lucraft.mods.heroes.antman.dimensions.QuantumRealmTeleporter;
import lucraft.mods.heroes.antman.network.MessageSendFlyingUpdate;
import lucraft.mods.heroes.antman.network.MessageSizeChange;
import lucraft.mods.heroes.antman.network.MessageSpawnSizeChangeCopy;
import lucraft.mods.heroes.antman.network.PacketDispatcher;
import lucraft.mods.heroes.antman.proxies.AntManProxy;
import lucraft.mods.heroes.antman.util.AMSizes;
import lucraft.mods.heroes.antman.util.AMSizes.AntManSizes;
import lucraft.mods.heroes.antman.util.AMSounds;
import lucraft.mods.heroes.antman.util.AntManHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;

public class AntManEntityData implements IExtendedEntityProperties {

	public float size;
	public float estimatedSize;
	public boolean hasBeenChangedBefore;
	public float standardHeight;
	public float standardWidth;
	public float standardStepHeight;
	public float sizeDifference;
	public float progress;
	public float lastFallDistance;
	public int quantumRealmTeleportState;
	public LastDimensionPosData lastPosData;
	public boolean isFlying;

	public final static String EXT_PROP_NAME = "AntMan";

	public final EntityLivingBase entity;
	public final int tickAmount = AMConfig.sizeChangeTicks;

	private boolean isSizeChanging;

	public AntManEntityData(EntityLivingBase entity) {
		this.entity = entity;
		this.size = 1F;
		this.lastFallDistance = entity.fallDistance;
		this.quantumRealmTeleportState = 0;
		this.lastPosData = new LastDimensionPosData(entity.dimension, entity.posX, entity.posY, entity.posZ);
		this.isFlying = false;
	}

	public static final void register(EntityLivingBase entity) {
		entity.registerExtendedProperties(AntManEntityData.EXT_PROP_NAME, new AntManEntityData(entity));
	}

	public static final AntManEntityData get(EntityLivingBase entity) {
		return (AntManEntityData) entity.getExtendedProperties(EXT_PROP_NAME);
	}

	public int joinTimer = 60 * 20;

	public void onUpdate() {

		if (hasBeenChangedBefore && !AntManHelper.isNormalSize(entity) && !entity.worldObj.isRemote) {

			sendUpdatePackets();

			if (joinTimer > 0) {
				joinTimer--;
				setSize(size);
			} else {
				setSize(size);
			}

			if (entity instanceof EntityPlayer) {
				((EntityPlayer) entity).eyeHeight = 1.62F * size;
			}

			if (size > 1) {
				if (entity.fallDistance == 0 && entity.fallDistance != lastFallDistance) {
					if (lastFallDistance > (size / 2) && entity.isSneaking()) {
						entity.worldObj.createExplosion(entity, entity.posX, entity.posY, entity.posZ, lastFallDistance, false);
					}
				}
			}

			lastFallDistance = entity.fallDistance;

			if (size == 1F)
				hasBeenChangedBefore = false;
		}

		if (isSizeChanging) {

			size += (sizeDifference / tickAmount);

			if (entity.isEntityInsideOpaqueBlock()) {

			}

			float amount = 20;
			ArrayList<Float> spawnList = new ArrayList<Float>();
			for (int i = 0; i < amount; i++) {
				spawnList.add((float) ((tickAmount / amount) * (i + 1)));
			}

			if (!entity.worldObj.isRemote && spawnList.contains(progress)) {
				PacketDispatcher.sendToAllAround(new MessageSpawnSizeChangeCopy(entity), entity.dimension, entity.posX, entity.posY, entity.posZ, 40);
			}

			progress--;

			setSize(size);

			if (progress == 0) {
				this.size = estimatedSize;
				setSize(estimatedSize);
				isSizeChanging = false;

				/*
				 * 0 = Nichts 1 = In die Quantenebene 2 = Zur�ck in die Welt
				 */

				if (quantumRealmTeleportState != 0 && entity instanceof EntityPlayerMP) {
					EntityPlayerMP player = (EntityPlayerMP) entity;

					if (quantumRealmTeleportState == 1) {
						this.lastPosData = new LastDimensionPosData(player.dimension, player.posX, player.posY, player.posZ);
						player.mcServer.getConfigurationManager().transferPlayerToDimension(player, AMDimensions.quantumRealmDimensionId, new QuantumRealmTeleporter(player.getServerForPlayer()));
						quantumRealmTeleportState = 0;
						setSize(10);
						changeSize(AMSizes.normal, false);
					} else if (quantumRealmTeleportState == 2) {
						player.mcServer.getConfigurationManager().transferPlayerToDimension(player, this.lastPosData.dimensionId, new QuantumRealmTeleporter(player.getServerForPlayer()));
						quantumRealmTeleportState = 0;
						player.playerNetServerHandler.setPlayerLocation(this.lastPosData.posX, this.lastPosData.posY, this.lastPosData.posZ, player.rotationYaw, player.rotationPitch);
						setSize(AMSizes.small);
						changeSize(AMSizes.normal, false);
					}
				}
			}
		}

		if(entity instanceof EntityPlayer && isFlying && !AntManHelper.hasArmorOn((EntityPlayer) entity)) {
			sendFlyUpdatePackets(false);
		}
		
		if (entity.dimension == AMDimensions.quantumRealmDimensionId) {
			double y = 0D;

			if (AntManHelper.isEntityMoving(entity))
				y = -entity.rotationPitch / 1000D;

			entity.fallDistance = 0;
			entity.motionY = y;
			
			if(!AMConfig.disableQuantumRealmDamage && !(entity instanceof EntityPlayer && AntManHelper.hasArmorOn((EntityPlayer) entity))) {
				dmgTimer--;
				if(dmgTimer == 0) {
					dmgTimer = 2 * 20;
					entity.attackEntityFrom(AntManProxy.dmgSuffocate, 5);
				}
			}
		}
	}

	private int dmgTimer = 2 * 20;
	
	public void onRespawn() {
		size = 1F;
		setSize(1F);
	}

	private boolean isNearFloat(float normal, float par2, float testFloat) {
		if (testFloat > (normal - par2) && testFloat < (normal + par2))
			return true;
		else
			return false;
	}

	public void sendUpdatePackets() {
		if (hasBeenChangedBefore && !isNearFloat(1, 0.05F, size))
			PacketDispatcher.sendToAllAround(new MessageSizeChange(entity), entity.dimension, entity.posX, entity.posY, entity.posZ, 40);
	}
	
	public void sendFlyUpdatePackets(boolean f) {
		this.isFlying = f;
		if(!entity.worldObj.isRemote) {
			PacketDispatcher.sendToAllAround(new MessageSendFlyingUpdate(entity, isFlying), entity.dimension, entity.posX, entity.posY, entity.posZ, 50);
		}
	}

	public void saveStandardSize() {
		standardHeight = entity.height;
		standardWidth = entity.width;
		standardStepHeight = entity.stepHeight;
	}

	public void startSizeChange(float newEstimatedSize) {
		if (!isSizeChanging) {
			isSizeChanging = true;
			this.sizeDifference = newEstimatedSize - size;
			this.estimatedSize = newEstimatedSize;
			this.progress = tickAmount;
		}
	}

	public void setSize(float scale) {
		setSize(scale, true);
	}

	public void setSize(float scale, boolean update) {
		if (!hasBeenChangedBefore) {
			saveStandardSize();
			hasBeenChangedBefore = true;
		}
		size = scale;
		AntManHelper.setSize(entity, standardHeight, standardWidth, size);
		entity.stepHeight = standardStepHeight * scale;
		if (update)
			sendUpdatePackets();
	}

	public boolean changeSize(float size, boolean simulate) {
		if (size != this.size && !isSizeChanging) {
			if (!simulate) {
				entity.worldObj.playSoundAtEntity(entity, size < this.size ? AMSounds.shrink : AMSounds.grow, 1F, 1F);
				startSizeChange(size);
			}
			return true;
		}

		return false;
	}

	public boolean makeOneStepSmaller() {
		return changeSize(AntManSizes.getSmallerSize(AntManSizes.getSizeFromFloat(size)).getScale(), false);
	}

	public boolean makeOneStepSmaller(boolean simulate) {
		return changeSize(AntManSizes.getSmallerSize(AntManSizes.getSizeFromFloat(size)).getScale(), simulate);
	}

	public boolean makeOneStepBigger() {
		return changeSize(AntManSizes.getBiggerSize(AntManSizes.getSizeFromFloat(size)).getScale(), false);
	}

	public boolean makeOneStepBigger(boolean simulate) {
		return changeSize(AntManSizes.getBiggerSize(AntManSizes.getSizeFromFloat(size)).getScale(), simulate);
	}

	private final String nbtSize = "size";
	private final String nbtEstimatedSize = "estimatedSize";
	private final String nbtHasBeenChangedBefore = "hasBeenChangedBefore";
	private final String nbtStandardHeight = "standardHeight";
	private final String nbtStandardWidth = "standardWidth";
	private final String nbtStandardStepHeight = "standardStepHeight";
	private final String nbtSizeDifference = "sizeDifference";
	private final String nbtProgress = "progress";
	private final String nbtisSizeChanging = "isSizeChanging";
	private final String nbtQuantumRealmTeleportState = "quantumRealmTeleportState";
	private final String nbtLastPosDataDimensionId = "lastPosDataDimensionId";
	private final String nbtLastPosDataPosX = "lastPosDataPosX";
	private final String nbtLastPosDataPosY = "lastPosDataPosY";
	private final String nbtLastPosDataPosZ = "lastPosDataPosZ";
	private final String nbtIsFlying = "isFlying";

	@Override
	public void saveNBTData(NBTTagCompound compound) {
		NBTTagCompound properties = new NBTTagCompound();
		properties.setFloat(nbtSize, size);
		properties.setFloat(nbtEstimatedSize, estimatedSize);
		properties.setBoolean(nbtHasBeenChangedBefore, hasBeenChangedBefore);
		properties.setFloat(nbtStandardHeight, standardHeight);
		properties.setFloat(nbtStandardWidth, standardWidth);
		properties.setFloat(nbtStandardStepHeight, standardStepHeight);
		properties.setFloat(nbtSizeDifference, sizeDifference);
		properties.setFloat(nbtProgress, progress);
		properties.setBoolean(nbtisSizeChanging, isSizeChanging);
		properties.setInteger(nbtQuantumRealmTeleportState, quantumRealmTeleportState);
		properties.setInteger(nbtLastPosDataDimensionId, lastPosData.dimensionId);
		properties.setDouble(nbtLastPosDataPosX, lastPosData.posX);
		properties.setDouble(nbtLastPosDataPosY, lastPosData.posY);
		properties.setDouble(nbtLastPosDataPosZ, lastPosData.posZ);
		properties.setBoolean(nbtIsFlying, isFlying);
		compound.setTag(EXT_PROP_NAME, properties);
	}

	@Override
	public void loadNBTData(NBTTagCompound compound) {
		NBTTagCompound properties = (NBTTagCompound) compound.getTag(EXT_PROP_NAME);
		if(properties != null) {
			size = properties.getFloat(nbtSize);
			estimatedSize = properties.getFloat(nbtEstimatedSize);
			hasBeenChangedBefore = properties.getBoolean(nbtHasBeenChangedBefore);
			standardHeight = properties.getFloat(nbtStandardHeight);
			standardWidth = properties.getFloat(nbtStandardWidth);
			standardStepHeight = properties.getFloat(nbtStandardStepHeight);
			sizeDifference = properties.getFloat(nbtSizeDifference);
			progress = properties.getFloat(nbtProgress);
			isSizeChanging = properties.getBoolean(nbtisSizeChanging);
			quantumRealmTeleportState = properties.getInteger(nbtQuantumRealmTeleportState);
			lastPosData = new LastDimensionPosData(properties.getInteger(nbtLastPosDataDimensionId), properties.getDouble(nbtLastPosDataPosX), properties.getDouble(nbtLastPosDataPosY), properties.getDouble(nbtLastPosDataPosZ));
			isFlying = properties.getBoolean(nbtIsFlying);
		}
		
		if (size == 0F)
			size = 1F;

		if (standardStepHeight == 0F)
			standardStepHeight = 0.5F;

		if (estimatedSize == 0F)
			estimatedSize = 1F;
	}

	@Override
	public void init(Entity entity, World world) {

	}

	public class LastDimensionPosData {

		public int dimensionId;
		public double posX;
		public double posY;
		public double posZ;

		public LastDimensionPosData(int dimensionId, double posX, double posY, double posZ) {
			this.dimensionId = dimensionId;
			this.posX = posX;
			this.posY = posY;
			this.posZ = posZ;
		}

	}

}