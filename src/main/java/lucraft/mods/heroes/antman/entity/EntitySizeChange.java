package lucraft.mods.heroes.antman.entity;

import lucraft.mods.heroes.antman.client.models.ModelSizeChange;
import lucraft.mods.lucraftcore.modhelpers.antman.INotSizeChangeableEntity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class EntitySizeChange extends EntityLivingBase implements INotSizeChangeableEntity {

	public EntityLivingBase acquired;
	public float scale;
	
	public int progress;

	public ModelSizeChange model;

	public EntitySizeChange(World par1World) {
		super(par1World);
		model = new ModelSizeChange(this);
		setSize(0.1F, 0.1F);
		noClip = true;
		renderDistanceWeight = 10D;
		ignoreFrustumCheck = true;
	}

	public EntitySizeChange(World par1World, EntityLivingBase ac) {
		super(par1World);
		acquired = ac;
		model = new ModelSizeChange(this);
		progress = 0;
		setSize(0.1F, 0.1F);
		noClip = true;
		renderDistanceWeight = 10D;
		ignoreFrustumCheck = true;
		setLocationAndAngles(acquired.prevPosX, acquired.prevPosY, acquired.prevPosZ, acquired.rotationYaw, acquired.rotationPitch);
		motionX = ac.motionX * 0.4D;
		motionY = ac.motionY * 0.15D;
		motionZ = ac.motionZ * 0.4D;
		
		scale = AntManEntityData.get(acquired).size;
	}

	@Override
	public void onUpdate() {
		prevPosX = posX;
		prevPosY = posY;
		prevPosZ = posZ;

		progress++;
		
		if (progress >= 10) {
			setDead();
			return;
		}
	}

	@Override
	public boolean shouldRenderInPass(int pass) {
		return pass == 1;
	}

	@Override
	public boolean canBeCollidedWith() {
		return false;
	}

	@Override
	public boolean canBePushed() {
		return false;
	}

	@Override
	public boolean isEntityAlive() {
		return !this.isDead;
	}

	@Override
	public void setHealth(float par1) {
	}

	@Override
	public boolean writeToNBTOptional(NBTTagCompound par1NBTTagCompound) {
		return false;
	}

	@Override
	protected void entityInit() {
		super.entityInit();
	}

	@Override
	public void readEntityFromNBT(NBTTagCompound nbttagcompound) {
	}

	@Override
	public void writeEntityToNBT(NBTTagCompound nbttagcompound) {
	}

	@Override
	public ItemStack getHeldItem() {
		return acquired.getHeldItem();
	}

	@Override
	public ItemStack getEquipmentInSlot(int i) {
		return acquired.getEquipmentInSlot(i);
	}

	@Override
	public ItemStack getCurrentArmor(int slotIn) {
		return acquired.getCurrentArmor(slotIn);
	}

	@Override
	public void setCurrentItemOrArmor(int i, ItemStack itemstack) {
	}

	@Override
	public ItemStack[] getInventory() {
		return new ItemStack[0];
	}
}