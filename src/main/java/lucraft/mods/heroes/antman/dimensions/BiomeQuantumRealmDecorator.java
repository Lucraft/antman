package lucraft.mods.heroes.antman.dimensions;

import java.util.Random;

import lucraft.mods.heroes.antman.blocks.AMBlocks;
import net.minecraft.inventory.IInventory;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.WeightedRandomChestContent;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeDecorator;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.ChestGenHooks;

public class BiomeQuantumRealmDecorator extends BiomeDecorator {
	
	protected void genDecorations(BiomeGenBase biomeGenBaseIn) {

	}
	
	@Override
	public void decorate(World world, Random random, BiomeGenBase p_180292_3_, BlockPos pos) {
		for(int i = 0; i < random.nextInt(10) + 10; i++) {
			int posX = pos.getX() + random.nextInt(16);
			int posY = pos.getY() + random.nextInt(255);
			int posZ = pos.getZ() + random.nextInt(16);
			
			world.setBlockState(new BlockPos(posX, posY, posZ), AMBlocks.quantumRealmBlock.getDefaultState());
		}
		
		int j = random.nextInt(10);
		
		if(j >= 8) {
			BlockPos p = new BlockPos(pos.getX() + random.nextInt(16), pos.getY() + random.nextInt(255), pos.getZ() + random.nextInt(16));
//			BlockPos p = new BlockPos(-752, 62, -314);
			world.setBlockState(p, AMBlocks.quantumRealmChest.getDefaultState());
			TileEntity te = world.getTileEntity(p);
			ChestGenHooks gen = ChestGenHooks.getInfo(AMDimensions.lootTableString);
			gen.setMax(6);
			WeightedRandomChestContent.generateChestContents(random, gen.getItems(random), (IInventory) te, gen.getCount(random));
		}
	}
	
}