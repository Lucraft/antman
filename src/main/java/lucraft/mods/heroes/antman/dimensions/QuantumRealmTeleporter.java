package lucraft.mods.heroes.antman.dimensions;

import java.util.List;
import java.util.Random;

import com.google.common.collect.Lists;

import net.minecraft.entity.Entity;
import net.minecraft.util.BlockPos;
import net.minecraft.util.LongHashMap;
import net.minecraft.world.Teleporter;
import net.minecraft.world.WorldServer;

public class QuantumRealmTeleporter extends Teleporter {
	private final WorldServer worldServerInstance;
	/** A private Random() function in Teleporter */
	private final Random random;
	private final LongHashMap<QuantumRealmTeleporter.PortalPosition> destinationCoordinateCache = new LongHashMap();
	private final List<Long> destinationCoordinateKeys = Lists.<Long> newArrayList();

	public QuantumRealmTeleporter(WorldServer worldIn) {
		super(worldIn);
		this.worldServerInstance = worldIn;
		this.random = new Random(worldIn.getSeed());
	}

	public void placeInPortal(Entity entityIn, float rotationYaw) {

	}

	public boolean placeInExistingPortal(Entity entityIn, float rotationYaw) {
		return true;
	}

	public boolean makePortal(Entity p_85188_1_) {
		return true;
	}

	/**
	 * called periodically to remove out-of-date portal locations from the cache
	 * list. Argument par1 is a WorldServer.getTotalWorldTime() value.
	 */
	public void removeStalePortalLocations(long worldTime) {

	}

	public class PortalPosition extends BlockPos {
		public long lastUpdateTime;

		public PortalPosition(BlockPos pos, long lastUpdate) {
			super(pos.getX(), pos.getY(), pos.getZ());
			this.lastUpdateTime = lastUpdate;
		}
	}
}