package lucraft.mods.heroes.antman.dimensions;

import lucraft.mods.heroes.antman.AMConfig;
import lucraft.mods.heroes.antman.items.AMItems;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.WeightedRandomChestContent;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.ChestGenHooks;
import net.minecraftforge.common.DimensionManager;

public class AMDimensions {

	public static int quantumRealmDimensionId;
	public static int quantumRealmBiomeId;
	public static BiomeGenBase quantumRealmBiome;
	
	public static String lootTableString = "antman.quantumRealm";
	
	public static void init() {
		quantumRealmDimensionId = AMConfig.quantumRealmDimensionId;
		quantumRealmBiomeId = AMConfig.quantumRealmBiomeId;
		quantumRealmBiome = new BiomeQuantumRealm(quantumRealmBiomeId);
		
		DimensionManager.registerProviderType(quantumRealmDimensionId, QuantumRealmWorldProvider.class, true);
		DimensionManager.registerDimension(quantumRealmDimensionId, quantumRealmDimensionId);
		
		// Loot stuff
		ChestGenHooks chestLoot = ChestGenHooks.getInfo(lootTableString);
		chestLoot.addItem(new WeightedRandomChestContent(new ItemStack(Items.diamond), 2, 3, 1));
		chestLoot.addItem(new WeightedRandomChestContent(new ItemStack(Items.iron_ingot), 1, 5, 10));
		chestLoot.addItem(new WeightedRandomChestContent(new ItemStack(Items.gold_ingot), 1, 3, 5));
		chestLoot.addItem(new WeightedRandomChestContent(new ItemStack(Items.redstone), 1, 6, 10));
		chestLoot.addItem(new WeightedRandomChestContent(new ItemStack(Items.blaze_rod), 1, 2, 5));
		chestLoot.addItem(new WeightedRandomChestContent(new ItemStack(Blocks.planks), 1, 6, 20));
		chestLoot.addItem(new WeightedRandomChestContent(new ItemStack(AMItems.waspWing), 1, 2, 3));
		chestLoot.addItem(new WeightedRandomChestContent(new ItemStack(AMItems.regulator), 1, 1, 2));
		chestLoot.addItem(new WeightedRandomChestContent(new ItemStack(AMItems.empCummunicationDevice), 1, 1, 2));
		chestLoot.addItem(new WeightedRandomChestContent(new ItemStack(AMItems.antAntenna), 1, 1, 2));
		chestLoot.addItem(new WeightedRandomChestContent(new ItemStack(AMItems.filter), 1, 1, 2));
		chestLoot.addItem(new WeightedRandomChestContent(new ItemStack(AMItems.tankTier1), 1, 1, 5));
		chestLoot.addItem(new WeightedRandomChestContent(new ItemStack(AMItems.tankTier2), 1, 1, 3));
		chestLoot.addItem(new WeightedRandomChestContent(new ItemStack(AMItems.tankTier3), 1, 1, 1));
		chestLoot.addItem(new WeightedRandomChestContent(new ItemStack(Items.stick), 1, 3, 20));
		chestLoot.addItem(new WeightedRandomChestContent(new ItemStack(Items.gunpowder), 1, 4, 10));
		
		chestLoot.setMin(2);
		chestLoot.setMax(6);
	}
	
}
