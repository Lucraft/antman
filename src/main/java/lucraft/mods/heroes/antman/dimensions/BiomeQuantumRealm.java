package lucraft.mods.heroes.antman.dimensions;

import net.minecraft.init.Blocks;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BiomeQuantumRealm extends BiomeGenBase {

	public BiomeQuantumRealm(int id) {
		super(id);
		this.setDisableRain();
		this.setBiomeName("QuantumRealm");
		this.spawnableMonsterList.clear();
		this.spawnableCreatureList.clear();
		this.spawnableWaterCreatureList.clear();
		this.spawnableCaveCreatureList.clear();
		this.topBlock = Blocks.dirt.getDefaultState();
		this.fillerBlock = Blocks.dirt.getDefaultState();
		this.minHeight = 0;
		this.maxHeight = 0;
		this.theBiomeDecorator = new BiomeQuantumRealmDecorator();
	}

	@SideOnly(Side.CLIENT)
	public int getSkyColorByTemp(float p_76731_1_) {
		return 0;
	}

}
