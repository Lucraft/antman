package lucraft.mods.heroes.antman.dimensions;

import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.biome.WorldChunkManagerHell;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraftforge.client.IRenderHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class QuantumRealmWorldProvider extends WorldProvider {

	IRenderHandler skyRenderer;

	@Override
	public String getDimensionName() {
		return "QuantumRealm";
	}

	@Override
	public String getInternalNameSuffix() {
		return "_quantum_realm";
	}

	@Override
	protected void registerWorldChunkManager() {
		this.worldChunkMgr = new WorldChunkManagerHell(AMDimensions.quantumRealmBiome, 0F);
		this.dimensionId = AMDimensions.quantumRealmDimensionId;
		this.hasNoSky = true;
	}

	public IChunkProvider createChunkGenerator() {
		return new ChunkProviderQuantumRealm(worldObj, dimensionId);
	}

	public float calculateCelestialAngle(long p_76563_1_, float p_76563_3_) {
		return 0.0F;
	}

	@SideOnly(Side.CLIENT)
	public IRenderHandler getSkyRenderer() {
		return new QuantumRealmSkyRenderer();
	}

	@SideOnly(Side.CLIENT)
	public float[] calcSunriseSunsetColors(float celestialAngle, float partialTicks) {
		return null;
	}

	@SideOnly(Side.CLIENT)
	public Vec3 getFogColor(float p_76562_1_, float p_76562_2_) {
		int i = 10518688;
		float f = MathHelper.cos(p_76562_1_ * (float) Math.PI * 2.0F) * 2.0F + 0.5F;
		f = MathHelper.clamp_float(f, 0.0F, 1.0F);
		float f1 = (float) (i >> 16 & 255) / 255.0F;
		float f2 = (float) (i >> 8 & 255) / 255.0F;
		float f3 = (float) (i & 255) / 255.0F;
		f1 = f1 * (f * 0.0F + 0.15F);
		f2 = f2 * (f * 0.0F + 0.15F);
		f3 = f3 * (f * 0.0F + 0.15F);
		return new Vec3((double) f1, (double) f2, (double) f3);
	}

	@SideOnly(Side.CLIENT)
	public boolean isSkyColored() {
		return false;
	}

	public boolean canRespawnHere() {
		return false;
	}

	public boolean isSurfaceWorld() {
		return false;
	}

	@SideOnly(Side.CLIENT)
	public boolean doesXZShowFog(int x, int z) {
		return true;
	}

}
