package lucraft.mods.heroes.antman.tileentity;

import lucraft.mods.heroes.antman.blocks.BlockPymParticleProducer;
import lucraft.mods.heroes.antman.fluids.AMFluids;
import lucraft.mods.heroes.antman.util.IPymParticleContainer;
import lucraft.mods.heroes.antman.util.IPymParticleContainer.PymParticleVersion;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ContainerFurnace;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.SlotFurnaceFuel;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.ITickable;
import net.minecraft.util.MathHelper;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.IFluidHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TileEntityPymParticleProducer extends TileEntity implements ITickable, ISidedInventory, IFluidHandler {
	
    private static final int[] slotsTop = new int[] {0, 1, 2, 3, 6, 7};
    private static final int[] slotsBottom = new int[] {4, 5, 8};
    private static final int[] slotsSides = new int[] {4};
	
	private int slotAmount = 9;
	private ItemStack[] pppItemStacks = new ItemStack[slotAmount];

	private int producerBurnTime;
	private int currentItemBurnTime;
	private int cookTime;
	private int totalCookTime;
	
    public int capacity = 5000;
    public FluidTank water;
    public FluidTank shrinkPymParticles;
    public FluidTank growPymParticles;
    public int fluidPerTick = 1;
	public int amountPerBucket = 1000;
	public int requiredAmount = 500;

	public TileEntityPymParticleProducer() {
		water = new FluidTank(FluidRegistry.WATER, 0, capacity);
		shrinkPymParticles = new FluidTank(AMFluids.shrinkPymParticles, 0, capacity);
		growPymParticles = new FluidTank(AMFluids.growPymParticles, 0, capacity);
	}
	
	public int getSizeInventory() {
		return this.pppItemStacks.length;
	}

	public ItemStack getStackInSlot(int index) {
		return this.pppItemStacks[index];
	}

	public ItemStack decrStackSize(int index, int count) {
		if (this.pppItemStacks[index] != null) {
			if (this.pppItemStacks[index].stackSize <= count) {
				ItemStack itemstack1 = this.pppItemStacks[index];
				this.pppItemStacks[index] = null;
				return itemstack1;
			} else {
				ItemStack itemstack = this.pppItemStacks[index].splitStack(count);

				if (this.pppItemStacks[index].stackSize == 0) {
					this.pppItemStacks[index] = null;
				}

				return itemstack;
			}
		} else {
			return null;
		}
	}

	public ItemStack removeStackFromSlot(int index) {
		if (this.pppItemStacks[index] != null) {
			ItemStack itemstack = this.pppItemStacks[index];
			this.pppItemStacks[index] = null;
			return itemstack;
		} else {
			return null;
		}
	}

	public void setInventorySlotContents(int index, ItemStack stack) {
		boolean flag = stack != null && stack.isItemEqual(this.pppItemStacks[index]) && ItemStack.areItemStackTagsEqual(stack, this.pppItemStacks[index]);
		this.pppItemStacks[index] = stack;

		if (stack != null && stack.stackSize > this.getInventoryStackLimit()) {
			stack.stackSize = this.getInventoryStackLimit();
		}

		if (index == 0 && !flag) {
			this.totalCookTime = this.getCookTime(stack);
			this.cookTime = 0;
			this.markDirty();
		}
	}

	public String getName() {
		return "container.pymParticleProducer";
	}

	public boolean hasCustomName() {
		return false;
	}
	
	@Override
	public void writeToNBT(NBTTagCompound compound) {
		super.writeToNBT(compound);
		this.writeCustomToNBT(compound);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);
        this.readCustomFromNBT(compound);
	}

	public int getInventoryStackLimit() {
		return 64;
	}

	public boolean isBurning() {
		return this.producerBurnTime > 0;
	}

	@SideOnly(Side.CLIENT)
	public static boolean isBurning(IInventory p_174903_0_) {
		return p_174903_0_.getField(0) > 0;
	}

	public boolean areInputSlotsOccupied() {
		return this.pppItemStacks[1] != null && this.pppItemStacks[2] != null && this.pppItemStacks[3] != null && this.water != null;
	}
	
	public boolean hasEnoughAmount(FluidTank tank, int amount) {
		return tank.getFluidAmount() >= amount; 
	}
	
	public int getPossibleFluidAmount(FluidTank tank, IPymParticleContainer container, ItemStack stack, PymParticleVersion version) {
		int amount = container.getMaxPymParticles(version, stack) - container.getPymParticles(version, stack);
		
		if(amount < tank.getFluidAmount())
			return amount;
		else
			return tank.getFluidAmount();
	}
	
	public void update() {
		boolean flag = this.isBurning();
		boolean flag1 = false;

		if(pppItemStacks[0] != null && FluidContainerRegistry.isContainer(pppItemStacks[0]) && FluidContainerRegistry.isFilledContainer(pppItemStacks[0])) {
			FluidStack stack = FluidContainerRegistry.getFluidForFilledItem(pppItemStacks[0]);
			
			if(stack.getFluid() == FluidRegistry.WATER && water.getFluidAmount() < water.getCapacity()) {
				water.fill(stack, true);
				setInventorySlotContents(0, FluidContainerRegistry.drainFluidContainer(pppItemStacks[0]));
			}
		}
		
		//----------------------------------------------------------------------------------------------------------------------
		
		// Load Shrink Tanks
		if(pppItemStacks[5] != null) {
			if(pppItemStacks[5].getItem() instanceof IPymParticleContainer) {
				IPymParticleContainer pymTank = ((IPymParticleContainer)pppItemStacks[5].getItem());
				
				if(pymTank.canAddPymParticles(PymParticleVersion.SHRINK, pppItemStacks[5], fluidPerTick) && shrinkPymParticles.getFluidAmount() >= fluidPerTick) {
					pymTank.addPymParticles(PymParticleVersion.SHRINK, pppItemStacks[5], fluidPerTick, false);
					this.shrinkPymParticles.drain(fluidPerTick, true);
				}
			}
//			else if (pppItemStacks[5].getItem() == Items.bucket && this.shrinkPymParticles.getFluidAmount() >= FluidContainerRegistry.BUCKET_VOLUME) {
//				this.shrinkPymParticles.drain(FluidContainerRegistry.BUCKET_VOLUME, true);
//				this.setInventorySlotContents(5, FluidContainerRegistry.fillFluidContainer(new FluidStack(AMFluids.shrinkPymParticles, FluidContainerRegistry.BUCKET_VOLUME), pppItemStacks[5]));
//			}
		}
		
		//----------------------------------------------------------------------------------------------------------------------
		
		// Unload Shrink Tanks
		if(pppItemStacks[6] != null) {
			if(pppItemStacks[6].getItem() instanceof IPymParticleContainer) {
				IPymParticleContainer pymTank = ((IPymParticleContainer)pppItemStacks[6].getItem());
				
				if(pymTank.canRemovePymParticles(PymParticleVersion.SHRINK, pppItemStacks[6], fluidPerTick) && shrinkPymParticles.getFluidAmount() <= shrinkPymParticles.getCapacity() - fluidPerTick) {
					pymTank.removePymParticles(PymParticleVersion.SHRINK, pppItemStacks[6], fluidPerTick, false);
					this.shrinkPymParticles.fill(new FluidStack(AMFluids.shrinkPymParticles, fluidPerTick), true);
				}
			}
//			else if (FluidContainerRegistry.isContainer(pppItemStacks[6]) && FluidContainerRegistry.isFilledContainer(pppItemStacks[6]) && FluidContainerRegistry.getFluidForFilledItem(pppItemStacks[6]).getFluid() == AMFluids.shrinkPymParticles) {
//				this.shrinkPymParticles.fill(new FluidStack(AMFluids.shrinkPymParticles, FluidContainerRegistry.BUCKET_VOLUME), true);
//				FluidContainerRegistry.drainFluidContainer(pppItemStacks[6]);
//			}
		}
		
		//----------------------------------------------------------------------------------------------------------------------
		
		// Load Shrink Tanks
		if(pppItemStacks[8] != null && pppItemStacks[8].getItem() instanceof IPymParticleContainer) {
			IPymParticleContainer pymTank = ((IPymParticleContainer)pppItemStacks[8].getItem());
			
			if(pymTank.canAddPymParticles(PymParticleVersion.GROW, pppItemStacks[8], fluidPerTick) && growPymParticles.getFluidAmount() >= fluidPerTick) {
				pymTank.addPymParticles(PymParticleVersion.GROW, pppItemStacks[8], fluidPerTick, false);
				this.growPymParticles.drain(fluidPerTick, true);
			}
		}
		
		//----------------------------------------------------------------------------------------------------------------------
		
		// Unload Shrink Tanks
		if(pppItemStacks[7] != null && pppItemStacks[7].getItem() instanceof IPymParticleContainer) {
			IPymParticleContainer pymTank = ((IPymParticleContainer)pppItemStacks[7].getItem());
			
			if(pymTank.canRemovePymParticles(PymParticleVersion.GROW, pppItemStacks[7], fluidPerTick) && growPymParticles.getFluidAmount() <= growPymParticles.getCapacity() - fluidPerTick) {
				pymTank.removePymParticles(PymParticleVersion.GROW, pppItemStacks[7], fluidPerTick, false);
				this.growPymParticles.fill(new FluidStack(AMFluids.growPymParticles, fluidPerTick), true);
			}
		}
		
		//----------------------------------------------------------------------------------------------------------------------
		
		if (this.isBurning()) {
			--this.producerBurnTime;
		}

		if (!this.worldObj.isRemote) {
			if (this.isBurning() || this.pppItemStacks[4] != null && areInputSlotsOccupied()) {
				if (!this.isBurning() && this.canSmelt()) {
					this.currentItemBurnTime = this.producerBurnTime = getItemBurnTime(this.pppItemStacks[4]);

					if (this.isBurning()) {
						flag1 = true;

						if (this.pppItemStacks[4] != null) {
							--this.pppItemStacks[4].stackSize;

							if (this.pppItemStacks[4].stackSize == 0) {
								this.pppItemStacks[4] = pppItemStacks[4].getItem().getContainerItem(pppItemStacks[4]);
							}
						}
					}
				}

				if (this.isBurning() && this.canSmelt()) {
					++this.cookTime;

					if (this.cookTime == this.totalCookTime) {
						this.cookTime = 0;
						this.totalCookTime = 200;
						this.smeltItem();
						flag1 = true;
					}
				} else {
					this.cookTime = 0;
				}
			} else if (!this.isBurning() && this.cookTime > 0) {
				this.cookTime = MathHelper.clamp_int(this.cookTime - 2, 0, this.totalCookTime);
			}

			if (flag != this.isBurning()) {
				flag1 = true;
				BlockPymParticleProducer.setState(this.isBurning(), this.worldObj, this.pos);
			}
		}
		
		if (flag1) {
			this.markDirty();
		}
	}

	public int getCookTime(ItemStack stack) {
		return 200;
	}

	private boolean canSmelt() {
		if(!areInputSlotsOccupied())
			return false;
		else {
			
			if(water.getFluidAmount() >= requiredAmount && pppItemStacks[3].getItem() == Items.ender_pearl && pppItemStacks[1].getItem() == Items.glowstone_dust) {
				
				if(pppItemStacks[2].getItem() == Items.dye && pppItemStacks[2].getItem().getDamage(pppItemStacks[2]) == 15)
					return growPymParticles.getFluidAmount() < capacity;
				
				else if(pppItemStacks[2].getItem() == Items.redstone)
					return shrinkPymParticles.getFluidAmount() < capacity;
			}
			
			return false;
		}
	}

	public void smeltItem() {
		if (this.canSmelt()) {
			
			if(pppItemStacks[2].getItem() == Items.redstone)
				fillShrinkPymParticles(requiredAmount);
			else if(pppItemStacks[2].getItem() == Items.dye)
				fillGrowPymParticles(requiredAmount);
			
			--this.pppItemStacks[1].stackSize;
			--this.pppItemStacks[2].stackSize;
			--this.pppItemStacks[3].stackSize;

			if (this.pppItemStacks[1].stackSize <= 0)
				this.pppItemStacks[1] = null;
			if (this.pppItemStacks[2].stackSize <= 0)
				this.pppItemStacks[2] = null;
			if (this.pppItemStacks[3].stackSize <= 0)
				this.pppItemStacks[3] = null;
			
			drainWater(requiredAmount);
		}
	}

	public static int getItemBurnTime(ItemStack p_145952_0_) {
		if (p_145952_0_ == null) {
			return 0;
		} else {
			Item item = p_145952_0_.getItem();

			if (item instanceof ItemBlock && Block.getBlockFromItem(item) != Blocks.air) {
				Block block = Block.getBlockFromItem(item);

				if (block == Blocks.wooden_slab) {
					return 150;
				}

				if (block.getMaterial() == Material.wood) {
					return 300;
				}

				if (block == Blocks.coal_block) {
					return 16000;
				}
			}

			if (item instanceof ItemTool && ((ItemTool) item).getToolMaterialName().equals("WOOD"))
				return 200;
			if (item instanceof ItemSword && ((ItemSword) item).getToolMaterialName().equals("WOOD"))
				return 200;
			if (item instanceof ItemHoe && ((ItemHoe) item).getMaterialName().equals("WOOD"))
				return 200;
			if (item == Items.stick)
				return 100;
			if (item == Items.coal)
				return 1600;
			if (item == Items.lava_bucket)
				return 20000;
			if (item == Item.getItemFromBlock(Blocks.sapling))
				return 100;
			if (item == Items.blaze_rod)
				return 2400;
			return net.minecraftforge.fml.common.registry.GameRegistry.getFuelValue(p_145952_0_);
		}
	}

	public static boolean isItemFuel(ItemStack p_145954_0_) {
		return getItemBurnTime(p_145954_0_) > 0;
	}

	public boolean isUseableByPlayer(EntityPlayer player) {
		return this.worldObj.getTileEntity(this.pos) != this ? false : player.getDistanceSq((double) this.pos.getX() + 0.5D, (double) this.pos.getY() + 0.5D, (double) this.pos.getZ() + 0.5D) <= 64.0D;
	}

	public void openInventory(EntityPlayer player) {
	}

	public void closeInventory(EntityPlayer player) {
	}

	public boolean isItemValidForSlot(int index, ItemStack stack) {
		return index == 2 ? false : (index != 1 ? true : isItemFuel(stack) || SlotFurnaceFuel.isBucket(stack));
	}

	public int[] getSlotsForFace(EnumFacing side) {
		return side == EnumFacing.DOWN ? slotsBottom : (side == EnumFacing.UP ? slotsTop : slotsSides);
	}

	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction) {
		return this.isItemValidForSlot(index, itemStackIn);
	}

	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction) {
		if (direction == EnumFacing.DOWN && index == 1) {
			Item item = stack.getItem();

			if (item != Items.water_bucket && item != Items.bucket) {
				return false;
			}
		}

		return true;
	}

	public String getGuiID() {
		return "antman:pymParticleProducer";
	}

	public Container createContainer(InventoryPlayer playerInventory, EntityPlayer playerIn) {
		return new ContainerFurnace(playerInventory, this);
	}

	public int getField(int id) {
		switch (id) {
		case 0:
			return this.producerBurnTime;
		case 1:
			return this.currentItemBurnTime;
		case 2:
			return this.cookTime;
		case 3:
			return this.totalCookTime;
		default:
			return 0;
		}
	}

	public void setField(int id, int value) {
		switch (id) {
		case 0:
			this.producerBurnTime = value;
			break;
		case 1:
			this.currentItemBurnTime = value;
			break;
		case 2:
			this.cookTime = value;
			break;
		case 3:
			this.totalCookTime = value;
		}
	}

	public int getFieldCount() {
		return 4;
	}

	public void clear() {
		for (int i = 0; i < this.pppItemStacks.length; ++i) {
			this.pppItemStacks[i] = null;
		}
	}

	net.minecraftforge.items.IItemHandler handlerTop = new net.minecraftforge.items.wrapper.SidedInvWrapper(this, net.minecraft.util.EnumFacing.UP);
	net.minecraftforge.items.IItemHandler handlerBottom = new net.minecraftforge.items.wrapper.SidedInvWrapper(this, net.minecraft.util.EnumFacing.DOWN);
	net.minecraftforge.items.IItemHandler handlerSide = new net.minecraftforge.items.wrapper.SidedInvWrapper(this, net.minecraft.util.EnumFacing.WEST);

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getCapability(net.minecraftforge.common.capabilities.Capability<T> capability, net.minecraft.util.EnumFacing facing) {
		if (facing != null && capability == net.minecraftforge.items.CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
			if (facing == EnumFacing.DOWN)
				return (T) handlerBottom;
			else if (facing == EnumFacing.UP)
				return (T) handlerTop;
			else
				return (T) handlerSide;
		return super.getCapability(capability, facing);
	}

	public void fillWater(int amount) {
		this.water.fill(new FluidStack(FluidRegistry.WATER, amount), true);
		this.worldObj.markBlockForUpdate(getPos());
	}
	
	public void fillShrinkPymParticles(int amount) {
		this.shrinkPymParticles.fill(new FluidStack(AMFluids.shrinkPymParticles, amount), true);
		this.worldObj.markBlockForUpdate(getPos());
	}
	
	public void fillGrowPymParticles(int amount) {
		this.growPymParticles.fill(new FluidStack(AMFluids.growPymParticles, amount), true);
		this.worldObj.markBlockForUpdate(getPos());
	}
	
	public void drainWater(int amount) {
		this.water.drain(amount, true);
		this.worldObj.markBlockForUpdate(getPos());
	}
	
	public void drainShrinkPymParticles(int amount) {
		this.shrinkPymParticles.drain(amount, true);
		this.worldObj.markBlockForUpdate(getPos());
	}
	
	public void drainGrowPymParticles(int amount) {
		this.shrinkPymParticles.drain(amount, true);
		this.worldObj.markBlockForUpdate(getPos());
	}
	
	@Override
	public IChatComponent getDisplayName() {
		return new ChatComponentText("ppp");
	}

	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt) {
        readCustomFromNBT(pkt.getNbtCompound());
        worldObj.markBlockRangeForRenderUpdate(getPos(), getPos());
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public Packet getDescriptionPacket() {
        NBTTagCompound tag = new NBTTagCompound();
        writeCustomToNBT(tag);
        return new S35PacketUpdateTileEntity(getPos(), 1, tag);
	}
	
	@Override
	public boolean canDrain(EnumFacing facing, Fluid fluid) {
		if(fluid == this.shrinkPymParticles.getFluid().getFluid())
			return facing == EnumFacing.NORTH || facing == EnumFacing.SOUTH;
		else if(fluid == this.growPymParticles.getFluid().getFluid())
			return facing == EnumFacing.EAST || facing == EnumFacing.WEST;
		else if(fluid == this.water.getFluid().getFluid())
			return facing == EnumFacing.DOWN || facing == EnumFacing.UP;
		
		return false;
	}

	@Override
	public boolean canFill(EnumFacing facing, Fluid fluid) {
		
		if(fluid == this.shrinkPymParticles.getFluid().getFluid())
			return facing == EnumFacing.NORTH || facing == EnumFacing.SOUTH;
		else if(fluid == this.growPymParticles.getFluid().getFluid())
			return facing == EnumFacing.EAST || facing == EnumFacing.WEST;
		else if(fluid == this.water.getFluid().getFluid())
			return facing == EnumFacing.DOWN || facing == EnumFacing.UP;
		
		return false;
	}

	@Override
	public FluidStack drain(EnumFacing facing, FluidStack fluid, boolean doDrain) {
		this.worldObj.markBlockForUpdate(getPos());
		
		if(!canDrain(facing, fluid.getFluid())) {
			return null;
		}
		
		return drain(facing, fluid.amount, doDrain);
	}

	@Override
	public FluidStack drain(EnumFacing facing, int amount, boolean doDrain) {
		this.worldObj.markBlockForUpdate(getPos());
		
		if(facing == EnumFacing.NORTH || facing == EnumFacing.SOUTH)
			return shrinkPymParticles.drain(amount, doDrain);
		else if(facing == EnumFacing.EAST || facing == EnumFacing.WEST)
			return growPymParticles.drain(amount, doDrain);
		else if(facing == EnumFacing.UP || facing == EnumFacing.DOWN)
			return water.drain(amount, doDrain);
		
		return null;
	}

	@Override
	public int fill(EnumFacing facing, FluidStack fluid, boolean doFill) {
		
		if(canFill(facing, fluid.getFluid())) {
			if(facing == EnumFacing.NORTH || facing == EnumFacing.SOUTH)
				return shrinkPymParticles.fill(fluid, doFill);
			else if(facing == EnumFacing.EAST || facing == EnumFacing.WEST)
				return growPymParticles.fill(fluid, doFill);
			else if(facing == EnumFacing.UP || facing == EnumFacing.DOWN)
				return water.fill(fluid, doFill);
		}
		
		return 0;
	}

	@Override
	public FluidTankInfo[] getTankInfo(EnumFacing from) {
		return new FluidTankInfo[]{new FluidTankInfo(water), new FluidTankInfo(shrinkPymParticles), new FluidTankInfo(growPymParticles)};
	}
	
	public void readCustomFromNBT(NBTTagCompound compound) {
		NBTTagList nbttaglist = compound.getTagList("Items", 10);
		this.pppItemStacks = new ItemStack[this.getSizeInventory()];

		for (int i = 0; i < nbttaglist.tagCount(); ++i) {
			NBTTagCompound nbttagcompound = nbttaglist.getCompoundTagAt(i);
			int j = nbttagcompound.getByte("Slot");

			if (j >= 0 && j < this.pppItemStacks.length) {
				this.pppItemStacks[j] = ItemStack.loadItemStackFromNBT(nbttagcompound);
			}
		}

		this.producerBurnTime = compound.getInteger("BurnTime");
		this.cookTime = compound.getInteger("CookTime");
		this.totalCookTime = compound.getInteger("CookTimeTotal");
		this.currentItemBurnTime = getItemBurnTime(this.pppItemStacks[1]);
        
        if(compound.hasKey("WaterTank")) {
        	water.readFromNBT(compound.getCompoundTag("WaterTank"));
        } if(compound.hasKey("ShrinkPymParticlesTank")) {
        	shrinkPymParticles.readFromNBT(compound.getCompoundTag("ShrinkPymParticlesTank"));
        } if(compound.hasKey("GrowPymParticlesTank")) {
        	growPymParticles.readFromNBT(compound.getCompoundTag("GrowPymParticlesTank"));
        }
	}
	
	public void writeCustomToNBT(NBTTagCompound compound) {
		compound.setInteger("BurnTime", this.producerBurnTime);
		compound.setInteger("CookTime", this.cookTime);
		compound.setInteger("CookTimeTotal", this.totalCookTime);
		NBTTagList nbttaglist = new NBTTagList();

		for (int i = 0; i < this.pppItemStacks.length; ++i) {
			if (this.pppItemStacks[i] != null) {
				NBTTagCompound nbttagcompound = new NBTTagCompound();
				nbttagcompound.setByte("Slot", (byte) i);
				this.pppItemStacks[i].writeToNBT(nbttagcompound);
				nbttaglist.appendTag(nbttagcompound);
			}
		}

		compound.setTag("Items", nbttaglist);
        
        if(water != null) {
        	NBTTagCompound waterCompound = new NBTTagCompound();
        	water.writeToNBT(waterCompound);
        	compound.setTag("WaterTank", waterCompound);
        } 
        if(shrinkPymParticles != null) {
        	NBTTagCompound lavaCompound = new NBTTagCompound();
        	shrinkPymParticles.writeToNBT(lavaCompound);
        	compound.setTag("ShrinkPymParticlesTank", lavaCompound);
        }
        if(growPymParticles != null) {
        	NBTTagCompound pymParticlesCompound = new NBTTagCompound();
        	growPymParticles.writeToNBT(pymParticlesCompound);
        	compound.setTag("GrowPymParticlesTank", pymParticlesCompound);
        }
	}

}
