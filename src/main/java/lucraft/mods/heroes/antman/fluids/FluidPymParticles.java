package lucraft.mods.heroes.antman.fluids;

import lucraft.mods.heroes.antman.AntMan;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;

public class FluidPymParticles extends Fluid {

	public FluidPymParticles(String fluidName, BlockFluidClassic block) {
		super(fluidName, new ResourceLocation(AntMan.ASSETDIR + "blocks/fluids/" + fluidName + "_still"), new ResourceLocation(AntMan.ASSETDIR + "blocks/fluids/" + fluidName + "_flow"));
		
		this.setUnlocalizedName(fluidName);
		FluidRegistry.registerFluid(this);
		setBlock(block);
	}
	
}
