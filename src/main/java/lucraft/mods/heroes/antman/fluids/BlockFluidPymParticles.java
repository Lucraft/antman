package lucraft.mods.heroes.antman.fluids;

import lucraft.mods.heroes.antman.blocks.AMBlocks;
import lucraft.mods.heroes.antman.entity.AntManEntityData;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class BlockFluidPymParticles extends BlockFluidClassic {

	public BlockFluidPymParticles(Fluid fluid) {
		super(fluid, Material.water);
		
		String name = fluid.getName().toLowerCase().replace("fluid.", "");
		this.setUnlocalizedName(name);
		GameRegistry.registerBlock(this, name);
	}
	
	@Override
	public void onEntityCollidedWithBlock(World worldIn, BlockPos pos, Entity entity) {
		
		if(entity instanceof EntityLivingBase) {
			AntManEntityData data = AntManEntityData.get((EntityLivingBase) entity);
			
			if(this == AMFluids.blockFluidShrinkPymParticles)
				data.makeOneStepSmaller();
			else
				data.makeOneStepBigger();
		}
		
		super.onEntityCollidedWithBlock(worldIn, pos, entity);
	}

	@Override
	public void onBlockAdded(World world, BlockPos pos, IBlockState state) {
		super.onBlockAdded(world, pos, state);
		mix(world, pos, state);
	}
	
	@Override
	public void onNeighborBlockChange(World world, BlockPos pos, IBlockState state, Block neighborBlock) {
		super.onNeighborBlockChange(world, pos, state, neighborBlock);
		mix(world, pos, state);
	}
	
	public void mix(World world, BlockPos pos, IBlockState state) {
		if(this == AMFluids.blockFluidShrinkPymParticles) {
			
			for(EnumFacing facing : EnumFacing.values()) {
				Block block = world.getBlockState(pos.offset(facing)).getBlock();
				if(block == AMFluids.blockFluidGrowPymParticles) {
					
					world.setBlockState(pos.offset(facing), AMBlocks.quantumRealmBlock.getDefaultState());
				}
			}
			
		}
	}
	
}
