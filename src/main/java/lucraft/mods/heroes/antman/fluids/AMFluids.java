package lucraft.mods.heroes.antman.fluids;

import lucraft.mods.heroes.antman.AntMan;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;

public class AMFluids {

	public static Fluid shrinkPymParticles;
	public static BlockFluidClassic blockFluidShrinkPymParticles;
	
	public static Fluid growPymParticles;
	public static BlockFluidClassic blockFluidGrowPymParticles;

	public static void init() {
		
		shrinkPymParticles = new FluidPymParticles("shrinkpymparticles", blockFluidShrinkPymParticles);
		blockFluidShrinkPymParticles = new BlockFluidPymParticles(shrinkPymParticles);
		
		growPymParticles = new FluidPymParticles("growpymparticles", blockFluidGrowPymParticles);
		blockFluidGrowPymParticles = new BlockFluidPymParticles(growPymParticles);
		
		AntMan.proxy.registerFluidModels(shrinkPymParticles);
		AntMan.proxy.registerFluidModels(growPymParticles);
		
		FluidRegistry.addBucketForFluid(shrinkPymParticles);
		FluidRegistry.addBucketForFluid(growPymParticles);
	}
	
}
