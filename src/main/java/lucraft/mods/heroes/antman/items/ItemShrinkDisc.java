package lucraft.mods.heroes.antman.items;

import lucraft.mods.heroes.antman.dimensions.AMDimensions;
import lucraft.mods.heroes.antman.entity.AntManEntityData;
import lucraft.mods.heroes.antman.entity.EntityGrowDisc;
import lucraft.mods.heroes.antman.entity.EntityShrinkDisc;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.StatList;
import net.minecraft.world.World;

public class ItemShrinkDisc extends ItemAMBase {

	public boolean shrink;

	public ItemShrinkDisc(String name, boolean shrink) {
		super(name);
		this.shrink = shrink;
		this.setMaxStackSize(16);
	}

	public ItemStack onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn) {
		if (!playerIn.capabilities.isCreativeMode && playerIn.dimension != AMDimensions.quantumRealmDimensionId) {
			--itemStackIn.stackSize;
		}

		if (!worldIn.isRemote) {
			if (playerIn.isSneaking()) {
				if(playerIn.dimension != AMDimensions.quantumRealmDimensionId) {
					if(shrink)
						AntManEntityData.get(playerIn).makeOneStepSmaller();
					else
						AntManEntityData.get(playerIn).makeOneStepBigger();
				}
				return itemStackIn;
			} else {
				if(shrink)
					worldIn.spawnEntityInWorld(new EntityShrinkDisc(worldIn, playerIn, shrink));
				else
					worldIn.spawnEntityInWorld(new EntityGrowDisc(worldIn, playerIn, !shrink));
				worldIn.playSoundAtEntity(playerIn, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));
			}
		}

		playerIn.triggerAchievement(StatList.objectUseStats[Item.getIdFromItem(this)]);
		return itemStackIn;
	}
}
