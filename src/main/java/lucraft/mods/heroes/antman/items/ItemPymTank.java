package lucraft.mods.heroes.antman.items;

import java.util.List;

import com.mojang.realmsclient.gui.ChatFormatting;

import lucraft.mods.heroes.antman.AntMan;
import lucraft.mods.heroes.antman.proxies.AntManProxy;
import lucraft.mods.heroes.antman.util.AntManHelper;
import lucraft.mods.heroes.antman.util.IPymParticleContainer;
import lucraft.mods.lucraftcore.items.ItemBase;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

public class ItemPymTank extends ItemBase implements IPymParticleContainer {

	public int tier;
	
	public ItemPymTank(int tier) {
		super("tankTier" + tier);
		setCreativeTab(AntManProxy.tabAntMan);
		this.tier = tier;
		this.setMaxStackSize(1);
		
		AntMan.proxy.registerItemModel(this, 0, "tankTier" + tier);
		AntMan.proxy.registerItemModel(this, 1, "tankTier" + tier + "_shrink");
		AntMan.proxy.registerItemModel(this, 2, "tankTier" + tier + "_grow");
		
		AMItems.items.add(this);
	}

	@Override
	public int getMetadata(ItemStack stack) {
		if(getPymParticlesInTank(stack) == PymParticleVersion.SHRINK)
			return 1;
		if(getPymParticlesInTank(stack) == PymParticleVersion.GROW)
			return 2;
		
		return 0;
	}
	
	@Override
	public void onCreated(ItemStack stack, World worldIn, EntityPlayer playerIn) {
		AntManHelper.setDefaultPymTankNBTTags(stack);
	}
	
	@Override
	public double getDurabilityForDisplay(ItemStack stack) {
		return super.getDurabilityForDisplay(stack);
	}
	
	@Override
	public ModelResourceLocation getModel(ItemStack stack, EntityPlayer player, int i) {
		if(getPymParticles(getPymParticlesInTank(stack), stack) > 0)
			return new ModelResourceLocation(AntMan.ASSETDIR + "tankTier" + tier + "_" + getPymParticlesInTank(stack).toString().toLowerCase(), "inventory");
		
		return new ModelResourceLocation(AntMan.ASSETDIR + "tankTier" + tier, "inventory");
	}
	
	@Override
	public String getItemStackDisplayName(ItemStack stack) {
		return StatCollector.translateToLocal("item.pymTank.name");
	}
	
	@Override
	public void getSubItems(Item item, CreativeTabs tabs, List<ItemStack> list) {
		list.add(AntManHelper.setDefaultPymTankNBTTags(new ItemStack(item, 1)));
		list.add(AntManHelper.setDefaultPymTankNBTTags(new ItemStack(item, 1), PymParticleVersion.SHRINK, tier * 1000));
		list.add(AntManHelper.setDefaultPymTankNBTTags(new ItemStack(item, 1), PymParticleVersion.GROW, tier * 1000));
	}
	
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List<String> list, boolean b) {
		PymParticleVersion v = getPymParticlesInTank(stack);
		list.add(StatCollector.translateToLocal("antman.info.tier") + " " + tier);
		if(v != null) {
			list.add("");
			String desc = AntManHelper.getPymTankDesc(v);
			list.add(desc);
			list.add("   " + ChatFormatting.GRAY + getPymParticles(v, stack) + ChatFormatting.DARK_GRAY + "/" + ChatFormatting.GRAY + getMaxPymParticles(v, stack));
		} else {
			list.add(ChatFormatting.RED + "" + ChatFormatting.ITALIC + StatCollector.translateToLocal("antman.info.nopymparticles"));
		}
	}
	
	public PymParticleVersion getPymParticlesInTank(ItemStack stack) {
		AntManHelper.setDefaultPymTankNBTTags(stack, null, 0);
		NBTTagCompound compound = stack.getTagCompound();
		PymParticleVersion v = null;
		
		if(compound.getString("Particles").equalsIgnoreCase("shrink"))
			v = PymParticleVersion.SHRINK;
		else if(compound.getString("Particles").equalsIgnoreCase("grow"))
			v = PymParticleVersion.GROW;
		
		if(getPymParticles(v, stack) == 0)
			return null;
		
		return v;
	}
	
	public void setPymParticlesInTank(ItemStack stack, PymParticleVersion version) {
		NBTTagCompound compound = stack.getTagCompound();
		
		if(version == PymParticleVersion.SHRINK)
			compound.setString("Particles", "shrink");
		else if(version == PymParticleVersion.GROW)
			compound.setString("Particles", "grow");
		else
			compound.setString("Particles", "null");
		
		stack.setTagCompound(compound);
	}
	
	public void updateParticles(ItemStack stack) {
		if(getPymParticles(getPymParticlesInTank(stack), stack) == 0) {
			setPymParticlesInTank(stack, null);
		}
	}
	
	@Override
	public int getPymParticles(PymParticleVersion version, ItemStack stack) {
		AntManHelper.setDefaultPymTankNBTTags(stack);
		return stack.getTagCompound().getInteger(getNBTTagString(version, stack));
	}

	@Override
	public void setPymParticles(PymParticleVersion version, ItemStack stack, int i) {
		if(getPymParticlesInTank(stack) == version || getPymParticlesInTank(stack) == null) {
			AntManHelper.setDefaultPymTankNBTTags(stack);
			
			PymParticleVersion v = null;
			
			if(i > getMaxPymParticles(version, stack)) {
				i = getMaxPymParticles(version, stack);
				v = version;
			}
			if(i < 0) {
				i = 0;
				v = null;
			}
			
			setPymParticlesInTank(stack, v);
			
			NBTTagCompound compound = stack.getTagCompound();
			compound.setInteger(getNBTTagString(version, stack), i);
			stack.setTagCompound(compound);
			
			updateParticles(stack);
			
		}
		
		this.setDamage(stack, getDamage(stack));
	}

	@Override
	public int addPymParticles(PymParticleVersion version, ItemStack stack, int i, boolean simulate) {
		if((getPymParticlesInTank(stack) == version || getPymParticlesInTank(stack) == null) && i > 0) {
			AntManHelper.setDefaultPymTankNBTTags(stack);
			
			int particles = getPymParticles(version, stack) + i;
			
			if(!simulate) {
				setPymParticles(version, stack, particles);
				setPymParticlesInTank(stack, version);
			}
			
			return particles;
		}
		
		return 0;
	}

	@Override
	public int removePymParticles(PymParticleVersion version, ItemStack stack, int i, boolean simulate) {
		if((getPymParticlesInTank(stack) == version || getPymParticlesInTank(stack) == null) && i > 0) {
			AntManHelper.setDefaultPymTankNBTTags(stack);
			
			int particles = getPymParticles(version, stack) - i;
			
			if(!simulate) {
				setPymParticles(version, stack, particles);
				setPymParticlesInTank(stack, version);
			}
			
			return particles;
		}
		
		return 0;
	}

	@Override
	public boolean hasEnoughPymParticles(PymParticleVersion version, ItemStack stack, int i) {
		AntManHelper.setDefaultPymTankNBTTags(stack);
		return getPymParticles(version, stack) >= i;
	}

	@Override
	public boolean canAddPymParticles(PymParticleVersion version, ItemStack stack, int i) {
		if(getPymParticlesInTank(stack) == null || getPymParticlesInTank(stack) == version) {
			AntManHelper.setDefaultPymTankNBTTags(stack);
			int amount = getMaxPymParticles(version, stack) - getPymParticles(version, stack);
			return amount >= i && getPymParticles(version, stack) + amount <= getMaxPymParticles(version, stack);
		}
		
		return false;
	}

	@Override
	public boolean canRemovePymParticles(PymParticleVersion version, ItemStack stack, int i) {
		if(getPymParticlesInTank(stack) == null || getPymParticlesInTank(stack) == version) {
			AntManHelper.setDefaultPymTankNBTTags(stack);
			return getPymParticles(version, stack) - i >= 0;
		}
		
		return false;
	}
	
	@Override
	public int getMaxPymParticles(PymParticleVersion version, ItemStack stack) {
		return tier * 1000;
	}

	@Override
	public String getNBTTagString(PymParticleVersion version, ItemStack stack) {
		return version == PymParticleVersion.SHRINK ? AntManHelper.nbtShrinkParticles : AntManHelper.nbtGrowParticles;
	}
	
}
