package lucraft.mods.heroes.antman.items;

import lucraft.mods.heroes.antman.AntMan;
import lucraft.mods.heroes.antman.util.ShrinkerTypes;
import lucraft.mods.heroes.antman.util.ShrinkerTypesClientHelper;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemAntManHelmet extends ItemAntManArmor {

	public ItemAntManHelmet(ShrinkerTypes type) {
		super(type, 0);
	}
	
	private final String nbtHelmetOpen = "helmetOpen";
	
	@Override
	public void onCreated(ItemStack stack, World worldIn, EntityPlayer playerIn) {
		this.onCreated(stack);
	}
	
	public void onCreated(ItemStack stack) {
		if(stack.getTagCompound() == null) {
			NBTTagCompound compound = new NBTTagCompound();
			compound.setBoolean(nbtHelmetOpen, false);
			stack.setTagCompound(compound);
		}
	}
	
	public boolean isHelmetOpen(ItemStack stack) {
		this.onCreated(stack);
		return stack.getTagCompound().getBoolean(nbtHelmetOpen);
	}
	
	public ItemStack setHelmetOpen(ItemStack stack, boolean open) {
		this.onCreated(stack);
		NBTTagCompound compound = stack.getTagCompound();
		compound.setBoolean(nbtHelmetOpen, open);
		stack.setTagCompound(compound);
		return stack;
	}
	
	public static ItemStack toggleHelmet(ItemStack stack) {
		if(stack.getItem() instanceof ItemAntManHelmet) {
			ItemAntManHelmet item = (ItemAntManHelmet)stack.getItem();
			item.onCreated(stack);
			return item.setHelmetOpen(stack, !item.isHelmetOpen(stack));
		}
		
		return stack;
	}
	
	@Override
	public String getAntManArmorTexture(ItemStack stack, Entity entity, int slot, boolean light) {
		String texture = AntMan.ASSETDIR + "textures/models/armor/" + this.type.toString().toLowerCase() + "/helmet";
		
		if(isHelmetOpen(stack))
			texture = texture + "_open";
		
		if(light)
			texture = texture + "_lights";
		
		return texture + ".png";
	}
	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {
		return getAntManArmorTexture(stack, entity, slot, false);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public ModelBiped getAntManModelArmor(ItemStack stack, Entity entity, int slot) {
		return ShrinkerTypesClientHelper.getHelmetModel(getShrinkerType(), getAntManArmorTexture(stack, entity, slot, true));
	}
	
}
