package lucraft.mods.heroes.antman.items;

import lucraft.mods.heroes.antman.AntMan;
import lucraft.mods.heroes.antman.proxies.AntManProxy;
import lucraft.mods.lucraftcore.items.ItemBase;

public class ItemAMBase extends ItemBase {

	public ItemAMBase(String name) {
		super(name);
		AntMan.proxy.registerItemModel(this, name);
		setCreativeTab(AntManProxy.tabAntMan);
		AMItems.items.add(this);
	}
	
}
