package lucraft.mods.heroes.antman.items;

import java.util.List;

import lucraft.mods.heroes.antman.AntMan;
import lucraft.mods.heroes.antman.proxies.AntManProxy;
import lucraft.mods.heroes.antman.util.ShrinkerTypes;
import lucraft.mods.heroes.antman.util.ShrinkerTypesClientHelper;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;

public class ItemAntManArmor extends ItemArmor {

	public ShrinkerTypes type;
	public boolean creative;
	
	public ItemAntManArmor(ShrinkerTypes type, int armorSlot, boolean creative) {
		super(type.getArmorMaterial(), 0, armorSlot);
		String name = type.toString().toLowerCase() + getArmorSlotName(armorSlot) + (creative ? "_creative" : "");
		this.creative = creative;
		this.setUnlocalizedName(name);
		this.setCreativeTab(AntManProxy.tabAntMan);
		GameRegistry.registerItem(this, name);
		AntMan.proxy.registerItemModel(this, name);
		this.type = type;
		AMItems.items.add(this);
		
		if(armorSlot == 0)
			type.helmet = new ItemStack(this);
		else if(armorSlot == 1)
			type.chestplate = new ItemStack(this);
		else if(armorSlot == 2)
			type.legs = new ItemStack(this);
		else if(armorSlot == 3)
			type.boots = new ItemStack(this);
	}
	
	public ItemAntManArmor(ShrinkerTypes type, int armorSlot) {
		this(type, armorSlot, false);
	}

	public ShrinkerTypes getShrinkerType() {
		return type;
	}
	
	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List<String> list, boolean b) {
		list.add(type.getDescription());
	}
	
	@Override
	public boolean getIsRepairable(ItemStack toRepair, ItemStack repair) {
		
		if(type.getThingToRepair() instanceof String) {
			for(ItemStack s : OreDictionary.getOres((String) type.getThingToRepair())) {
				if(s.getItem() == repair.getItem() && s.getItemDamage() == repair.getItemDamage()) {
					return true;
				}
			}
		}
		
		else if(type.getThingToRepair() instanceof ItemStack) {
			ItemStack s = (ItemStack) type.getThingToRepair();
			if(s.getItem() == repair.getItem() && s.getItemDamage() == repair.getItemDamage()) {
				return true;
			}
		}
		
		return super.getIsRepairable(toRepair, repair);
	}
	
	public String getArmorSlotName(int i) {
		switch (i) {
		case 0:
			return "Helmet";
		case 1:
			return "Chestplate";
		case 2:
			return "Legs";
		default:
			return "Boots";
		}
	}

	public String getAntManArmorTexture(ItemStack stack, Entity entity, int slot, boolean light) {
		return AntMan.ASSETDIR + "textures/models/armor/" + this.type.toString().toLowerCase() + "/armor_" + (slot == 2 ? "2" : "1") + (light ? "_lights" : "") + ".png";
	}
	
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {
		return getAntManArmorTexture(stack, entity, slot, false);
	}

	@SideOnly(Side.CLIENT)
	public ModelBiped getAntManModelArmor(ItemStack stack, Entity entity, int slot) {
		return ShrinkerTypesClientHelper.getChestplateModel(getShrinkerType(), getAntManArmorTexture(stack, entity, slot, true));
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public ModelBiped getArmorModel(EntityLivingBase entityLiving, ItemStack itemStack, int armorSlot, ModelBiped _default) {
		ModelBiped armorModel = null;
		
		if (itemStack != null) {
			armorModel = getAntManModelArmor(itemStack, entityLiving, armorSlot);
			
			if (armorModel != null) {

				armorModel.setModelAttributes(_default);
				
				return armorModel;
			}
		}
		
		return super.getArmorModel(entityLiving, itemStack, armorSlot, _default);
	}

}
