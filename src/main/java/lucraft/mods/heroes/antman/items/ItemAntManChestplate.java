package lucraft.mods.heroes.antman.items;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import com.mojang.realmsclient.gui.ChatFormatting;

import lucraft.mods.heroes.antman.entity.AntManEntityData;
import lucraft.mods.heroes.antman.network.SyncTracker;
import lucraft.mods.heroes.antman.util.AMSounds;
import lucraft.mods.heroes.antman.util.AntManHelper;
import lucraft.mods.heroes.antman.util.IPymParticleContainer;
import lucraft.mods.heroes.antman.util.ShrinkerArmorAbilities;
import lucraft.mods.heroes.antman.util.ShrinkerTypes;
import lucraft.mods.lucraftcore.client.LucraftCoreKeyBindings.LucraftKeyTypes;
import lucraft.mods.lucraftcore.client.render.IRenderFirstPersonHand;
import lucraft.mods.lucraftcore.util.IShiftableItemToolTip;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.StatCollector;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class ItemAntManChestplate extends ItemAntManArmor implements IPymParticleContainer, IShiftableItemToolTip, IRenderFirstPersonHand {

	public ItemAntManChestplate(ShrinkerTypes type, boolean creative) {
		super(type, 1, creative);
	}

	@Override
	public void onArmorTick(World world, EntityPlayer player, ItemStack stack) {
		if (AntManHelper.hasArmorOn(player) && this.getShrinkerType().canFly())
			useJetpack(player, stack, false);
	}

	private void useJetpack(EntityLivingBase user, ItemStack armor, boolean force) {
		boolean flyKeyDown = force || SyncTracker.isFlyKeyDown(user);

		if (flyKeyDown) {
			float speedSideways = (float) (user.isSneaking() ? 0.08F * 0.5F : 0.28F);
			double currentAccel = user.motionY < 0.3D ? 0.12F * 2.5 : 0.12F;

			user.motionY = Math.min(user.motionY + currentAccel, 0.3F);

			if (SyncTracker.isForwardKeyDown(user)) {
				user.moveFlying(0, speedSideways, speedSideways);
			}
			if (SyncTracker.isBackwardKeyDown(user)) {
				user.moveFlying(0, -speedSideways, speedSideways * 0.8F);
			}
			if (SyncTracker.isLeftKeyDown(user)) {
				user.moveFlying(speedSideways, 0, speedSideways);
			}
			if (SyncTracker.isRightKeyDown(user)) {
				user.moveFlying(-speedSideways, 0, speedSideways);
			}

			if (!user.worldObj.isRemote) {
				user.fallDistance = 0.0F;
			}

			AntManEntityData.get(user).sendFlyUpdatePackets(true);

			String sound = getShrinkerType() == ShrinkerTypes.MCU_YELLOWJACKET ? AMSounds.jetpack : AMSounds.wings;
			EnumParticleTypes particle = getShrinkerType() == ShrinkerTypes.MCU_YELLOWJACKET ? EnumParticleTypes.SMOKE_NORMAL : null;

			if (getShrinkerType() == ShrinkerTypes.MCU_YELLOWJACKET)
				user.worldObj.playSoundAtEntity(user, sound, 0.05F, 1F);
			else
				user.worldObj.playSoundAtEntity(user, sound, 0.3F, 1F);

			if (particle != null) {
				Random rand = new Random();

				{
					Vec3 v = user.getPositionVector().addVector(0, user.height / 2, 0);
					Vec3 v2 = new Vec3(user.width / 3F, 0F, -user.width / 3F);
					v2 = v2.rotateYaw(-user.renderYawOffset * (float) Math.PI / 180F);
					v = v.add(v2);
					user.worldObj.spawnParticle(particle, v.xCoord, v.yCoord - 0.2F, v.zCoord, rand.nextDouble() * 0.05D - 0.025D, -0.2D, rand.nextDouble() * 0.05D - 0.025D);
				}

				{
					Vec3 v = user.getPositionVector().addVector(0, user.height / 2, 0);
					Vec3 v2 = new Vec3(-user.width / 3F, 0F, -user.width / 3F);
					v2 = v2.rotateYaw(-user.renderYawOffset * (float) Math.PI / 180F);
					v = v.add(v2);
					user.worldObj.spawnParticle(particle, v.xCoord, v.yCoord - 0.2F, v.zCoord, rand.nextDouble() * 0.05D - 0.025D, -0.2D, rand.nextDouble() * 0.05D - 0.025D);
				}
			}
		} else {
			AntManEntityData.get(user).sendFlyUpdatePackets(false);
		}
	}

	@Override
	public String getItemStackDisplayName(ItemStack stack) {
		if (creative)
			return StatCollector.translateToLocal("antman.info.creative") + " " + StatCollector.translateToLocal(super.getItemStackDisplayName(stack).replace("_creative", ""));

		return super.getItemStackDisplayName(stack);
	}

	@Override
	public void onCreated(ItemStack stack, World worldIn, EntityPlayer playerIn) {
		super.onCreated(stack, worldIn, playerIn);
		setDefaultTags(stack);
	}

	private void setDefaultTags(ItemStack stack) {
		AntManHelper.setDefaultPymTankNBTTags(stack);

		if (!stack.getTagCompound().hasKey(AntManHelper.nbtSizeChestValue) && hasAbility(ShrinkerArmorAbilities.sizeRegulator)) {
			NBTTagCompound c = stack.getTagCompound();
			c.setDouble(AntManHelper.nbtSizeChestValue, 3D);
			stack.setTagCompound(c);
		}
	}

	public boolean hasAbility(ShrinkerArmorAbilities ab) {
		return getShrinkerType().getAbilites().contains(ab);
	}

	public double getEstimatedSizeToChange(ItemStack stack) {
		setDefaultTags(stack);

		if (hasAbility(ShrinkerArmorAbilities.sizeRegulator)) {
			return stack.getTagCompound().getDouble(AntManHelper.nbtSizeChestValue);
		}

		return hasAbility(ShrinkerArmorAbilities.grow) ? 3D : 0.11D;
	}

	@Override
	public void getSubItems(Item item, CreativeTabs tabs, List<ItemStack> list) {
		if (creative) {
			list.add(new ItemStack(item));
			return;
		}

		ItemStack stack = new ItemStack(item, 1);
		list.add(getSubItemStack(item, 0));
		list.add(getSubItemStack(item, getMaxPymParticles(PymParticleVersion.SHRINK, stack)));
	}

	public ItemStack getSubItemStack(Item item, int amount) {
		ItemAntManChestplate item2 = (ItemAntManChestplate) item;
		ItemStack stack = new ItemStack(item2);
		item2.setPymParticles(PymParticleVersion.SHRINK, stack, amount);
		item2.setPymParticles(PymParticleVersion.GROW, stack, amount);
		return stack;
	}

	@Override
	public void addInformation(ItemStack stack, EntityPlayer player, List<String> list, boolean b) {
		super.addInformation(stack, player, list, b);
		list.add("");
	}

	@Override
	public boolean shouldRenderShiftableToolTip(ItemStack stack, EntityPlayer player) {
		return true;
	}

	@Override
	public boolean shouldRenderArmorActionsToolTip(ItemStack stack, EntityPlayer player) {
		return true;
	}

	@Override
	public List<String> getShiftableToolTip(ItemStack stack, EntityPlayer player) {
		List<String> list = new ArrayList<String>();
		String shrinkParticles = "   " + (creative ? ChatFormatting.GRAY + StatCollector.translateToLocal("antman.info.infinite") : "" + ChatFormatting.GRAY + getPymParticles(PymParticleVersion.SHRINK, stack) + ChatFormatting.DARK_GRAY + "/" + ChatFormatting.GRAY + getMaxPymParticles(PymParticleVersion.SHRINK, stack));
		String growParticles = "   " + (creative ? ChatFormatting.GRAY + StatCollector.translateToLocal("antman.info.infinite") : "" + ChatFormatting.GRAY + getPymParticles(PymParticleVersion.GROW, stack) + ChatFormatting.DARK_GRAY + "/" + ChatFormatting.GRAY + getMaxPymParticles(PymParticleVersion.GROW, stack));

		list.add(AntManHelper.getPymTankDesc(PymParticleVersion.SHRINK));
		list.add(shrinkParticles);
		list.add("");
		list.add(AntManHelper.getPymTankDesc(PymParticleVersion.GROW));
		list.add(growParticles);

		if (type.getAbilites().size() > 0) {
			list.add("");
			list.add(ChatFormatting.GREEN + StatCollector.translateToLocal("antman.info.abilites") + ChatFormatting.DARK_GREEN + ":");
			for (ShrinkerArmorAbilities ab : type.getAbilites())
				list.add("   " + ChatFormatting.DARK_GRAY + "- " + EnumChatFormatting.GRAY + StatCollector.translateToLocal(ab.getDisplayName()));
		}

		if (hasAbility(ShrinkerArmorAbilities.sizeRegulator)) {
			list.add("");
			list.add(ChatFormatting.YELLOW + ShrinkerArmorAbilities.sizeRegulator.getDisplayName() + ChatFormatting.GOLD + ":");
			list.add("   " + ChatFormatting.GRAY + AntManHelper.getSavedScaleFromChestplate(stack));
		}
		return list;
	}

	@Override
	public String getArmorActionsToolTipForType(ItemStack stack, EntityPlayer player, LucraftKeyTypes type) {
		switch (type) {
		case ARMOR_1:
			return "antman.keybinding.keySize";
		case ARMOR_2:
			return "antman.keybinding.sizeGUI";
		case ARMOR_3:
			return "antman.keybinding.laser";
		case ARMOR_4:
			return "antman.keybinding.togglehud";
		default:
			return "/";
		}
	}

	@Override
	public int getPymParticles(PymParticleVersion version, ItemStack stack) {
		if (creative)
			return getMaxPymParticles(version, stack);

		AntManHelper.setDefaultPymTankNBTTags(stack);
		return stack.getTagCompound().getInteger(getNBTTagString(version, stack));
	}

	@Override
	public void setPymParticles(PymParticleVersion version, ItemStack stack, int i) {
		if (creative)
			return;

		AntManHelper.setDefaultPymTankNBTTags(stack);

		if (i > getMaxPymParticles(version, stack)) {
			i = getMaxPymParticles(version, stack);
		}
		if (i < 0) {
			i = 0;
		}

		NBTTagCompound compound = stack.getTagCompound();
		compound.setInteger(getNBTTagString(version, stack), i);
		stack.setTagCompound(compound);
	}

	@Override
	public int addPymParticles(PymParticleVersion version, ItemStack stack, int i, boolean simulate) {
		if (creative)
			return 0;

		if (i > 0) {
			AntManHelper.setDefaultPymTankNBTTags(stack);

			int particles = getPymParticles(version, stack) + i;

			if (!simulate) {
				setPymParticles(version, stack, particles);
			}

			return particles;
		}

		return 0;
	}

	@Override
	public int removePymParticles(PymParticleVersion version, ItemStack stack, int i, boolean simulate) {
		if (creative)
			return 0;

		if (i > 0) {
			AntManHelper.setDefaultPymTankNBTTags(stack);

			int particles = getPymParticles(version, stack) - i;

			if (!simulate) {
				setPymParticles(version, stack, particles);
			}

			return particles;
		}

		return 0;
	}

	@Override
	public boolean hasEnoughPymParticles(PymParticleVersion version, ItemStack stack, int i) {
		if (creative)
			return true;

		AntManHelper.setDefaultPymTankNBTTags(stack);
		return getPymParticles(version, stack) >= i;
	}

	@Override
	public boolean canAddPymParticles(PymParticleVersion version, ItemStack stack, int i) {
		if (creative)
			return false;

		AntManHelper.setDefaultPymTankNBTTags(stack);
		int amount = getMaxPymParticles(version, stack) - getPymParticles(version, stack);
		return amount >= i && getPymParticles(version, stack) + amount <= getMaxPymParticles(version, stack);

	}

	@Override
	public boolean canRemovePymParticles(PymParticleVersion version, ItemStack stack, int i) {
		if (creative)
			return true;

		AntManHelper.setDefaultPymTankNBTTags(stack);
		return getPymParticles(version, stack) - i >= 0;
	}

	@Override
	public int getMaxPymParticles(PymParticleVersion version, ItemStack stack) {
		return 1000;
	}

	@Override
	public String getNBTTagString(PymParticleVersion version, ItemStack stack) {
		return version == PymParticleVersion.SHRINK ? AntManHelper.nbtShrinkParticles : AntManHelper.nbtGrowParticles;
	}

	@Override
	public HashMap<ModelBiped, Object[]> getFirstPersonModels(ItemStack stack, EntityPlayer player, int armorSlot, boolean smallArms) {
		HashMap<ModelBiped, Object[]> map = new HashMap<ModelBiped, Object[]>();
		
		map.put(getAntManModelArmor(stack, player, armorSlot), new Object[] {getAntManArmorTexture(stack, player, armorSlot, false), false});
		map.put(getAntManModelArmor(stack, player, armorSlot), new Object[] {getAntManArmorTexture(stack, player, armorSlot, true), true});
		
		return map;
	}
	
}
