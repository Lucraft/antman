package lucraft.mods.heroes.antman.items;

import java.util.ArrayList;

import lucraft.mods.heroes.antman.util.ShrinkerTypes;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraftforge.common.util.EnumHelper;

public class AMItems {
	
	public static final ArmorMaterial antMan = EnumHelper.addArmorMaterial("antMan", "antMan", 25, new int[]{3, 7, 6, 3}, 3);
	public static final ArmorMaterial antManCW = EnumHelper.addArmorMaterial("antManCW", "antManCW", 39, new int[]{5, 10, 8, 5}, 5);
	public static final ArmorMaterial yellowjacket = EnumHelper.addArmorMaterial("yellowjacket", "yellowjacket", 43, new int[]{7, 12, 10, 7}, 6);
	
	public static ArrayList<Item> items = new ArrayList<Item>();
	
	public static Item regulator;
	public static Item yjRegulator;
	public static Item filter;
	public static Item empCummunicationDevice;
	public static Item antAntenna;
	public static Item yjLaserArm;
	public static Item waspWing;
	public static Item tankTier1;
	public static Item tankTier2;
	public static Item tankTier3;
	
	public static Item discShrink;
	public static Item discGrow;
	
	public static Item antManHelmet;
	public static Item antManChestplate;
	public static Item antManChestplate_creative;
	public static Item antManLegs;
	public static Item antManBoots;
	
	public static Item antManCivilWarHelmet;
	public static Item antManCivilWarChestplate;
	public static Item antManCivilWarChestplate_creative;
	public static Item antManCivilWarLegs;
	public static Item antManCivilWarBoots;
	
	public static Item yellowjacketHelmet;
	public static Item yellowjacketChestplate;
	public static Item yellowjacketChestplate_creative;
	public static Item yellowjacketLegs;
	public static Item yellowjacketBoots;
	
	public static Item waspHelmet;
	public static Item waspChestplate;
	public static Item waspChestplate_creative;
	public static Item waspLegs;
	public static Item waspBoots;
	
	public static Item comicAntManHelmet;
	public static Item comicAntManChestplate;
	public static Item comicAntManChestplate_creative;
	public static Item comicAntManLegs;
	public static Item comicAntManBoots;
	
	public static Item comicWaspHelmet;
	public static Item comicWaspChestplate;
	public static Item comicWaspChestplate_creative;
	public static Item comicWaspLegs;
	public static Item comicWaspBoots;
	
	public static void init() {
		
		antManHelmet = new ItemAntManHelmet(ShrinkerTypes.MCU_ANTMAN);
		antManChestplate = new ItemAntManChestplate(ShrinkerTypes.MCU_ANTMAN, false);
		antManChestplate_creative = new ItemAntManChestplate(ShrinkerTypes.MCU_ANTMAN, true);
		antManLegs = new ItemAntManArmor(ShrinkerTypes.MCU_ANTMAN, 2);
		antManBoots = new ItemAntManArmor(ShrinkerTypes.MCU_ANTMAN, 3);
		
		antManCivilWarHelmet = new ItemAntManHelmet(ShrinkerTypes.MCU_ANTMAN_2);
		antManCivilWarChestplate = new ItemAntManChestplate(ShrinkerTypes.MCU_ANTMAN_2, false);
		antManCivilWarChestplate_creative = new ItemAntManChestplate(ShrinkerTypes.MCU_ANTMAN_2, true);
		antManCivilWarLegs = new ItemAntManArmor(ShrinkerTypes.MCU_ANTMAN_2, 2);
		antManCivilWarBoots = new ItemAntManArmor(ShrinkerTypes.MCU_ANTMAN_2, 3);
		
		yellowjacketHelmet = new ItemAntManHelmet(ShrinkerTypes.MCU_YELLOWJACKET);
		yellowjacketChestplate = new ItemAntManChestplate(ShrinkerTypes.MCU_YELLOWJACKET, false);
		yellowjacketChestplate_creative = new ItemAntManChestplate(ShrinkerTypes.MCU_YELLOWJACKET, true);
		yellowjacketLegs = new ItemAntManArmor(ShrinkerTypes.MCU_YELLOWJACKET, 2);
		yellowjacketBoots = new ItemAntManArmor(ShrinkerTypes.MCU_YELLOWJACKET, 3);
		
		waspHelmet = new ItemAntManHelmet(ShrinkerTypes.MCU_WASP);
		waspChestplate = new ItemAntManChestplate(ShrinkerTypes.MCU_WASP, false);
		waspChestplate_creative = new ItemAntManChestplate(ShrinkerTypes.MCU_WASP, true);
		waspLegs = new ItemAntManArmor(ShrinkerTypes.MCU_WASP, 2);
		waspBoots = new ItemAntManArmor(ShrinkerTypes.MCU_WASP, 3);
		
		comicAntManHelmet = new ItemAntManHelmet(ShrinkerTypes.COMIC_ANTMAN);
		comicAntManChestplate = new ItemAntManChestplate(ShrinkerTypes.COMIC_ANTMAN, false);
		comicAntManChestplate_creative = new ItemAntManChestplate(ShrinkerTypes.COMIC_ANTMAN, true);
		comicAntManLegs = new ItemAntManArmor(ShrinkerTypes.COMIC_ANTMAN, 2);
		comicAntManBoots = new ItemAntManArmor(ShrinkerTypes.COMIC_ANTMAN, 3);
		
		comicWaspHelmet = new ItemAntManHelmet(ShrinkerTypes.COMIC_WASP);
		comicWaspChestplate = new ItemAntManChestplate(ShrinkerTypes.COMIC_WASP, false);
		comicWaspChestplate_creative = new ItemAntManChestplate(ShrinkerTypes.COMIC_WASP, true);
		comicWaspLegs = new ItemAntManArmor(ShrinkerTypes.COMIC_WASP, 2);
		comicWaspBoots = new ItemAntManArmor(ShrinkerTypes.COMIC_WASP, 3);
		
		regulator = new ItemAMBase("regulator");
		yjRegulator = new ItemAMBase("yjRegulator");
		filter = new ItemAMBase("filter");
		empCummunicationDevice = new ItemAMBase("empCummunicationDevice");
		antAntenna = new ItemAMBase("antAntenna");
		yjLaserArm = new ItemAMBase("yjLaserArm");
		waspWing = new ItemAMBase("waspWing");
		tankTier1 = new ItemPymTank(1);
		tankTier2 = new ItemPymTank(2);
		tankTier3 = new ItemPymTank(3);
		
		discShrink = new ItemShrinkDisc("discShrink", true);
		discGrow = new ItemShrinkDisc("discGrow", false);
	}

}