package lucraft.mods.heroes.antman.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.heroes.antman.entity.AntManEntityData;
import lucraft.mods.heroes.antman.util.AntManHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSizeChange implements IMessage {

	private int entityId;
	private NBTTagCompound data;

	public MessageSizeChange() {
		
	}
	
	public MessageSizeChange(EntityLivingBase entity) {
		entityId = entity.getEntityId();
		data = new NBTTagCompound();
		AntManEntityData.get(entity).saveNBTData(data);
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		entityId = buf.readInt();
		data = ByteBufUtils.readTag(buf);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(entityId);
		ByteBufUtils.writeTag(buf, data);
	}

	public static class Handler extends AbstractClientMessageHandler<MessageSizeChange> {
		
		@Override
		public IMessage handleClientMessage(EntityPlayer player, MessageSizeChange message, MessageContext ctx) {
			if(player != null && message != null && ctx != null) {
				EntityLivingBase en = (EntityLivingBase) player.worldObj.getEntityByID(message.entityId);
				if(en != null) {
					AntManEntityData.get(en).loadNBTData(message.data);
					AntManHelper.setSize(en, AntManEntityData.get(en).standardHeight, AntManEntityData.get(en).standardWidth, AntManEntityData.get(en).size);
				}
			}

			return null;
		}
	}

}
