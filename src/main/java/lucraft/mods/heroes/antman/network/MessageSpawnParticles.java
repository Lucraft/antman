package lucraft.mods.heroes.antman.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumParticleTypes;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSpawnParticles implements IMessage {

	public MessageSpawnParticles() {
		
	}
	
	public EnumParticleTypes particle;
	public double xCoord;
	public double yCoord;
	public double zCoord;
	public double xOffset;
	public double yOffset;
	public double zOffset;
	
	public MessageSpawnParticles(EnumParticleTypes particle, double xCoord, double yCoord, double zCoord, double xOffset, double yOffset, double zOffset) {
		this.particle = particle;
		this.xCoord = xCoord;
		this.yCoord = yCoord;
		this.xCoord = xCoord;
		this.zOffset = zOffset;
		this.yOffset = yOffset;
		this.zOffset = zOffset;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		particle = EnumParticleTypes.getParticleFromId(buf.readInt());
		xCoord = buf.readDouble();
		yCoord = buf.readDouble();
		zCoord = buf.readDouble();
		xOffset = buf.readDouble();
		yOffset = buf.readDouble();
		zOffset = buf.readDouble();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(particle.getParticleID());
		buf.writeDouble(xCoord);
		buf.writeDouble(yCoord);
		buf.writeDouble(zCoord);
		buf.writeDouble(xOffset);
		buf.writeDouble(yOffset);
		buf.writeDouble(zOffset);
	}

	public static class Handler extends AbstractClientMessageHandler<MessageSpawnParticles> {
		
		@Override
		public IMessage handleClientMessage(EntityPlayer player, MessageSpawnParticles message, MessageContext ctx) {
			Minecraft.getMinecraft().thePlayer.worldObj.spawnParticle(message.particle, message.xCoord, message.yCoord, message.zCoord, message.xOffset, message.yOffset, message.zOffset);
			return null;
		}
	}
}
