package lucraft.mods.heroes.antman.network;

import java.util.HashMap;

import io.netty.buffer.ByteBuf;
import lucraft.mods.heroes.antman.AntMan;
import lucraft.mods.heroes.antman.client.gui.GuiIDs;
import lucraft.mods.heroes.antman.items.ItemAntManHelmet;
import lucraft.mods.heroes.antman.util.AntManHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSendInfoToServer implements IMessage {

	public InfoType type;

	public MessageSendInfoToServer() {}
	
	public MessageSendInfoToServer(InfoType type) {
		this.type = type;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		type = MessageSendInfoToServer.InfoType.getInfoTypeFromId(buf.readInt());
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(type.ordinal());
	}
	
	public static class Handler extends AbstractServerMessageHandler<MessageSendInfoToServer> {
		
		@Override
		public IMessage handleServerMessage(EntityPlayer player, MessageSendInfoToServer message, MessageContext ctx) {
			InfoType type = message.type;
			switch (type) {
			
			case HELMET:
				if(AntManHelper.hasHelmetOn(player) && AntManHelper.isNormalSize(player)) {
					ItemAntManHelmet.toggleHelmet(player.getCurrentArmor(3));
					player.worldObj.playSoundAtEntity(player, AntManHelper.getShrinkerTypeFromHelmet(player).getHelmetSound(), 0.5F, 0.5F);
				}
				break;
				
			case REGULATOR_GUI:
				if(AntManHelper.hasArmorOn(player)) {
					player.openGui(AntMan.instance, GuiIDs.sizeRegulatorGuiId, player.worldObj, (int)player.posX, (int)player.posY, (int)player.posZ);
				}
				break;
				
			default:
				break;
			}
			
			
			return null;
		}
	}

	public static HashMap<Integer, InfoType> ids = new HashMap<Integer, MessageSendInfoToServer.InfoType>();
	
	public enum InfoType {
		
		HELMET,
		REGULATOR_GUI;
		
		private InfoType() {
			MessageSendInfoToServer.ids.put(this.ordinal(), this);
		}
		
		public static InfoType getInfoTypeFromId(int id) {
			return MessageSendInfoToServer.ids.get(id);
		}
		
	}
	
}
