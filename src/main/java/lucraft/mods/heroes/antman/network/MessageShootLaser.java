package lucraft.mods.heroes.antman.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.heroes.antman.util.AntManHelper;
import lucraft.mods.heroes.antman.util.ShrinkerTypes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageShootLaser implements IMessage {

	@Override
	public void fromBytes(ByteBuf buf) {
		
	}

	@Override
	public void toBytes(ByteBuf buf) {
		
	}

	public static class Handler extends AbstractServerMessageHandler<MessageShootLaser> {
		
		public static boolean left = true;
		
		@Override
		public IMessage handleServerMessage(EntityPlayer player, MessageShootLaser message, MessageContext ctx) {
			
			if(AntManHelper.hasArmorOn(player, ShrinkerTypes.MCU_YELLOWJACKET)) {
				AntManHelper.shootYellowjacketLaser(player, left);
				left = !left;
			}
			
			return null;
		}
		
	}
	
}
