package lucraft.mods.heroes.antman.network;

import lucraft.mods.heroes.antman.AntMan;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class PacketDispatcher {

	private static byte packetId = 0;

	/**
	 * The SimpleNetworkWrapper instance is used both to register and send
	 * packets. Since I will be adding wrapper methods, this field is private,
	 * but you should make it public if you plan on using it directly.
	 */
	private static final SimpleNetworkWrapper dispatcher = NetworkRegistry.INSTANCE.newSimpleChannel(AntMan.MODID);

	/**
	 * Call this during pre-init or loading and register all of your packets
	 * (messages) here
	 */
	public static final void registerPackets() {

		PacketDispatcher.registerMessage(MessageSizeChange.Handler.class, MessageSizeChange.class, Side.CLIENT);
		PacketDispatcher.registerMessage(MessageSpawnSizeChangeCopy.Handler.class, MessageSpawnSizeChangeCopy.class, Side.CLIENT);
		PacketDispatcher.registerMessage(MessageSizeKey.Handler.class, MessageSizeKey.class, Side.SERVER);
		PacketDispatcher.registerMessage(MessageFlyKeys.Handler.class, MessageFlyKeys.class, Side.SERVER);
		PacketDispatcher.registerMessage(MessageSetChestplateSize.Handler.class, MessageSetChestplateSize.class, Side.SERVER);
		PacketDispatcher.registerMessage(MessageSendInfoToServer.Handler.class, MessageSendInfoToServer.class, Side.SERVER);
		PacketDispatcher.registerMessage(MessageShootLaser.Handler.class, MessageShootLaser.class, Side.SERVER);
		PacketDispatcher.registerMessage(MessageSpawnParticles.Handler.class, MessageSpawnParticles.class, Side.CLIENT);
		PacketDispatcher.registerMessage(MessageSendFlyingUpdate.Handler.class, MessageSendFlyingUpdate.class, Side.CLIENT);
	}

	/**
	 * Registers a message and message handler
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static final void registerMessage(Class handlerClass, Class messageClass, Side side) {
		PacketDispatcher.dispatcher.registerMessage(handlerClass, messageClass, packetId++, side);
	}

	// ========================================================//
	// The following methods are the 'wrapper' methods; again,
	// this just makes sending a message slightly more compact
	// and is purely a matter of stylistic preference
	// ========================================================//

	/**
	 * Send this message to the specified player. See
	 * {@link SimpleNetworkWrapper#sendTo(IMessage, EntityPlayerMP)}
	 */
	public static final void sendTo(IMessage message, EntityPlayerMP player) {
		PacketDispatcher.dispatcher.sendTo(message, player);
	}

	/**
	 * Send this message to everyone within a certain range of a point. See
	 * {@link SimpleNetworkWrapper#sendToDimension(IMessage, NetworkRegistry.TargetPoint)}
	 */
	public static final void sendToAllAround(IMessage message, NetworkRegistry.TargetPoint point) {
		PacketDispatcher.dispatcher.sendToAllAround(message, point);
	}

	public static final void sendToAll(IMessage message) {
		PacketDispatcher.dispatcher.sendToAll(message);
	}

	/**
	 * Sends a message to everyone within a certain range of the coordinates in
	 * the same dimension.
	 */
	public static final void sendToAllAround(IMessage message, int dimension, double x, double y, double z,

	double range) {
		PacketDispatcher.sendToAllAround(message, new NetworkRegistry.TargetPoint(dimension, x, y, z,

		range));
	}

	/**
	 * Sends a message to everyone within a certain range of the player
	 * provided.
	 */
	public static final void sendToAllAround(IMessage message, EntityPlayer player, double range) {
		PacketDispatcher.sendToAllAround(message, player.worldObj.provider.getDimensionId(), player.posX, player.posY, player.posZ, range);
	}

	/**
	 * Send this message to everyone within the supplied dimension. See
	 * {@link SimpleNetworkWrapper#sendToDimension(IMessage, int)}
	 */
	public static final void sendToDimension(IMessage message, int dimensionId) {
		PacketDispatcher.dispatcher.sendToDimension(message, dimensionId);
	}

	/**
	 * Send this message to the server. See
	 * {@link SimpleNetworkWrapper#sendToServer(IMessage)}
	 */
	public static final void sendToServer(IMessage message) {
		PacketDispatcher.dispatcher.sendToServer(message);
	}
}
