package lucraft.mods.heroes.antman.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.heroes.antman.client.AMClientEvents;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSpawnSizeChangeCopy implements IMessage {

	public int entityId;
	
	public MessageSpawnSizeChangeCopy() {
		
	}
	
	public MessageSpawnSizeChangeCopy(EntityLivingBase entity) {
		entityId = entity.getEntityId();
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		entityId = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(entityId);
	}

	public static class Handler extends AbstractClientMessageHandler<MessageSpawnSizeChangeCopy> {
		@Override
		public IMessage handleClientMessage(EntityPlayer player, MessageSpawnSizeChangeCopy message, MessageContext ctx) {
			if(player != null && message != null && ctx != null) {
				EntityLivingBase en = (EntityLivingBase) player.worldObj.getEntityByID(message.entityId);
				if(en != null) {
					AMClientEvents.entity = en;
				}
			}

			return null;
		}
	}
	
}
