package lucraft.mods.heroes.antman.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.heroes.antman.AMConfig;
import lucraft.mods.heroes.antman.dimensions.AMDimensions;
import lucraft.mods.heroes.antman.entity.AntManEntityData;
import lucraft.mods.heroes.antman.items.AMItems;
import lucraft.mods.heroes.antman.items.ItemAntManChestplate;
import lucraft.mods.heroes.antman.items.ItemAntManHelmet;
import lucraft.mods.heroes.antman.util.AMSizes;
import lucraft.mods.heroes.antman.util.AntManHelper;
import lucraft.mods.heroes.antman.util.IPymParticleContainer.PymParticleVersion;
import lucraft.mods.heroes.antman.util.ShrinkerArmorAbilities;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSizeKey implements IMessage {


	public MessageSizeKey() {
		
	}

	@Override
	public void fromBytes(ByteBuf buf) {
	}

	@Override
	public void toBytes(ByteBuf buf) {
	}

	public static class Handler extends AbstractServerMessageHandler<MessageSizeKey> {
		
		@Override
		public IMessage handleServerMessage(EntityPlayer player, MessageSizeKey message, MessageContext ctx) {
			
			if(AntManHelper.hasArmorOn(player)) {
				ItemStack stack = player.getCurrentArmor(3);
				ItemAntManHelmet helmet = (ItemAntManHelmet) stack.getItem();
				
				if(helmet.isHelmetOpen(stack))
					ItemAntManHelmet.toggleHelmet(stack);
					
				ItemStack chestStack = player.getCurrentArmor(2);
				ItemAntManChestplate chestplate = (ItemAntManChestplate) chestStack.getItem();
				ItemStack inInv = AntManHelper.getItemInAntManArmorChestplate(chestStack);
				
				if(inInv == null) {
					if(player.dimension != AMDimensions.quantumRealmDimensionId && chestplate.canRemovePymParticles(PymParticleVersion.SHRINK, chestStack, AMConfig.pymParticlesForSizeChange) && AntManEntityData.get(player).changeSize(AMSizes.small, true)) {
							AntManEntityData.get(player).quantumRealmTeleportState = 1;
							chestplate.removePymParticles(PymParticleVersion.SHRINK, chestStack, AMConfig.pymParticlesForSizeChange, false);
							AntManEntityData.get(player).changeSize((float) 0.01F, false);
					}
				} 
				
				else if(inInv.getItem() == AntManHelper.getRegulatorFromArmor(chestStack) && player.dimension != AMDimensions.quantumRealmDimensionId) {
					if(AntManHelper.isNormalSize(player)) {
						if(chestplate.getEstimatedSizeToChange(chestStack) != 1D) {
							boolean shrink = chestplate.getEstimatedSizeToChange(chestStack) < 1D;
							PymParticleVersion pyms = shrink ? PymParticleVersion.SHRINK : PymParticleVersion.GROW;
							
							if(chestplate.canRemovePymParticles(pyms, chestStack, AMConfig.pymParticlesForSizeChange) && chestplate.getEstimatedSizeToChange(chestStack) != 1D) {
								AntManEntityData.get(player).changeSize((float) chestplate.getEstimatedSizeToChange(chestStack), false);
								chestplate.removePymParticles(pyms, chestStack, AMConfig.pymParticlesForSizeChange, false);
							}
						}
					} else {
						boolean shrunken = AntManEntityData.get(player).size < 1D;
						PymParticleVersion pyms = shrunken ? PymParticleVersion.GROW : PymParticleVersion.SHRINK;
						
						if(chestplate.canRemovePymParticles(pyms, chestStack, AMConfig.pymParticlesForSizeChange) && AntManEntityData.get(player).changeSize(AMSizes.normal, true)) {
							AntManEntityData.get(player).changeSize(AMSizes.normal, false);
							chestplate.removePymParticles(pyms, chestStack, AMConfig.pymParticlesForSizeChange, false);
						}
					}
				}
				
				else if(inInv.getItem() == AMItems.discShrink && player.dimension != AMDimensions.quantumRealmDimensionId) {
					if(chestplate.canRemovePymParticles(PymParticleVersion.SHRINK, chestStack, AMConfig.pymParticlesForSizeChange) && AntManEntityData.get(player).makeOneStepSmaller(true)) {
						AntManEntityData.get(player).makeOneStepSmaller();
						chestplate.removePymParticles(PymParticleVersion.SHRINK, chestStack, AMConfig.pymParticlesForSizeChange, false);
					}
				}
				
				else if(inInv.getItem() == AMItems.discGrow) {
					if(player.dimension == AMDimensions.quantumRealmDimensionId) {
						if(chestplate.canRemovePymParticles(PymParticleVersion.GROW, chestStack, AMConfig.pymParticlesForSizeChange) && AntManEntityData.get(player).changeSize(AMSizes.big, true)) {
							AntManEntityData.get(player).quantumRealmTeleportState = 2;
							AntManEntityData.get(player).setSize(0.01F);
							chestplate.removePymParticles(PymParticleVersion.GROW, chestStack, AMConfig.pymParticlesForSizeChange, false);
							AntManEntityData.get(player).changeSize((float) AMSizes.big, false);
						}
					}
					else if(chestplate.canRemovePymParticles(PymParticleVersion.GROW, chestStack, AMConfig.pymParticlesForSizeChange) && AntManEntityData.get(player).makeOneStepBigger(true)) {
						if(AntManEntityData.get(player).size < AMSizes.normal || chestplate.hasAbility(ShrinkerArmorAbilities.grow)) {
							AntManEntityData.get(player).makeOneStepBigger();
							chestplate.removePymParticles(PymParticleVersion.GROW, chestStack, AMConfig.pymParticlesForSizeChange, false);
						}
					}
				}
				
			}
			
			return null;
		}
	}

}
