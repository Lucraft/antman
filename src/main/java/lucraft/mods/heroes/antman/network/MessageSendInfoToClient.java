package lucraft.mods.heroes.antman.network;

import java.util.HashMap;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSendInfoToClient implements IMessage {

	public InfoType type;
	public int i;

	public MessageSendInfoToClient() {}
	
	public MessageSendInfoToClient(InfoType type) {
		this.type = type;
		this.i = 0;
	}
	
	public MessageSendInfoToClient(InfoType type, int i) {
		this.type = type;
		this.i = i;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		type = MessageSendInfoToClient.InfoType.getInfoTypeFromId(buf.readInt());
		i = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(type.ordinal());
		buf.writeInt(i);
	}
	
	public static class Handler extends AbstractClientMessageHandler<MessageSendInfoToClient> {
		
		@Override
		public IMessage handleClientMessage(EntityPlayer player, MessageSendInfoToClient message, MessageContext ctx) {
			InfoType type = message.type;
			switch (type) {
			
//			case NAMETAGS:
//				AMClientRenderer.disableNameTags = message.i == 1;
			default:
				break;
			}
			
			
			return null;
		}
	}

	public static HashMap<Integer, InfoType> ids = new HashMap<Integer, MessageSendInfoToClient.InfoType>();
	
	public enum InfoType {
		
		NAMETAGS;
		
		private InfoType() {
			MessageSendInfoToClient.ids.put(this.ordinal(), this);
		}
		
		public static InfoType getInfoTypeFromId(int id) {
			return MessageSendInfoToClient.ids.get(id);
		}
		
	}
	
}
