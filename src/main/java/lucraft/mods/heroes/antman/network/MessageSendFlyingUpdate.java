package lucraft.mods.heroes.antman.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.heroes.antman.entity.AntManEntityData;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSendFlyingUpdate implements IMessage {

	public MessageSendFlyingUpdate() {
	
	}
	
	public int entityId;
	public boolean isFlying;
	
	public MessageSendFlyingUpdate(Entity en, boolean isFlying) {
		this.entityId = en.getEntityId();
		this.isFlying = isFlying;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		entityId = buf.readInt();
		isFlying = buf.readBoolean();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(entityId);
		buf.writeBoolean(isFlying);
	}
	
	public static class Handler extends AbstractClientMessageHandler<MessageSendFlyingUpdate> {
		
		@Override
		public IMessage handleClientMessage(EntityPlayer player, MessageSendFlyingUpdate message, MessageContext ctx) {
			Entity entity = player.worldObj.getEntityByID(message.entityId);
			
			if(entity != null) {
				AntManEntityData.get((EntityLivingBase) entity).isFlying = message.isFlying;
			}
			
			return null;
		}
	}
}
