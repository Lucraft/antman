package lucraft.mods.heroes.antman.network;

import io.netty.buffer.ByteBuf;
import lucraft.mods.heroes.antman.items.ItemAntManChestplate;
import lucraft.mods.heroes.antman.util.AntManHelper;
import lucraft.mods.heroes.antman.util.ShrinkerArmorAbilities;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSetChestplateSize implements IMessage  {

	public float size;
	
	public MessageSetChestplateSize() {
		
	}
	
	public MessageSetChestplateSize(float f) {
		this.size = f;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		size = buf.readFloat();
	}

	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeFloat(size);
	}

	public static class Handler extends AbstractServerMessageHandler<MessageSetChestplateSize> {

		@Override
		public IMessage handleServerMessage(EntityPlayer player, MessageSetChestplateSize message, MessageContext ctx) {
			if(player != null && message != null && ctx != null) {
				if(AntManHelper.hasChestplateOn(player) && ((ItemAntManChestplate)AntManHelper.getAntManChestplate(player).getItem()).hasAbility(ShrinkerArmorAbilities.sizeRegulator)) {
					float f = message.size;
					MathHelper.clamp_float(f, 0.11F, 10F);
					AntManHelper.setSavedScaleInChestplate(player, f);
				}
			}
			
			return null;
		}
		
	}
	
}
