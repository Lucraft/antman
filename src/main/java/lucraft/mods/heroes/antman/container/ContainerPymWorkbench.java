package lucraft.mods.heroes.antman.container;

import lucraft.mods.heroes.antman.blocks.AMBlocks;
import lucraft.mods.heroes.antman.container.slots.SlotPymCrafting;
import lucraft.mods.heroes.antman.recipes.AntManCraftingManager;
import lucraft.mods.heroes.antman.tileentity.TileEntityPymWorkbench;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.InventoryCraftResult;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;

public class ContainerPymWorkbench extends Container {
	
	/** The crafting matrix inventory (3x3). */
	public InventoryCrafting craftMatrix = new InventoryCrafting(this, 3, 3);
	public IInventory craftResult = new InventoryCraftResult();
	private World worldObj;
	private TileEntityPymWorkbench tileEntity;
	private BlockPos pos;

	public ContainerPymWorkbench(TileEntityPymWorkbench tileE, InventoryPlayer playerInventory, World worldIn, BlockPos posIn) {
		this.worldObj = worldIn;
		this.pos = posIn;
		this.tileEntity = tileE;
		this.addSlotToContainer(new SlotPymCrafting(playerInventory.player, this.craftMatrix, this.craftResult, 0, 124, 35));

		updateCraftingMatrix();
		
		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 3; ++j) {
				this.addSlotToContainer(new Slot(this.craftMatrix, j + i * 3, 30 + j * 18, 17 + i * 18));
			}
		}

		for (int k = 0; k < 3; ++k) {
			for (int i1 = 0; i1 < 9; ++i1) {
				this.addSlotToContainer(new Slot(playerInventory, i1 + k * 9 + 9, 8 + i1 * 18, 84 + k * 18));
			}
		}

		for (int l = 0; l < 9; ++l) {
			this.addSlotToContainer(new Slot(playerInventory, l, 8 + l * 18, 142));
		}

		this.onCraftMatrixChanged(this.craftMatrix);
	}

    private void updateCraftingMatrix() {
    	for (int i = 0; i < craftMatrix.getSizeInventory(); i++) {
    	craftMatrix.setInventorySlotContents(i, tileEntity.craftMatrixInventory[i]);
    	}
    }
	
	public void onCraftMatrixChanged(IInventory inventoryIn) {
		this.craftResult.setInventorySlotContents(0, AntManCraftingManager.getInstance().findMatchingRecipe(this.craftMatrix, this.worldObj));
	}

	public void onContainerClosed(EntityPlayer player) {
		super.onContainerClosed(player);
    	saveCraftingMatrix();
	}

    private void saveCraftingMatrix() {
    	for (int i = 0; i < craftMatrix.getSizeInventory(); i++) {
    		tileEntity.craftMatrixInventory[i] = craftMatrix.getStackInSlot(i);
    	}
    }
	
	public boolean canInteractWith(EntityPlayer playerIn) {
		return this.worldObj.getBlockState(this.pos).getBlock() != AMBlocks.pymWorkbench ? false : playerIn.getDistanceSq((double) this.pos.getX() + 0.5D, (double) this.pos.getY() + 0.5D, (double) this.pos.getZ() + 0.5D) <= 64.0D;
	}

	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
		ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(index);

		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (index == 0) {
				if (!this.mergeItemStack(itemstack1, 10, 46, true)) {
					return null;
				}

				slot.onSlotChange(itemstack1, itemstack);
			} else if (index >= 10 && index < 37) {
				if (!this.mergeItemStack(itemstack1, 37, 46, false)) {
					return null;
				}
			} else if (index >= 37 && index < 46) {
				if (!this.mergeItemStack(itemstack1, 10, 37, false)) {
					return null;
				}
			} else if (!this.mergeItemStack(itemstack1, 10, 46, false)) {
				return null;
			}

			if (itemstack1.stackSize == 0) {
				slot.putStack((ItemStack) null);
			} else {
				slot.onSlotChanged();
			}

			if (itemstack1.stackSize == itemstack.stackSize) {
				return null;
			}

			slot.onPickupFromSlot(playerIn, itemstack1);
		}

		return itemstack;
	}

	public boolean canMergeSlot(ItemStack stack, Slot p_94530_2_) {
		return p_94530_2_.inventory != this.craftResult && super.canMergeSlot(stack, p_94530_2_);
	}
}