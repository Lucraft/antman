package lucraft.mods.heroes.antman.container.slots;

import lucraft.mods.heroes.antman.tileentity.TileEntityPymParticleProducer;
import lucraft.mods.heroes.antman.util.IPymParticleContainer;
import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;

public class SlotPymParticleProducer extends Slot {

	public SlotPymParticleProducer(IInventory inventoryIn, int index, int xPosition, int yPosition) {
		super(inventoryIn, index, xPosition, yPosition);
	}

	@Override
	public boolean isItemValid(ItemStack stack) {
		switch (this.slotNumber) {
		case 0:
			return FluidContainerRegistry.isContainer(stack) && FluidContainerRegistry.containsFluid(stack, new FluidStack(FluidRegistry.WATER, FluidContainerRegistry.BUCKET_VOLUME));
		case 1:
			return stack.getItem() == Items.glowstone_dust;
		case 2:
			return stack.getItem() == Items.redstone ? true : (stack.getItem() == Items.dye && stack.getItemDamage() == 15);
		case 3:
			return stack.getItem() == Items.ender_pearl;
		case 4:
			return TileEntityPymParticleProducer.isItemFuel(stack);
		default:
			return stack.getItem() instanceof IPymParticleContainer;
		}
	}
	
}
