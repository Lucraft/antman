package lucraft.mods.heroes.antman.container;

import lucraft.mods.heroes.antman.container.slots.SlotRegulator;
import lucraft.mods.heroes.antman.items.InventoryRegulator;
import lucraft.mods.heroes.antman.items.ItemAntManChestplate;
import lucraft.mods.heroes.antman.util.AntManHelper;
import lucraft.mods.heroes.antman.util.ShrinkerArmorAbilities;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerRegulator extends Container {

	public final InventoryRegulator inventory;

	public boolean canRegulateSize;
	public int regSlotX;
	public int regSlotY;
	
	private static final int INV_START = InventoryRegulator.INV_SIZE, INV_END = INV_START + 26, HOTBAR_START = INV_END + 1, HOTBAR_END = HOTBAR_START + 8;

	public ContainerRegulator(EntityPlayer player, InventoryPlayer inventoryPlayer, InventoryRegulator inventoryItem) {
		this.inventory = inventoryItem;
		this.canRegulateSize = ((ItemAntManChestplate)AntManHelper.getAntManChestplate(player).getItem()).hasAbility(ShrinkerArmorAbilities.sizeRegulator);
		
		regSlotX = 120;
		regSlotY = canRegulateSize ? 166 : 90;
		this.addSlotToContainer(new SlotRegulator(inventoryItem, 0, regSlotX, regSlotY, inventory.regulatorItem));
		
		for (int j = 0; j < 9; j++) {
			this.addSlotToContainer(new Slot(inventoryPlayer, j, 48 + j * 18, 200));
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return inventory.isUseableByPlayer(entityplayer);
	}

	/**
	 * Called when a player shift-clicks on a slot. You must override this or
	 * you will crash when someone does that.
	 */
	public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int index) {
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(index);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if (index < 1)
            {
                if (!this.mergeItemStack(itemstack1, 1, this.inventorySlots.size(), true))
                {
                    return null;
                }
            }
            else if (!this.mergeItemStack(itemstack1, 0, 1, false))
            {
                return null;
            }

            if (itemstack1.stackSize == 0)
            {
                slot.putStack((ItemStack)null);
            }
            else
            {
                slot.onSlotChanged();
            }
        }

        return itemstack;
	}

	/**
	 * You should override this method to prevent the player from moving the
	 * stack that opened the inventory, otherwise if the player moves it, the
	 * inventory will not be able to save properly
	 */
	@Override
	public ItemStack slotClick(int slot, int button, int flag, EntityPlayer player) {
		return super.slotClick(slot, button, flag, player);
	}

	@Override
	protected boolean mergeItemStack(ItemStack stack, int start, int end, boolean backwards) {
		return super.mergeItemStack(stack, start, end, backwards);
	}
}
