package lucraft.mods.heroes.antman.container;

import lucraft.mods.heroes.antman.container.slots.SlotPymParticleProducer;
import lucraft.mods.heroes.antman.tileentity.TileEntityPymParticleProducer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnaceFuel;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ContainerPymParticleProducer extends Container {
	
	private final TileEntityPymParticleProducer tileFurnace;
	private int field_178152_f;
	private int field_178153_g;
	private int field_178154_h;
	private int field_178155_i;
	public FluidTank water;
	public FluidTank shrinkPymParticles;
	public FluidTank growPymParticles;

	public ContainerPymParticleProducer(InventoryPlayer playerInventory, TileEntityPymParticleProducer furnaceInventory) {
		this.tileFurnace = furnaceInventory;
		this.addSlotToContainer(new SlotPymParticleProducer(furnaceInventory, 0, 9, 10));
		this.addSlotToContainer(new SlotPymParticleProducer(furnaceInventory, 1, 38, 28));
		this.addSlotToContainer(new SlotPymParticleProducer(furnaceInventory, 2, 38, 50));
		this.addSlotToContainer(new SlotPymParticleProducer(furnaceInventory, 3, 38, 72));
		this.addSlotToContainer(new SlotFurnaceFuel(furnaceInventory, 4, 97, 49));
		this.addSlotToContainer(new SlotPymParticleProducer(furnaceInventory, 5, 121, 74));
		this.addSlotToContainer(new SlotPymParticleProducer(furnaceInventory, 6, 143, 9));
		this.addSlotToContainer(new SlotPymParticleProducer(furnaceInventory, 7, 164, 9));
		this.addSlotToContainer(new SlotPymParticleProducer(furnaceInventory, 8, 186, 74));

		for (int i = 0; i < 3; ++i) {
			for (int j = 0; j < 9; ++j) {
				this.addSlotToContainer(new Slot(playerInventory, j + i * 9 + 9, 24 + j * 18, 101 + i * 18));
			}
		}

		for (int k = 0; k < 9; ++k) {
			this.addSlotToContainer(new Slot(playerInventory, k, 24 + k * 18, 159));
		}
	}

	public void onCraftGuiOpened(ICrafting listener) {
		super.onCraftGuiOpened(listener);
		listener.sendAllWindowProperties(this, this.tileFurnace);
	}

	/**
	 * Looks for changes made in the container, sends them to every listener.
	 */
	public void detectAndSendChanges() {
		super.detectAndSendChanges();

		for (int i = 0; i < this.crafters.size(); ++i) {
			ICrafting icrafting = (ICrafting) this.crafters.get(i);

			if (this.field_178152_f != this.tileFurnace.getField(2)) {
				icrafting.sendProgressBarUpdate(this, 2, this.tileFurnace.getField(2));
			}

			if (this.field_178154_h != this.tileFurnace.getField(0)) {
				icrafting.sendProgressBarUpdate(this, 0, this.tileFurnace.getField(0));
			}

			if (this.field_178155_i != this.tileFurnace.getField(1)) {
				icrafting.sendProgressBarUpdate(this, 1, this.tileFurnace.getField(1));
			}

			if (this.field_178153_g != this.tileFurnace.getField(3)) {
				icrafting.sendProgressBarUpdate(this, 3, this.tileFurnace.getField(3));
			}
		}

		this.field_178152_f = this.tileFurnace.getField(2);
		this.field_178154_h = this.tileFurnace.getField(0);
		this.field_178155_i = this.tileFurnace.getField(1);
		this.field_178153_g = this.tileFurnace.getField(3);
		
		this.water = tileFurnace.water;
		this.shrinkPymParticles = tileFurnace.shrinkPymParticles;
		this.growPymParticles = tileFurnace.growPymParticles;
	}

	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int id, int data) {
		this.tileFurnace.setField(id, data);
	}

	public boolean canInteractWith(EntityPlayer playerIn) {
		return this.tileFurnace.isUseableByPlayer(playerIn);
	}

	/**
	 * Take a stack from the specified inventory slot.
	 */
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
		ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(index);

		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (index == 2) {
				if (!this.mergeItemStack(itemstack1, 3, 39, true)) {
					return null;
				}

				slot.onSlotChange(itemstack1, itemstack);
			} else if (index != 1 && index != 0) {
				if (FurnaceRecipes.instance().getSmeltingResult(itemstack1) != null) {
					if (!this.mergeItemStack(itemstack1, 0, 1, false)) {
						return null;
					}
				} else if (TileEntityFurnace.isItemFuel(itemstack1)) {
					if (!this.mergeItemStack(itemstack1, 1, 2, false)) {
						return null;
					}
				} else if (index >= 3 && index < 30) {
					if (!this.mergeItemStack(itemstack1, 30, 39, false)) {
						return null;
					}
				} else if (index >= 30 && index < 39 && !this.mergeItemStack(itemstack1, 3, 30, false)) {
					return null;
				}
			} else if (!this.mergeItemStack(itemstack1, 3, 39, false)) {
				return null;
			}

			if (itemstack1.stackSize == 0) {
				slot.putStack((ItemStack) null);
			} else {
				slot.onSlotChanged();
			}

			if (itemstack1.stackSize == itemstack.stackSize) {
				return null;
			}

			slot.onPickupFromSlot(playerIn, itemstack1);
		}

		return itemstack;
	}
}