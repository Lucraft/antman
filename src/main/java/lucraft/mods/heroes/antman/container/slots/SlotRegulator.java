package lucraft.mods.heroes.antman.container.slots;

import lucraft.mods.heroes.antman.util.AntManHelper;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class SlotRegulator extends Slot {
	
	public Item regulator;
	
	public SlotRegulator(IInventory inv, int index, int xPos, int yPos, Item regulator) {
		super(inv, index, xPos, yPos);
		this.regulator = regulator;
	}

	@Override
	public boolean isItemValid(ItemStack itemstack) {
		return AntManHelper.isItemAcceptableForRegulator(itemstack, regulator);
	}
}
