package lucraft.mods.heroes.antman.recipes;

import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import net.minecraft.block.Block;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.ShapedRecipes;
import net.minecraft.item.crafting.ShapelessRecipes;
import net.minecraft.world.World;

public class AntManCraftingManager {

	private static final AntManCraftingManager instance = new AntManCraftingManager();
	private final List<IRecipe> recipes = Lists.<IRecipe> newArrayList();

	public static AntManCraftingManager getInstance() {
		return instance;
	}

	public ShapedRecipes addRecipe(ItemStack stack, Object... recipeComponents) {
		String s = "";
		int i = 0;
		int j = 0;
		int k = 0;

		if (recipeComponents[i] instanceof String[]) {
			String[] astring = (String[]) ((String[]) recipeComponents[i++]);

			for (int l = 0; l < astring.length; ++l) {
				String s2 = astring[l];
				++k;
				j = s2.length();
				s = s + s2;
			}
		} else {
			while (recipeComponents[i] instanceof String) {
				String s1 = (String) recipeComponents[i++];
				++k;
				j = s1.length();
				s = s + s1;
			}
		}

		Map<Character, ItemStack> map;

		for (map = Maps.<Character, ItemStack> newHashMap(); i < recipeComponents.length; i += 2) {
			Character character = (Character) recipeComponents[i];
			ItemStack itemstack = null;

			if (recipeComponents[i + 1] instanceof Item) {
				itemstack = new ItemStack((Item) recipeComponents[i + 1]);
			} else if (recipeComponents[i + 1] instanceof Block) {
				itemstack = new ItemStack((Block) recipeComponents[i + 1], 1, 32767);
			} else if (recipeComponents[i + 1] instanceof ItemStack) {
				itemstack = (ItemStack) recipeComponents[i + 1];
			}

			map.put(character, itemstack);
		}

		ItemStack[] aitemstack = new ItemStack[j * k];

		for (int i1 = 0; i1 < j * k; ++i1) {
			char c0 = s.charAt(i1);

			if (map.containsKey(Character.valueOf(c0))) {
				aitemstack[i1] = ((ItemStack) map.get(Character.valueOf(c0))).copy();
			} else {
				aitemstack[i1] = null;
			}
		}

		ShapedRecipes shapedrecipes = new ShapedRecipes(j, k, aitemstack, stack);
		this.recipes.add(shapedrecipes);
		return shapedrecipes;
	}

	public void addShapelessRecipe(ItemStack stack, Object... recipeComponents) {
		List<ItemStack> list = Lists.<ItemStack> newArrayList();

		for (Object object : recipeComponents) {
			if (object instanceof ItemStack) {
				list.add(((ItemStack) object).copy());
			} else if (object instanceof Item) {
				list.add(new ItemStack((Item) object));
			} else {
				if (!(object instanceof Block)) {
					throw new IllegalArgumentException("Invalid shapeless recipe: unknown type " + object.getClass().getName() + "!");
				}

				list.add(new ItemStack((Block) object));
			}
		}

		this.recipes.add(new ShapelessRecipes(stack, list));
	}

	public void addRecipe(IRecipe recipe) {
		this.recipes.add(recipe);
	}

	public ItemStack findMatchingRecipe(InventoryCrafting p_82787_1_, World worldIn) {
		for (IRecipe irecipe : this.recipes) {
			if (irecipe.matches(p_82787_1_, worldIn)) {
				return irecipe.getCraftingResult(p_82787_1_);
			}
		}

		return null;
	}

	public ItemStack[] func_180303_b(InventoryCrafting p_180303_1_, World worldIn) {
		for (IRecipe irecipe : this.recipes) {
			if (irecipe.matches(p_180303_1_, worldIn)) {
				return irecipe.getRemainingItems(p_180303_1_);
			}
		}

		ItemStack[] aitemstack = new ItemStack[p_180303_1_.getSizeInventory()];

		for (int i = 0; i < aitemstack.length; ++i) {
			aitemstack[i] = p_180303_1_.getStackInSlot(i);
		}

		return aitemstack;
	}

	public List<IRecipe> getRecipeList() {
		return this.recipes;
	}

}
