package lucraft.mods.heroes.antman.recipes;

import lucraft.mods.heroes.antman.AMConfig;
import lucraft.mods.heroes.antman.blocks.AMBlocks;
import lucraft.mods.heroes.antman.items.AMItems;
import lucraft.mods.heroes.antman.items.ItemPymTank;
import lucraft.mods.heroes.antman.util.AntManHelper;
import lucraft.mods.heroes.antman.util.IPymParticleContainer.PymParticleVersion;
import lucraft.mods.lucraftcore.items.LCItems;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.ShapedOreRecipe;

public class AMRecipes {

	public static void init() {
		
		// Pym Workbench
		GameRegistry.addRecipe(new ShapedOreRecipe(AMBlocks.pymWorkbench, "PPP", "I I", "IBI", 'P', "plateIron", 'I', "ingotIron", 'B', Items.paper));
		
		// Pym Particle Producer
		addPymWorkbenchRecipe(AMBlocks.pymParticleProducer_inactive, "IRI", "T T", "III", 'I', "plateIron", 'R', Items.redstone, 'T', AMItems.tankTier1);
		
		// Pym Tank 1
		addPymWorkbenchRecipe(AMItems.tankTier1, "IGI", "IGI", 'I', "plateIron", 'G', "paneGlass");
		
		// Pym Tank 2
		addPymWorkbenchRecipe(AMItems.tankTier2, "PTP", "PGP", "PTP", 'G', "paneGlass", 'T', AMItems.tankTier1, 'P', "plateIron");
		
		// Pym Tank 3
		addPymWorkbenchRecipe(AMItems.tankTier3, "P1P", "BGB", "P2P", 'P', "plateIron", '1', AMItems.tankTier1, '2', AMItems.tankTier2, 'B', "blockIron", 'G', "blockGlass");
		
		// Filter
		addPymWorkbenchRecipe(AMItems.filter, "IBI", "BRB", "IBI", 'I', "plateIron", 'B', Blocks.iron_bars, 'R', "plateSilver");
		
		// EMP Dingens
		addPymWorkbenchRecipe(AMItems.empCummunicationDevice, "PP ", "PRG", " II", 'I', "ingotIron", 'P', "plateIron", 'R', Items.redstone, 'G', Items.glowstone_dust);

		// Ant Antenna
		addPymWorkbenchRecipe(AMItems.antAntenna, "  I", " I ", "P  ", 'I', "ingotIron", 'P', AMItems.empCummunicationDevice);
		
		// YJ Laser Arm
		addPymWorkbenchRecipe(AMItems.yjLaserArm, "R ", " P", "P ", 'R', "blockRedstone", 'P', "ingotTitanium");
		
		// Regulator
		addPymWorkbenchRecipe(AMItems.regulator, "IPI", "BTB", "IPI", 'I', "ingotIron", 'P', "plateIron", 'B', "blockRedstone", 'T', AMItems.tankTier3);
		
		// YJ Regulator
		addPymWorkbenchRecipe(AMItems.yjRegulator, "PGP", "GTG", "PGP", 'P', "plateTitanium", 'G', Blocks.glowstone, 'T', AMItems.tankTier3);
		
		// Shrink Disc
		if(AMConfig.recipes.get(AMItems.discShrink)) {
			ItemStack stack = new ItemStack(AMItems.tankTier1);
			ItemPymTank tank = (ItemPymTank) stack.getItem();
			stack.setItemDamage(1);
			AntManHelper.setDefaultPymTankNBTTags(stack, PymParticleVersion.SHRINK, tank.getMaxPymParticles(PymParticleVersion.SHRINK, stack));
			AntManCraftingManager.getInstance().addRecipe(new ShapedPymTankOreRecipe(new ItemStack(AMItems.discShrink, 16), " P ", "PTP", " P ", 'P', "plateIron", 'T', stack));
		}
		
		// Grow Disc
		if(AMConfig.recipes.get(AMItems.discGrow)) {
			ItemStack stack = new ItemStack(AMItems.tankTier1);
			ItemPymTank tank = (ItemPymTank) stack.getItem();
			stack.setItemDamage(2);
			AntManHelper.setDefaultPymTankNBTTags(stack, PymParticleVersion.GROW, tank.getMaxPymParticles(PymParticleVersion.GROW, stack));
			AntManCraftingManager.getInstance().addRecipe(new ShapedPymTankOreRecipe(new ItemStack(AMItems.discGrow, 16), " P ", "PTP", " P ", 'P', "plateIron", 'T', stack));
		}
		
		//---- Armor -----------------------------------------------------------------------------------------------------------------
		
		// MCU 1 Helmet
		addPymWorkbenchRecipe(AMItems.antManHelmet, "APA", "GPG", "IFI", 'A', AMItems.antAntenna, 'P', "plateIron", 'G', "paneGlassRed", 'I', "ingotIron", 'F', AMItems.filter);
		
		// MCU 1 Chestplate
		addPymWorkbenchRecipe(AMItems.antManChestplate, "ITI", "PIP", "XRX", 'I', LCItems.getColoredTriPolymer(EnumDyeColor.RED, 1), 'T', AMItems.tankTier2, 'P', "plateIron", 'R', AMItems.regulator, 'X', "ingotIron");
		
		// MCU 1 Leggings
		addPymWorkbenchRecipe(AMItems.antManLegs, "PLP", "L L", "I I", 'L', LCItems.getColoredTriPolymer(EnumDyeColor.RED, 1), 'P', "plateIron", 'I', "ingotIron");
		
		// MCU 1 Leggings
		addPymWorkbenchRecipe(AMItems.antManBoots, "B B", "P P", 'B', LCItems.getColoredTriPolymer(EnumDyeColor.GRAY, 1), 'P', "plateIron");
		
		
		// MCU 2 Helmet
		addPymWorkbenchRecipe(AMItems.antManCivilWarHelmet, "APA", "GPG", "IFI", 'A', AMItems.antAntenna, 'P', "plateSilver", 'G', "paneGlassRed", 'I', "ingotSilver", 'F', AMItems.filter);
		
		// MCU 2 Chestplate
		addPymWorkbenchRecipe(AMItems.antManCivilWarChestplate, "PTP", "SPS", "IRI", 'P', "plateIntertium", 'I', "ingotIron", 'T', AMItems.tankTier2, 'R', AMItems.regulator, 'S', "plateSilver");
	
		// MCU 2 Leggings
		addPymWorkbenchRecipe(AMItems.antManCivilWarLegs, "PLP", "L L", "I I", 'A', AMItems.antAntenna, 'P', "plateSilver", 'I', "ingotIron", 'L', "plateIntertium");

		// MCU 2 Boots
		addPymWorkbenchRecipe(AMItems.antManCivilWarBoots, "P P", "S S", 'P', "plateIntertium", 'S', "plateSilver");
			
			
		// Yellowjacket Helmet
		addPymWorkbenchRecipe(AMItems.yellowjacketHelmet, "GPG", "XPX", "III", 'G', "paneGlassYellow", 'I', "ingotTitanium", 'P', "plateTitanium", 'X', "dustGlowstone");
			
		// Yellowjacket Chestplate
		if(AMConfig.recipes.get(AMItems.yellowjacketChestplate)) {
			AntManCraftingManager.getInstance().addRecipe(new ShapedColoredLeatherArmorOreRecipe(AMItems.yellowjacketChestplate, "LTL", "CGC", "PRP", 'L', AMItems.yjLaserArm, 'T', AMItems.tankTier2, 'C', getColoredLeatherArmor(Items.leather_chestplate, 0xE5E533), 'P', "plateTitanium", 'R', AMItems.yjRegulator, 'G', "dustGlowstone"));
		}
		
		// Yellowjacket Legs
		if(AMConfig.recipes.get(AMItems.yellowjacketLegs)) {
			AntManCraftingManager.getInstance().addRecipe(new ShapedColoredLeatherArmorOreRecipe(AMItems.yellowjacketLegs, "PGP", "L L", "G G", 'L', getColoredLeatherArmor(Items.leather_leggings, 0xE5E533), 'P', "plateTitanium", 'G', "dustGlowstone"));
		}
		
		// Yellowjacket Boots
		if(AMConfig.recipes.get(AMItems.yellowjacketBoots)) {
			AntManCraftingManager.getInstance().addRecipe(new ShapedColoredLeatherArmorOreRecipe(AMItems.yellowjacketBoots, "P P", "L L", 'L', getColoredLeatherArmor(Items.leather_boots, 0xE5E533), 'P', "plateTitanium"));
		}
		
		
		// MCU Wasp Helmet
		addPymWorkbenchRecipe(AMItems.waspHelmet, "APA", "GPG", "PFP", 'A', AMItems.antAntenna, 'P', "plateIron", 'G', "paneGlassYellow", 'F', AMItems.filter);
		
		// MCU Wasp Chestplate
		if(AMConfig.recipes.get(AMItems.waspChestplate)) {
			AntManCraftingManager.getInstance().addRecipe(new ShapedColoredLeatherArmorOreRecipe(AMItems.waspChestplate, "WTW", "III", "PRP", 'I', getColoredLeatherArmor(Items.leather_chestplate, 0xE5E533), 'T', AMItems.tankTier2, 'P', "plateIron", 'R', AMItems.regulator, 'W', new ItemStack(AMItems.waspWing, 2)));
		}
		
		// MCU Wasp Leggings
		if(AMConfig.recipes.get(AMItems.waspLegs)) {
			AntManCraftingManager.getInstance().addRecipe(new ShapedColoredLeatherArmorOreRecipe(AMItems.waspLegs, "PLP", "L L", "I I", 'L', getColoredLeatherArmor(Items.leather_leggings, 0xE5E533), 'P', "plateIron", 'I', "ingotIron"));
		}
		
		// MCU Wasp Leggings
		if(AMConfig.recipes.get(AMItems.waspBoots)) {
			AntManCraftingManager.getInstance().addRecipe(new ShapedColoredLeatherArmorOreRecipe(AMItems.waspBoots, "B B", "P P", 'B', getColoredLeatherArmor(Items.leather_boots, 0xE5E533), 'P', "plateIron"));
		}
		
		
		// Comic Ant-Man Helmet
		addPymWorkbenchRecipe(AMItems.comicAntManHelmet, "APA", "GPG", "IFI", 'A', AMItems.antAntenna, 'P', "plateSilver", 'G', "paneGlassWhite", 'I', "ingotSilver", 'F', AMItems.filter);
		
		// Comic Ant-Man Chestplate
		if(AMConfig.recipes.get(AMItems.comicAntManChestplate)) {
			AntManCraftingManager.getInstance().addRecipe(new ShapedColoredLeatherArmorOreRecipe(AMItems.comicAntManChestplate, "ITI", "PIP", "XRX", 'I', getColoredLeatherArmor(Items.leather_chestplate, 0x993333), 'T', AMItems.tankTier2, 'P', LCItems.getColoredTriPolymer(EnumDyeColor.BLACK, 1), 'R', AMItems.regulator, 'X', "ingotSilver"));
		}
		
		// Comic Ant-Man Leggings
		if(AMConfig.recipes.get(AMItems.comicAntManLegs)) {
			AntManCraftingManager.getInstance().addRecipe(new ShapedColoredLeatherArmorOreRecipe(AMItems.comicAntManLegs, "GIG", "R R", "P P", 'R', getColoredLeatherArmor(Items.leather_leggings, 0x993333), 'G', getColoredLeatherArmor(Items.leather_leggings, 0x4C4C4C), 'P', "plateSilver", 'I', "ingotSilver"));
		}
		
		// Comic Ant-Man Leggings
		if(AMConfig.recipes.get(AMItems.comicAntManBoots)) {
			AntManCraftingManager.getInstance().addRecipe(new ShapedColoredLeatherArmorOreRecipe(AMItems.comicAntManBoots, "B B", "P P", 'B', getColoredLeatherArmor(Items.leather_boots, 0x4C4C4C), 'P', "plateSilver"));
		}
		
		
		// Comic Wasp Helmet
		addPymWorkbenchRecipe(AMItems.comicWaspHelmet, "A A", "Y Y", 'A', AMItems.antAntenna, 'Y', LCItems.getColoredTriPolymer(EnumDyeColor.YELLOW, 1));
		
		// Comic Wasp Chestplate
		if(AMConfig.recipes.get(AMItems.comicWaspChestplate)) {
			AntManCraftingManager.getInstance().addRecipe(new ShapedColoredLeatherArmorOreRecipe(AMItems.comicWaspChestplate, "WTW", "YBY", "CRC", 'C', getColoredLeatherArmor(Items.leather_chestplate, 0xE5E533), 'T', AMItems.tankTier2, 'Y', LCItems.getColoredTriPolymer(EnumDyeColor.YELLOW, 1), 'R', AMItems.regulator, 'B', LCItems.getColoredTriPolymer(EnumDyeColor.BLACK, 1), 'W', AMItems.waspWing));
		}
		
		// Comic Wasp Legs
		if(AMConfig.recipes.get(AMItems.comicWaspLegs)) {
			AntManCraftingManager.getInstance().addRecipe(new ShapedColoredLeatherArmorOreRecipe(AMItems.comicWaspLegs, "YPY", "B B", "B B", 'P', "plateIron", 'Y', LCItems.getColoredTriPolymer(EnumDyeColor.YELLOW, 1), 'B', LCItems.getColoredTriPolymer(EnumDyeColor.BLACK, 1)));
		}
		
		// Comic Wasp Legs
		if(AMConfig.recipes.get(AMItems.comicWaspBoots)) {
			AntManCraftingManager.getInstance().addRecipe(new ShapedColoredLeatherArmorOreRecipe(AMItems.comicWaspBoots, "Y Y", "B B", 'Y', LCItems.getColoredTriPolymer(EnumDyeColor.YELLOW, 1), 'B', LCItems.getColoredTriPolymer(EnumDyeColor.BLACK, 1)));
		}
	}
	
	public static ItemStack getColoredLeatherArmor(Item item, int color) {
		ItemStack leather = new ItemStack(item);
		if(leather.getTagCompound() == null)
			leather.setTagCompound(new NBTTagCompound());
		NBTTagCompound c = leather.getTagCompound();
		NBTTagCompound nbttagcompound1 = c.getCompoundTag("display");
		nbttagcompound1.setInteger("color", color);
		c.setTag("display", nbttagcompound1);
		leather.setTagCompound(c);
		return leather;
	}
	
	public static void addPymWorkbenchRecipe(ItemStack result, Object... recipe) {
		if(AMConfig.recipes.get(result.getItem()))
			AntManCraftingManager.getInstance().addRecipe(new ShapedPymOreRecipe(result, recipe));
	}
	
	public static void addPymWorkbenchRecipe(Item result, Object... recipe) {
		addPymWorkbenchRecipe(new ItemStack(result), recipe);
	}
	
	public static void addPymWorkbenchRecipe(Block result, Object... recipe) {
		addPymWorkbenchRecipe(new ItemStack(result), recipe);
	}
	
}
