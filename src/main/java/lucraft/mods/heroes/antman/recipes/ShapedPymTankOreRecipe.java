package lucraft.mods.heroes.antman.recipes;

import java.util.Iterator;
import java.util.List;

import lucraft.mods.heroes.antman.items.ItemPymTank;
import net.minecraft.block.Block;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public class ShapedPymTankOreRecipe extends ShapedPymOreRecipe {

	public ShapedPymTankOreRecipe(Block result, Object... recipe) {
		this(new ItemStack(result), recipe);
	}

	public ShapedPymTankOreRecipe(Item result, Object... recipe) {
		this(new ItemStack(result), recipe);
	}

	public ShapedPymTankOreRecipe(ItemStack result, Object... recipe) {
		super(result, recipe);
	}

	@SuppressWarnings("unchecked")
	protected boolean checkMatch(InventoryCrafting inv, int startX, int startY, boolean mirror) {
		for (int x = 0; x < MAX_CRAFT_GRID_WIDTH; x++) {
			for (int y = 0; y < MAX_CRAFT_GRID_HEIGHT; y++) {
				int subX = x - startX;
				int subY = y - startY;
				Object target = null;

				if (subX >= 0 && subY >= 0 && subX < width && subY < height) {
					if (mirror) {
						target = input[width - subX - 1 + subY * width];
					} else {
						target = input[subX + subY * width];
					}
				}

				ItemStack slot = inv.getStackInRowAndColumn(x, y);

				if (target instanceof ItemStack) {

					if (target != null && slot != null && ((ItemStack) target).getItem() != null && slot.getItem() != null) {
						if (((ItemStack) target).getItem() instanceof ItemPymTank && slot.getItem() instanceof ItemPymTank) {
							ItemPymTank tank = (ItemPymTank) ((ItemStack) target).getItem();
							ItemPymTank tankInInv = (ItemPymTank) slot.getItem();

							if (tank.getPymParticlesInTank((ItemStack) target) != tankInInv.getPymParticlesInTank(slot))
								return false;

							if (tank.getPymParticles(tank.getPymParticlesInTank((ItemStack) target), (ItemStack) target) != tankInInv.getPymParticles(tankInInv.getPymParticlesInTank(slot), slot))
								return false;
						}
					}

					if (!OreDictionary.itemMatches((ItemStack) target, slot, false)) {
						return false;
					}
				} else if (target instanceof List) {
					boolean matched = false;

					Iterator<ItemStack> itr = ((List<ItemStack>) target).iterator();
					while (itr.hasNext() && !matched) {
						matched = OreDictionary.itemMatches(itr.next(), slot, false);
					}

					if (!matched) {
						return false;
					}
				} else if (target == null && slot != null) {
					return false;
				}
			}
		}

		return true;
	}

}
