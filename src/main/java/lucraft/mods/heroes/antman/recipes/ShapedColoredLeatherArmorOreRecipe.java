package lucraft.mods.heroes.antman.recipes;

import java.util.Iterator;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public class ShapedColoredLeatherArmorOreRecipe extends ShapedPymOreRecipe {

	public ShapedColoredLeatherArmorOreRecipe(Block result, Object... recipe) {
		this(new ItemStack(result), recipe);
	}

	public ShapedColoredLeatherArmorOreRecipe(Item result, Object... recipe) {
		this(new ItemStack(result), recipe);
	}

	public ShapedColoredLeatherArmorOreRecipe(ItemStack result, Object... recipe) {
		super(result, recipe);
	}

	@SuppressWarnings("unchecked")
	protected boolean checkMatch(InventoryCrafting inv, int startX, int startY, boolean mirror) {
		for (int x = 0; x < MAX_CRAFT_GRID_WIDTH; x++) {
			for (int y = 0; y < MAX_CRAFT_GRID_HEIGHT; y++) {
				int subX = x - startX;
				int subY = y - startY;
				Object target = null;

				if (subX >= 0 && subY >= 0 && subX < width && subY < height) {
					if (mirror) {
						target = input[width - subX - 1 + subY * width];
					} else {
						target = input[subX + subY * width];
					}
				}

				ItemStack slot = inv.getStackInRowAndColumn(x, y);

				if (target instanceof ItemStack) {

					if (target != null && slot != null && ((ItemStack) target).getItem() != null && slot.getItem() != null) {
						ItemStack t = (ItemStack) target;
						
						if(t.getItem() instanceof ItemArmor && slot.getItem() instanceof ItemArmor) {
							if(t.getTagCompound() == null || t.getTagCompound().getCompoundTag("display") == null || slot.getTagCompound() == null || slot.getTagCompound().getCompoundTag("display") == null)
								return false;
								
							if(t.getTagCompound().getCompoundTag("display").getInteger("color") != slot.getTagCompound().getCompoundTag("display").getInteger("color")) {
								return false;
							}
						}
					}

					if (!OreDictionary.itemMatches((ItemStack) target, slot, false)) {
						return false;
					}
				} else if (target instanceof List) {
					boolean matched = false;

					Iterator<ItemStack> itr = ((List<ItemStack>) target).iterator();
					while (itr.hasNext() && !matched) {
						matched = OreDictionary.itemMatches(itr.next(), slot, false);
					}

					if (!matched) {
						return false;
					}
				} else if (target == null && slot != null) {
					return false;
				}
			}
		}

		return true;
	}

}
